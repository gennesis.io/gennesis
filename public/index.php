<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: index.php
 -------------------------------------------------------------------
 | @user: william
 | @creation: 05/03/16 07:54
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Load the app core
 |
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

/**
 * @preflight
 */
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
        header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS");
    }

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    }

    exit(0);
}


use Apocalipse\Core\App;


define('__APP_START__', microtime());

define('__APP_DIR_ROOT__', dirname(__DIR__));
define('__APP_DIR__', implode(DIRECTORY_SEPARATOR, [__APP_DIR_ROOT__, 'app']));

$env = dirname(__DIR__) . '/app/config/.env';

define('__APP_ENV__', file_exists($env) ? trim(rtrim(file_get_contents($env))) : 'sample');


/** @noinspection PhpIncludeInspection */
require_once implode(DIRECTORY_SEPARATOR, [__APP_DIR_ROOT__, 'kernel', 'bootstrap.php']);


if (!isset($service)) {
    $service = App::input('app-service');
}

define('__APP_URL__', App::getUrlRoot($service));
define('__APP_URL_CURRENT__', __APP_URL__ . '/' . $service);
define('__APP_URL_SERVICE__', $service);
define('__APP_URL_QUERY__', App::getUrlQueryString());


/** @setup `install` & `setup` */
require 'install.php';

/**
 * create a Response
 */
$response = App::http($service);

/*
 * generate:
 *   - headers
 *   - body
 */
App::response($response);
