<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Service
 | @file: View.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 09:28
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Service;


use Apocalipse\Basic\Controller\ViewController;
use Apocalipse\Basic\Model\Page;
use Apocalipse\Core\App;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Http\Header;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;

/**
 * Class View
 * @package Apocalipse\Basic\Service
 */
class View extends Service
{
    /**
     * @var string
     */
    public static $DEFAULT_PAGE = '404';

    /**
     * @var string
     */
    public static $DEFAULT_ROUTE = 'index';

    /***
     * @param \Apocalipse\Core\Http\Request $request
     * @return \Apocalipse\Core\Http\Response
     */
    public function render(Request $request)
    {
        $route = $request->getRoute();

        $use = pathToNamespace('/Apocalipse/Basic/Controller/ViewController');

        // search page to define page controller and resource type
        $context = $this->search($route);

        if ($context->controller) {
            $use = pathToNamespace($context->controller);
        }

        $resource = $context->resource;

        $headers = [];
        if ($context->cache === 'never') {
            $headers[] = new Header("Expires", "Tue, 01 Jan 2000 00:00:00 GMT");
            $headers[] = new Header("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
            $headers[] = new Header("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
            $headers[] = new Header("Cache-Control", "post-check=0, pre-check=0", false);
            $headers[] = new Header("Pragma", "no-cache");
        }

        $scope = environment('scope');
        $resources = environment('resources.views.' . $resource);

        $root = path(__APP_DIR_ROOT__, $scope->type, $resources->root);

        $views = path($root, $resources->template);
        $design = path($root, $resources->design);
        $suffix = $resources->suffix;

        $status = '200';

        if ($resource === 'app') {

            if (count($route) > 1) {

                if (isset($route[1]) && $route[1] === 'routes') {

                    if (isset($route[2]) && $route[2] !== 'auth' && $route[2] !== 'controller') {

                        if (!$this->isAllowed($request->getHeaders())) {

                            $status = '403';
                            $route = ['routes', 'error', '403'];
                            Wrapper::unauthorized('Is not allowed');
                        }
                        else if ($route[2] === 'report') {

                            $route[2] = 'view';
                            $context = new Record(['scope' => $scope, 'layer' => 'model', 'module' => $route[3], 'entity' => $route[4]]);
                            $views = path(true, 'src', 'view');
                            $use = pathToNamespace('/Apocalipse/Basic/Controller/ReportController');
                        }
                    }
                }
            }
        }

        /** @var ViewController $controller */
        $controller = new $use($context, $views, $suffix, $design);

        $container = $controller->render($route, $request->getBody());

        $content = $container->getData();
        $type = $container->getType();

        if ($content) {

            Wrapper::approve("The page requested by '" . App::getClientAddress() . "' was compiled");
        } else {

            $content = $controller->getFallback();
            $status = '404';
        }

        $headers[] = new Header("App-Status", $status);

        $response = new Response($content, $type, $headers);

        return $response;
    }

    /**
     * @param $route
     * @return Record
     */
    protected function search($route)
    {
        if (!is_array($route) || !count($route)) {
            $route = [self::$DEFAULT_ROUTE];
        }

        $page = new Page();

        $pages = $page->read();

        return self::compare($route, $pages);
    }

    /**
     * @param $peaces
     * @param $pages
     *
     * @return Record
     */
    protected function compare(array $peaces, $pages)
    {
        /** @var Record $page */

        $default = self::$DEFAULT_PAGE;
        $parameters = [];

        $page = $this->match($pages, implode('/', $peaces));

        if (!$page) {

            while (count($peaces) > 0) {

                $breakable = $this->match($pages, implode('/', $peaces));

                if ($breakable) {

                    if ($breakable->brook || !count($parameters)) {
                        $page = $breakable;
                        break;
                    }
                }
                $parameters[] = array_pop($peaces);
            }
        }

        if (!$page) {
            $page = $this->match($pages, $default);
        }

        if ($page) {
            $page->inject('parameters', $parameters);
        }

        return $page;
    }

    /**
     * @param $pages
     * @param $search
     * @return Page
     */
    protected function match($pages, $search)
    {
        $match = null;

        if (is_object($pages) || is_array($pages)) {

            /** @var Page $page */
            foreach ($pages as $page) {

                if ($page->path === $search) {
                    $match = $page;
                    break;
                }
            }
        }
        return $match;
    }
}