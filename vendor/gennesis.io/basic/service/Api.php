<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Service
 | @file: Api.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 08:54
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Service;


use Apocalipse\Basic\Controller\ApiController;
use Apocalipse\Core\Domain\Security\Auth;
use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;

/**
 * Class Api
 * @package Apocalipse\Basic\Service
 */
class Api extends Service
{
    /***
     * @param Request $request
     * @return Response
     */
    public function render(Request $request)
    {
        $headers = $request->getHeaders();
        $route = $request->getRoute();

        $container = new Container('', Container::TYPE_JSON);

        array_shift($route);

        if (count($route) >= 4) {

            //$version = $route[0];
            $module = $route[1];
            $entity = $route[2];
            $operation = $route[3];

            switch ($module) {

                case 'auth':

                    $container = Auth::request($operation, $request->getBody()->getPayload(), $headers);
                    break;

                default:

                    if ($this->isAllowed($headers)) {

                        // TODO: get Controller from "environment"

                        $controller = new ApiController($module, $entity);

                        switch ($operation) {

                            case 'model':

                                $content = $controller->getModel();

                                if ($content) {
                                    $content->definition->driver = null;
                                }
                                $container = new Container($content, Container::TYPE_JSON);
                                break;

                            default:

                                $use = pathToNamespace('/' . path(environment('scope.namespace'), 'Controller', dashesToCamelCase($module, true), dashesToCamelCase($entity, true)));
                                if (class_exists($use)) {
                                    $controller = new $use();
                                }

                                $container = $controller->render($operation, $request->getBody());
                                break;
                        }

                    } else {
                        Wrapper::unauthorized("Not authorized: 'invalid token'");
                    }
                    break;
            }

            if ($container->getData()) {
                Wrapper::approve("Operation '" . $operation . "' of '" . $entity . "' from '" . $module . "' successfully");
            }

        } else {

            Wrapper::err('Route invalid: "' . implode('/', $route) . '"');
        }

        return new Response($container->getData(), $container->getType());
    }
}