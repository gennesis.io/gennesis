<?php
/*
 -------------------------------------------------------------------
 | @project: php@gennesis
 | @package: Apocalipse\Basic\Service
 | @file: Resource.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 30/04/16 16:00
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Service;

use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;
use Apocalipse\Core\Model\Type\Result;
use Apocalipse\Basic\Controller\ApiController;

class Resource extends Service
{
    /***
     * @param \Apocalipse\Core\Http\Request $request
     * @return \Apocalipse\Core\Http\Response
     */
    public function render(Request $request)
    {
        $container = new Container('', Container::TYPE_JSON);

        if ($this->isAllowed($request->getHeaders())) {

            $route = $request->getRoute();

            array_shift($route);

            if (count($route) === 2) {

                $container->setData($route);

                $module = $route[0];
                $entity = $route[1];

                $use = pathToNamespace('/' . path(environment('scope.namespace'), 'Controller', dashesToCamelCase($module, true), dashesToCamelCase($entity, true)));
                if (class_exists($use)) {
                    $controller = new $use();
                } else {
                    $controller = new ApiController($module, $entity);
                }

                $data = $request->getBody();
                $method = $request->getMethod();
                switch ($method) {
                    case 'GET':
                        //read
                        $operation = 'read';
                        break;
                    case 'POST':
                        //new
                        $operation = 'save';
                        $data = new Data(null, null, null, new Record([(object) $data->getPayload()->all()]));
                        break;
                    case 'PUT':
                        //update
                        $operation = 'save';
                        break;
                    case 'DELETE':
                        //delete
                        $operation = 'delete';
                        $data = new Data(null, null, null, new Record([(object) $data->getGet()->all()]));
                        //dd($data->getPayload()->get('_id'));
                        break;
                    default:
                        $operation = strtolower($method);
                        break;
                }

                $container = $controller->render($operation, $data);
            }
        }

        if ($container->getData()) {
            Wrapper::approve("Resource successfully");
        }

        return new Response($container->getData(), $container->getType());
    }
}