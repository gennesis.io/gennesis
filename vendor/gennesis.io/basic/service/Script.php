<?php
/*
 -------------------------------------------------------------------
 | @project: php@sistemaconcursos.com.br
 | @package: Apocalipse\Basic\Service
 | @file: Script.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 25/05/16 02:40
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Service;


use Apocalipse\Core\App;
use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;

/**
 * Class Script
 * @package Apocalipse\Basic\Service
 */
class Script extends Service
{
    /***
     * @param \Apocalipse\Core\Http\Request $request
     * @return \Apocalipse\Core\Http\Response
     */
    public function render(Request $request)
    {
        $route = $request->getRoute();

        array_shift($route);

        $extension = 'php';

        $filename = path(true, 'src', 'domain', 'script', implode(DIRECTORY_SEPARATOR, $route) . '.' . $extension);

        if (File::exists($filename)) {

            ob_start();

            /** @noinspection PhpIncludeInspection */
            require_once $filename;

            $closure = (string) dashesToCamelCase($route[(count($route) - 1)]);

            if (isset($closure) && is_callable($closure)) {
                $closure($request->getBody());
            }

            $content = ob_get_contents();

            ob_end_clean();

            Wrapper::approve("The page requested by '" . App::getClientAddress() . "' was compiled");

        } else {

            $content = 'Script not found';

            Wrapper::err($content);
        }

        $type = Container::TYPE_HTML;
        $headers = [];

        return new Response($content, $type, $headers);
    }
}