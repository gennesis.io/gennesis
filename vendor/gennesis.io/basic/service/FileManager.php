<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Content
 | @file: Manager.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 02:53
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Service;


use Apocalipse\Basic\Controller\FileReaderController;
use Apocalipse\Basic\Controller\FileWriterController;
use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Http\Header;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class FileManager
 * @package Apocalipse\Basic\Resource
 */
class FileManager extends Service
{
    /**
     * @param Request $request
     * @return Response
     */
    public function render(Request $request)
    {
        $route = $request->getRoute();
        /** @var Data $data */
        $data = $request->getBody();

        array_shift($route);

        $controller = new FileReaderController();

        $content = '';
        $type = '';

        $headers = [];

        $allowed = true;

        $scope = environment('scope');
        $root = path(__APP_DIR_ROOT__, $scope->type);
        if ($scope->type === 'vendor') {
            $root = path($root, $scope->package);
        }
        $resources = environment('resources');

        $service = $route[0];

        switch ($service) {

            case 'download':

                if ($resources->download->root) {

                    array_shift($route);

                    $controller->setContext(path($root, $resources->download->root));
                } else {

                    $allowed = false;
                }
                break;

            case 'upload':

                $allowed = $this->isAllowed($request->getHeaders());

                if ($allowed && $resources->upload) {

                    array_shift($route);

                    $content = Json::encode([
                        'ok'=> false,
                        'status'=> 'error',
                        'data'=> null,
                        'notification'=> []
                    ]);

                    $controller = new FileWriterController(path($root, $resources->upload->root));

                }
                break;

            default:

                if ($resources->assets) {

                    $controller->setContext(path($root, $resources->assets->root));
                } else {

                    $allowed = false;
                }
                break;
        }

        if ($allowed) {

            /** @var Container $container */
            $container = $controller->render($route, $data);

            $content = $container->getData();
            $type = $container->getType();
            $info = $container->getInfo();


            if ($content) {

                $content_disposition = sif($info, 'content-disposition');
                $content_length = sif($info, 'content-length');

                switch ($service) {

                    case 'download':

                        $headers[] = new Header('Content-Type', 'application/force-download');
                        $headers[] = new Header('Content-Type', 'application/octet-stream');
                        $headers[] = new Header('Content-Type', 'application/download');
                        $headers[] = new Header('Content-Type', 'application/save');
                        $headers[] = new Header('Content-Description', 'File Transfer');
                        $headers[] = new Header('Content-Transfer-Encoding', 'binary');
                        if ($content_disposition) {
                            $headers[] = new Header('Content-Disposition', 'attachment; filename=' . $content_disposition);
                        }
                        if ($content_length) {
                            $headers[] = new Header('Content-Length', $content_length);
                        }
                        $headers[] = new Header('Expires', '0');
                        $headers[] = new Header('Pragma', 'no-cache');
                        break;

                    case 'upload':
                        $content = Json::encode([
                            'ok'=> true,
                            'status'=> 'success',
                            'data'=> $content,
                            'notification'=> []
                        ]);
                        break;

                    default:

                        if ($content_length) {
                            $headers[] = new Header('Content-Length', $content_length);
                        }
                        break;
                }

                Wrapper::approve("Resource service parse the resource successfully");
            }
        }

        return new Response($content, $type, $headers);
    }
}