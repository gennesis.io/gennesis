/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceApi', ['$http', '$location', '$timeout', '$cookies',
    function ($http, $location, $timeout, $cookies) {

        var ServiceApi = {
            /**
             * @param module
             * @param entity
             * @param operation
             * @param data
             * @param callback //server, ok, status, data, notification
             * @param method
             */
            request: function (module, entity, operation, data, callback, method) {

                var headers = {};
                headers[App.header] = App.token;

                var request = {
                    method: (method ? method : 'POST'),
                    url: ServiceApi._url([module, entity, operation]),
                    /*headers: headers,*/
                    data: data
                };

                $http(request).then(
                    function (response) {
                        ServiceApi._success(callback, response);
                    },
                    function (response) {
                        ServiceApi._error(callback, response);
                    });
            },
            /**
             *
             * @param module
             * @param entity
             * @param layout
             * @param data
             * @param callback
             */
            report: function (module, entity, layout, data, callback) {

                var request = {
                    method: 'POST',
                    url: 'routes/report/' + module + '/' + entity + '/' + layout + '.' + 'html' + '?t=' + App.uniqid(),
                    data: data
                };

                // success
                $http(request).then(
                    function (response) {
                        if (angular.isFunction(callback)) {
                            callback.call(this, response);
                        }
                    });
            },
            /**
             * @param pieces
             * @returns {string}
             */
            _url: function (pieces) {

                return App.api + '/' + $.map(pieces, function (piece) {
                        return App.uncamelize(piece);
                    }).join('/');
            },
            /**
             * @param callback
             * @param response
             *
             * @private
             */
            _success: function (callback, response) {
                ServiceApi._then(true, callback, response);
            },
            /**
             * @param callback
             * @param response
             *
             * @private
             */
            _error: function (callback, response) {
                ServiceApi._then(false, callback, response);
            },
            /**
             * @param status
             * @param callback
             * @param result
             *
             * @private
             */
            _then: function (status, callback, result) {

                if (angular.isFunction(callback)) {

                    var response = result.data;

                    if (response.status === 'unauthorized') {

                        ServiceApi.auth.logout(App.token);
                    } else {

                        callback.call(this, status, response.ok, response.status, response.data, response.notifications);
                    }
                }
            },
            /**
             * @param where
             */
            go: function (where) {
                $timeout(function () {
                    $location.path('/' + where);
                }, 100);
            },
            /**
             * @var {object}
             */
            auth: {
                /**
                 * @var {object}
                 */
                $scope: {},
                /**
                 * @param $scope
                 */
                init: function ($scope) {
                    ServiceApi.$scope = $scope;
                },
                /**
                 * @param credential
                 * @param callback
                 */
                login: function (credential, callback) {

                    ServiceApi.request('auth', 'request', 'login', credential, function (server, ok, status, token) {
                        if (ok) {
                            if (token) {
                                ServiceApi.auth.token(token, credential.remember);
                                ServiceApi.$scope.logged = true;
                            }
                            if (angular.isFunction(callback)) {
                                callback.call(this, token);
                            }
                        }
                    });
                },
                /**
                 * @param token
                 * @param redirect
                 */
                logout: function (token, redirect) {

                    ServiceApi.request('auth', 'request', 'logout', {token: token}, null);
                    ServiceApi.auth.token(null);
                    App.history = {};
                    ServiceApi.$scope.logged = false;
                    if (redirect) {
                        window.location.href = redirect;
                    } else {
                        ServiceApi.go('auth/login');
                    }
                },
                /**
                 * @param token
                 */
                grant: function (token) {

                    ServiceApi.request('auth', 'request', 'permission', {token: token}, function (server, ok, status, data) {
                        if (ok) {
                            ServiceApi.$scope.permissions = data;
                        }
                    });
                },
                /**
                 * @param token
                 * @param remember
                 */
                token: function (token, remember) {

                    App.token = token;
                    ServiceApi.auth.grant(token);

                    if (token) {
                        var expires = null;
                        if (remember) {
                            var expiration = new Date();
                            expiration.setMonth(expiration.getMonth() + 1);
                            expires = {expires: expiration};
                        }
                        $cookies.put(App.ref, token, expires);
                    } else {
                        console.log('remove');
                        $cookies.remove(App.ref);
                    }
                }
            }
        };

        return ServiceApi;
    }]);