/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceOperation',
    ['$route', '$location', '$timeout', '$window', 'ServiceModel', 'ServiceDialog', 'ServiceGrowl', 'ServiceListener', 'ServiceApi', 'ServicePagination',
        function ($route, $location, $timeout, $window, ServiceModel, ServiceDialog, ServiceGrowl, ServiceListener, ServiceApi, ServicePagination) {

            var ServiceOperation = {
                /**
                 *
                 * @param index
                 * @param record
                 * @param after
                 */
                operation: function (index, record, after) {

                    var operation = ServiceModel.getOperation(index);

                    if (operation) {

                        switch (operation.type) {

                            case 'form':

                                var dirty = false;
                                if (ServiceModel.$scope.form) {
                                    dirty = ServiceModel.$scope.form.$dirty;
                                }

                                if (!dirty) {

                                    ServiceOperation.form.run(operation, record);
                                } else {

                                    ServiceDialog.confirm(
                                        "Alerta",
                                        "Você possui alterações não salvas. Deseja continuar?",
                                        "Sim, pode descartar minhas alterações!",
                                        function () {

                                            $timeout(function () {
                                                ServiceOperation.form.run(operation, record);
                                            }, 100);
                                        }
                                    );

                                }
                                break;
                            case 'action':

                                if (operation.confirm) {

                                    ServiceDialog.confirm("Alerta", operation.message.confirm, "Sim", function () {
                                        ServiceOperation.action.run(operation, record, after);
                                    });
                                } else {
                                    ServiceOperation.action.run(operation, record, after);
                                }

                                break;
                        }

                    } else {

                        ServiceOperation.operation('index');
                    }
                },
                /**
                 * @var {object}
                 */
                action: {
                    /**
                     *
                     * @param operation
                     * @param record
                     * @param after
                     */
                    run: function (operation, record, after) {

                        var
                            index = operation.id,
                            parameters = [],
                            listen = ServiceModel.module + '.' + ServiceModel.entity + '.' + ServiceModel.index;

                        ServiceListener.trigger('before.' + index, listen, record);

                        if (operation.interaction) {

                            switch (operation.interaction) {
                                case 'link':
                                    var
                                        link = operation.settings,
                                        path = window.location.pathname.split('/');
                                        path.pop();
                                        path.pop();
                                    var
                                        title = jQuery('title').html(),
                                        url = window.location.hostname + path.join('/'),
                                        width = jQuery(window).width(),
                                        height = jQuery(window).height();

                                    link = link.replace('{url}', url);

                                    for (var i in record) {
                                        //noinspection JSUnfilteredForInLoop
                                        link = link.replace('{' + i + '}', record[i]);
                                    }

                                    $window.open(link, title, 'width=' + width + ',height=' + height);
                                    break;

                                case 'function':
                                    if (ServiceModel.$scope && ServiceModel.$scope[operation.settings]) {
                                        var callable = ServiceModel.$scope[operation.settings];
                                        if (angular.isFunction(callable)) {
                                            callable.call(this, record);
                                        }
                                    }
                                    break;
                            }

                            ServiceListener.trigger('after.' + index, listen, record);

                        } else {

                            angular.forEach(operation.parameters, function (parameter) {
                                switch (parameter) {
                                    case 'record':
                                        var data = angular.copy(record);
                                        if (data && data.relations) {
                                            delete data.relations;
                                        }
                                        for (var _property in data) {
                                            //noinspection JSUnfilteredForInLoop
                                            var position = data[_property];
                                            if (position instanceof Array) {
                                                angular.forEach(position, function (_row, i) {
                                                    delete _row.relations;
                                                    position[i] = _row;
                                                });
                                            }
                                        }
                                        parameters.push(data);
                                        break;
                                    case 'filters':
                                        parameters.push(null);
                                        break;
                                    case 'sorters':
                                        parameters.push(null);
                                        break;
                                    case 'aggregators':
                                        parameters.push(null);
                                        break;
                                    case 'fields':
                                        parameters.push(null);
                                        break;
                                }
                            });

                            ServiceApi.request(ServiceModel.module, ServiceModel.entity, index, parameters, function (ss, ok, status, data, notifications) {

                                if (ok) {

                                    if (operation.message && operation.message[status]) {
                                        ServiceGrowl.growl(operation.message[status], 'inverse');
                                    }

                                    if (status === 'success') {

                                        if (ServiceModel.$scope.form) {

                                            ServiceModel.$scope.form.$setPristine();
                                        }

                                        if (typeof after === 'string') {

                                            ServiceOperation.operation(after);

                                        } else if (angular.isFunction(after)) {

                                            after.call(this, data);
                                        }

                                        ServiceListener.trigger('after.' + index, listen, record, data, notifications);
                                    }
                                }
                            });
                        }
                    }
                },
                /**
                 * @var {object}
                 */
                form: {
                    /**
                     *
                     * @param operation
                     * @param record
                     */
                    run: function (operation, record) {

                        var
                            url = '/view/' + ServiceModel.module + '/' + ServiceModel.entity + '/' + operation.id;

                        if (operation.templateOptions.recover && operation.templateOptions.wrapper) {
                            if (operation.templateOptions.wrapper === 'record' && record && record[App.id]) {
                                url = url + '/' + record[App.id];
                            }
                        }

                        var current = $route.current.loadedTemplateUrl;

                        var reload = (current.indexOf('routes' + url) === 0);

                        if (reload) {
                            $route.reload();

                        } else {

                            $location.path(url);
                        }
                    }
                },
                /**
                 *
                 * @param index
                 * @param record
                 */
                init: function (index, record) {

                    var
                        operation = ServiceModel.getOperation(index),
                        listen = ServiceModel.module + '.' + ServiceModel.entity + '.' + ServiceModel.index;

                    if (operation) {

                        ServiceListener.trigger('before.' + index, listen, record);

                        var template = operation.templateOptions;

                        if (template) {

                            if (template.recover) {

                                switch (template.wrapper) {

                                    case 'record':

                                        if (record) {

                                            var object = {};
                                            object[App.id] = record[0];

                                            ServiceOperation.recoverRecord(operation.id, template.recover, object);
                                        }
                                        break;

                                    case 'recordSet':

                                        ServiceOperation.recoverRecordSet(template.recover);
                                        break;
                                }
                            } else {

                                if (!record) {
                                    record = {};
                                }
                                ServiceModel.record = record;
                            }

                            ServiceListener.trigger('after.' + operation.id, listen, record);
                        }
                    }
                },
                /**
                 *
                 * @param index
                 */
                recoverRecordSet: function (index) {

                    var
                        filters = ServicePagination.filters(ServiceModel.getItems()),
                        sorters = null,
                        aggregators = null,
                        limiters = ServicePagination.limiters(),
                        fields = null,
                        listen = ServiceModel.module + '.' + ServiceModel.entity + '.' + ServiceModel.index;

                    ServiceApi.request(
                        ServiceModel.module, ServiceModel.entity, index, [filters, sorters, aggregators, limiters, fields],

                        function (ss, ok, status, data) {

                            if (ok) {

                                ServicePagination.update(ServiceModel.getId(), data.total);

                                ServiceListener.trigger('after.recover-record-set', listen, filters, data);
                            }
                        }
                    );
                },
                /**
                 *
                 * @param index
                 * @param recover
                 * @param query
                 */
                recoverRecord: function (index, recover, query) {

                    ServiceApi.request(
                        ServiceModel.module, ServiceModel.entity, recover, [query],

                        function (ss, ok, status, data, notification) {

                            if (ok) {

                                var record = data.rows[0],
                                    listen = ServiceModel.module + '.' + ServiceModel.entity + '.' + ServiceModel.index;

                                if (record) {
                                    record = ServiceModel.format.front(record);
                                }

                                ServiceModel.record = record;

                                ServiceListener.trigger('after.recover-record', listen, record, data);
                            }
                        }
                    );
                }
            };

            return ServiceOperation;
        }]);