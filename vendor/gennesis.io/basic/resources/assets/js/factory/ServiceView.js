/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceView',
    ['$timeout', 'ServiceApi', 'ServicePopup', 'ServiceDialog', 'ServiceListener', 'ServiceGrowl',
        'ServiceOutput', 'ServiceOperation', 'ServiceModel', 'ServiceLayout', 'ServiceInput', 'ServicePagination',
        function ($timeout, ServiceApi, ServicePopup, ServiceDialog, ServiceListener, ServiceGrowl,
                  ServiceOutput, ServiceOperation, ServiceModel, ServiceLayout, ServiceInput, ServicePagination) {

            var ServiceView = {
                /**
                 * @var {string}
                 */
                effect: 'flip',
                //effect: 'lightspeed',
                /**
                 *
                 * @param value
                 * @returns {*}
                 */
                layout: ServiceLayout,
                /**
                 *
                 * @param value
                 * @returns {*}
                 */
                page: ServicePagination,
                /**
                 *
                 * @param value
                 * @returns {*}
                 */
                out: ServiceOutput,
                /**
                 *
                 * @param value
                 * @returns {*}
                 */
                in: ServiceInput,
                /**
                 *
                 * @var {object}
                 */
                dialog: ServiceDialog,
                /**
                 * @var {object}
                 */
                popup: ServicePopup,
                /**
                 *
                 * @var {object}
                 */
                listener: ServiceListener,
                /**
                 *
                 * @var {object}
                 */
                model: ServiceModel,
                /**
                 *
                 * @param $scope
                 * @param module
                 * @param entity
                 * @param index
                 * @param value
                 */
                init: function ($scope, module, entity, index, value) {

                    ServiceModel.$scope = $scope;
                    ServiceModel.module = module;
                    ServiceModel.entity = entity;
                    ServiceModel.index = index;

                    ServiceModel.record = {};
                    ServiceModel.recordSet = [];

                    ServicePopup.reset();

                    var id = ServiceModel.getId();

                    ServicePagination.load(id);

                    if (!ServiceModel.models[id]) {

                        ServiceApi.request(
                            ServiceModel.module, ServiceModel.entity, 'model', null,

                            function (server, ok, status, data) {
                                if (ok) {
                                    if (status === 'success') {
                                        ServiceModel.models[id] = data;
                                        ServiceOperation.init(index, value);
                                    } else {
                                        ServiceGrowl.growl('Can not load this resource', 'danger');
                                    }
                                }
                            }
                        );
                    } else {

                        ServiceOperation.init(index, value);
                    }
                },
                /**
                 *
                 * @param index
                 * @param record
                 * @param after
                 */
                operation: function (index, record, after) {

                    ServiceOperation.operation(index, record, after);
                },
                /**
                 *
                 * @param index
                 * @param page
                 */
                recover: function (index, page) {

                    if (ServiceOperation._timeout) {
                        $timeout.cancel(ServiceOperation._timeout);
                    }

                    ServiceOperation._timeout = $timeout(function() {

                        ServicePagination.current(page);
                        ServiceOperation.init(index);

                        ServiceOperation._timeout = null;
                    }, 100);
                }
            };

            /**
             * @param $scope
             * @param module
             * @param entity
             * @param index
             * @param value
             */
            return function ($scope, module, entity, index, value) {

                ServiceView.init($scope, module, entity, index, value);

                return ServiceView;
            };
        }])
;