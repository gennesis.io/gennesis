/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceInput', ['$http', 'ServiceModel', function ($http, ServiceModel) {

    var ServiceInput = {
        /**
         *
         * @param record
         * @param entity
         * @param module
         * @param name
         * @param files
         */
        file: function (record, module, entity, name, files) {

            module = module ? module : ServiceModel.module;
            entity = entity ? entity : ServiceModel.entity;

            var
                id = record[App.id],
                url = App.upload + '/' + module + '/' + entity + '/' + id,
                data = {},
                options = {};

            if (files.length) {

                var file = files[0];

                data = new FormData();
                data.append(name, file);

                options = {
                    withCredentials: true,
                    headers: {'Content-Type': undefined},
                    transformRequest: angular.identity
                };

                $http.post(url, data, options)
                    .success(function (response) {

                        //console.log(response);
                        if (response.data && response.data[name]) {

                            //ServiceModel.record[name] = response.data[name];
                            record[name] = response.data[name];
                        }
                    })
                    .error(function () {

                    });
            } else {

                record[name] = '';
            }
        }
    };

    return ServiceInput;
}]);