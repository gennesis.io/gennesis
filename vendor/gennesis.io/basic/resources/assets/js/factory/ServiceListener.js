/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceListener', ['ServiceModel', function (ServiceModel) {

    /**
     *
     * @param alias
     * @returns {boolean}
     */
    Array.prototype.hasAlias = function (alias) {
        var i = this.length;
        while (i--) {
            //console.log(i, this[i], this[i]['alias']);
            if (this[i] && this[i]['alias'] === alias) {
                return true;
            }
        }
        return false;
    };

    /**
     *
     * @returns {number}
     */
    String.prototype.hashCode = function () {
        var hash = 0;
        if (this.length == 0) return hash;
        for (var i = 0; i < this.length; i++) {
            hash = ((hash << 5) - hash) + this.charCodeAt(i);
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
    };

    var ServiceListener = {
        /**
         *
         * @var {object}
         */
        list: {},
        /**
         *
         * @param event
         * @param id
         * @param callable
         * @param alias
         */
        register: function (event, id, callable, alias) {

            if (!ServiceListener.list[event]) {
                ServiceListener.list[event] = {};
            }
            if (!ServiceListener.list[event][id]) {
                ServiceListener.list[event][id] = [];
            }

            alias = alias ? alias : id + '/' + event + '/' + callable.toString().hashCode();

            if (!ServiceListener.list[event][id].hasAlias(alias)) {
                ServiceListener.list[event][id].push({
                    alias: alias,
                    callable: callable
                });
            }

        },
        /**
         *
         * @param event
         * @param id
         * @param record
         * @param data
         * @param notifications
         */
        trigger: function (event, id, record, data, notifications) {

            if (App.debug) {
                console.log('trigger.request: "' + event + '" by "' + id + '"');
            }

            var calls = ServiceListener.list[event] ? ServiceListener.list[event] : [];
            for (var i in calls) {
                if (calls.hasOwnProperty(i) && i === id) {
                    var events = calls[i];
                    if (angular.isArray(events)) {
                        ServiceListener._run(events, record, data, notifications);
                        if (App.debug) {
                            console.log('trigger.called: "' + event + '" on "' + i + '"');
                        }
                    }
                }
            }
        },
        /**
         *
         * @param events
         * @param record
         * @param data
         * @param notifications
         * @private
         */
        _run: function (events, record, data, notifications) {

            events.forEach(function (event) {

                if (angular.isFunction(event.callable)) {

                    var primaries = ServiceModel.getPrimary();
                    event.callable.call(this, record, data, primaries, notifications);
                }
            });
        }
    };

    return ServiceListener;
}]);