/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceReport', ['$compile', '$timeout', '$window', 'ServiceModel', 'ServiceApi',
    function ($compile, $timeout, $window, ServiceModel, ServiceApi) {

        Number.prototype.padLeft = function(base,chr){
            var  len = (String(base || 10).length - String(this).length)+1;
            return len > 0? new Array(len).join(chr || '0')+this : this;
        };

        var ServiceReport = {
            /**
             * @var string
             */
            src: 'template/report.html',
            /**
             * @var string
             */
            orientation: 'portrait',
            /**
             * @var Object
             */
            data: {},
            /**
             * @var string
             */
            style: '',
            /**
             * @var string
             */
            time: '',
            /**
             * @var boolean
             */
            showTime: true,
            /**
             *
             */
            time: null,
            /**
             *
             */
            showTime: true,
            /**
             *
             * @param module
             * @param entity
             * @param index
             * @returns {ServiceReport}
             */
            create: function (module, entity, index) {

                ServiceReport.module = module;
                ServiceReport.entity = entity;
                ServiceReport.index = index;

                ServiceReport.data = [];

                ServiceReport.executed = false;
                ServiceReport.orientation = 'portrait';

                //return angular.copy(ServiceReport);
                return ServiceReport;
            },
            /**
             *
             * @param callback
             */
            init: function (callback) {

                $timeout(function () {

                    ServiceModel.record = {};
                    if (angular.isFunction(callback)) {
                        callback.call(this);
                    }
                }, 1000);
            },
            /**
             *
             */
            updateStyle: function () {
                ServiceReport.style = 'height: ' + ($(window).height() - 150) + 'px';
            },
            /**
             *
             * @param html
             */
            printable: function (html) {
                $('#printable').html(html);
            },
            /**
             *
             * @param executed
             */
            updateButtons: function (executed) {

                var positions = ['top', 'bottom'];

                positions.forEach(function (position) {
                    var toolbar = ServiceModel.getActions(ServiceReport.index, position);
                    toolbar.forEach(function (button) {
                        if (button.id === 'execute') {
                            button.hide = executed;
                        } else {
                            button.hide = !executed;
                        }
                    });
                });
            },
            /**
             *
             * @param record
             * @param noButtons
             */
            reportExecute: function (record, noButtons) {

                if (ServiceReport.showTime) {
                    ServiceReport.time = ServiceReport.getTime();
                }

                ServiceReport.executed = true;
                if (!noButtons) {
                    ServiceReport.updateButtons(true);
                }

                ServiceReport.updateStyle();

                ServiceApi.report(ServiceReport.module, ServiceReport.entity, 'template', [record ? record : ServiceModel.record], function (response) {

                    $('.report-container .report-body').html(response.data);

                    $timeout(function () {
                        ServiceReport.printable($('.report-container .report-page').html());
                    }, 500);
                });
            },
            /**
             *
             */
            reportBack: function () {

                ServiceReport.executed = false;
                ServiceReport.updateButtons(false);

                ServiceReport.printable('');
            },
            /**
             *
             */
            reportPrint: function () {

                ServiceReport.printable($('.report-container .report-page').html());
                $timeout(function () {
                    window.print();
                }, 500);
            },
            /**
             *
             */
            reportDownload: function () {

                var
                    html = $('#printable').find('.report-body').html().toString(),
                    content = App.removeDiacritics(html);

                var blob = new Blob([content], {type: "text/plain;charset=utf-8"});

                saveAs(blob, "report.xls");
            },
            /**
             *
             * @param $scope
             */
            register: function ($scope) {

                $scope.$parent.reportExecute = $scope.report.reportExecute;

                $scope.$parent.reportBack = $scope.report.reportBack;

                $scope.$parent.reportPrint = $scope.report.reportPrint;

                $scope.$parent.reportDownload = $scope.report.reportDownload;
            },
            /**
             *
             * @returns {string}
             */
            getTime: function() {
                var date = new Date;
                return [(date.getMonth() + 1).padLeft(),
                        date.getDate().padLeft(),
                        date.getFullYear()].join('/') + ' ' +
                    [date.getHours().padLeft(),
                        date.getMinutes().padLeft(),
                        date.getSeconds().padLeft()].join(':');
            }
        };

        //angular.element($window).bind('resize', function () {
        //    ServiceReport.updateStyle();
        //    console.log(ServiceReport.style);
        //});

        return ServiceReport;
    }]);