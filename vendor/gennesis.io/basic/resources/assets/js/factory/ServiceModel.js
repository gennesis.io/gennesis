/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceModel', ['$http', function ($http) {

    var ServiceModel = {
        /**
         * @var {object}
         */
        $scope: null,
        /**
         * @var {string}
         */
        module: '',
        /**
         * @var {string}
         */
        entity: '',
        /**
         * @var {string}
         */
        index: '',
        /**
         * @var {object}
         */
        models: {},
        /**
         * @var {object}
         */
        record: {},
        /**
         * @var {Array}
         */
        recordSet: [],
        /**
         *
         * @returns {string}
         */
        getId: function () {
            return ServiceModel.module + '.' + ServiceModel.entity;
        }
        ,
        /**
         *
         * @returns {*}
         */
        getModel: function () {
            var
                id = ServiceModel.getId();

            return ServiceModel.models[id];
        }
        ,
        /**
         *
         * @param index
         * @returns {*}
         */
        getOperation: function (index) {
            var
                model = ServiceModel.getModel();

            return (model && model.operations) ? model.operations[index] : null;
        }
        ,
        /**
         *
         * @returns {*}
         */
        getPrimary: function () {
            var
                primary = [],
                items = ServiceModel.getItems();

            if (angular.isArray(items)) {
                items.forEach(function (item) {
                    if (item.behavior === 'pk') {
                        primary.push(item.name);
                    }
                });
            }

            return primary;
        },
        /**
         *
         * @returns {*}
         */
        getItems: function () {
            var
                model = ServiceModel.getModel(),
                items = {};
            if (model && model.definition) {
                items = model.definition.items;
            }
            return items;
        },
        /**
         *
         * @param name
         * @returns {*}
         */
        getItem: function (name) {
            var
                items = ServiceModel.getItems(),
                item = {};

            angular.forEach(items, function (_item) {
                if (_item.name === name) {
                    item = _item;
                }
            });

            return item;
        },
        /**
         *
         * @returns {object}
         */
        getProperties: function () {
            var
                model = ServiceModel.getModel(),
                properties = {};
            if (model && model.definition) {
                properties = model.definition.properties;
            }
            return properties;
        },
        /**
         *
         * @param index
         * @param position
         */
        getActions: function (index, position) {
           var
               operation = ServiceModel.getOperation(index),
               actions = [];

            if (operation && operation.actions) {
                angular.forEach(operation.actions, function (action) {
                    var pos = parseInt(action.position[position]);
                    if (!isNaN(pos)) {
                        actions[pos] = action;
                    }
                });
            }

            return actions;
        },
        /**
         * @var {object}
         */
        format: {
            /**
             *
             * @param record
             * @param items
             * @returns {*}
             */
            front: function (record, items) {

                if (!items) {
                    var model = ServiceModel.getModel();
                    items = model.definition.items;
                }

                angular.forEach(items, function (field) {

                    if (record[field.name]) {
                        var value = record[field.name];
                        switch (field.type) {
                            case 'date':
                                if (!(value instanceof Date)) {
                                    record[field.name] = new Date(value + ' 02:00:00');
                                }
                                break;
                            case 'boolean':
                                record[field.name] = (value === '1' || value === true);
                                break;
                            case 'int':
                                if (!field.behavior) {
                                    record[field.name] = parseInt(value);
                                }
                                break;
                        }
                        switch (field.behavior) {
                            case 'fk':
                                if (record[field.name] === '0') {
                                    record[field.name] = '';
                                }
                                var fields = ServiceModel.getOperation(ServiceModel.index).fields;
                                if (fields) {
                                    fields.forEach(function (_field) {
                                        if (_field.key === field.name && _field.remote) {
                                            _field.remote.init(record[field.name]);
                                        }
                                    });
                                }
                                break;
                        }
                    }
                });

                return record;
            },
            /**
             *
             * @param record
             * @param items
             * @returns {*}
             */
            back: function (record, items) {

                if (!items) {
                    var model = ServiceModel.getModel();
                    items = model.definition.items;
                }

                angular.forEach(items, function (field) {
                    if (record[field.name]) {
                        switch (field.type) {
                            case 'date':
                                var value = record[field.name];
                                if (value instanceof Date) {
                                    record[field.name] =
                                        value.getFullYear() + '-' + value.getMonth() + '-' + value.getDay();
                                }
                                break;
                            case 'boolean':
                                record[field.name] = value ? '1' : '0';
                                break;
                        }
                    }
                });

                return record;
            }
        }
    };

    return ServiceModel;
}]);