/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceOutput', ['$http', function ($http) {

    var ServiceOutput = {
        /**
         *
         * @param value
         * @param type
         * @returns {*}
         */
        print: function (value, type) {

            type = type ? type : typeof value;

            if (value instanceof Date) {
                type = 'date';
            }

            switch (type) {
                case 'int':
                case 'input[number]':
                    value = value.indexOf('.') ? parseFloat(value) :  parseInt(value);
                    break;
                case 'date':
                case 'input[date]':
                    var
                        date = (value instanceof Date) ? value : new Date(value + ' 02:00:00'),
                        day = date.getDate(),
                        month = date.getMonth() + 1,
                        year = date.getFullYear();
                    if (!isNaN(day) && !isNaN(month) && !isNaN(year)) {
                        value = App.pad(day, 2) + '/' + App.pad(month, 2) + '/' + year;
                    } else {
                        value = '';
                    }
                    break;
                case 'download':
                case 'input[file]':
                    value = App.download + '/' + value;
                    break;
            }

            return value;
        },
        /**
         *
         * @param fields
         * @param record
         */
        resume: function (fields, record) {
            var resume = [];
            for (var i in record) {
                if (record.hasOwnProperty(i) && i !== '$$hashKey' && i !== '_id') {
                    resume.push(record[i]);
                }
            }
            return resume.join(' / ');
        }
    };

    return ServiceOutput;
}]);