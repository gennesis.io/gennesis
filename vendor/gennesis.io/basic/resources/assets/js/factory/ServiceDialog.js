/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */

App.factory('ServiceDialog', ['$http', function ($http) {

    return {
        /**
         *
         * @param title
         * @param text
         * @param confirmButtonText
         */
        alert: function (title, text, confirmButtonText) {
            swal({
                title: title,
                text: text,
                confirmButtonText: confirmButtonText ? confirmButtonText : 'Ok',
                confirmButtonColor: "#F44336"
            });
        },
        /**
         *
         * @param title
         * @param text
         * @param confirmButtonText
         * @param _function
         */
        confirm: function (title, text, confirmButtonText, _function) {
            swal({
                title: title,
                text: text,
                confirmButtonText: confirmButtonText,
                type: "warning",
                confirmButtonColor: "#F44336",
                cancelButtonText: "Cancelar",
                showCancelButton: true,
                closeOnConfirm: true
            }, function () {
                if (angular.isFunction(_function)) {
                    _function.call(this);
                }
            });
        }
    };
}]);