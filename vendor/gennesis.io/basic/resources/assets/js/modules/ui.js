App

// =========================================================================
// MALIHU SCROLL
// =========================================================================

//On Custom Class
    .directive('cOverflow', ['scrollService', function (scrollService) {
        return {
            restrict: 'C',
            link: function (scope, element) {

                if (!$('html').hasClass('ismobile')) {
                    scrollService.malihuScroll(element, 'minimal-dark', 'y');
                }
            }
        }
    }])

    // =========================================================================
    // WAVES
    // =========================================================================

    // For .btn classes
    .directive('btn', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                if (element.hasClass('btn-icon') || element.hasClass('btn-float')) {
                    Waves.attach(element, ['waves-circle']);
                }

                else if (element.hasClass('btn-light')) {
                    Waves.attach(element, ['waves-light']);
                }

                else {
                    Waves.attach(element);
                }

                Waves.init();
            }
        }
    })

    // =========================================================================
    // FORM
    // =========================================================================

    .directive('formField', function ($compile) {

        return {
            replace: false
            , restrict: 'E'
            , scope: {
                form: '=',
                items: '=',
                field: '=',
                record: '=',
                listener: '='
            }
            /**
             * @param $scope
             * @param $timeout
             * @param ServiceApi
             */
            , controller: ['$scope', '$timeout', 'ServiceApi', 'ServiceInput', 'ServiceOutput', 'ServiceModel', 'ServicePopup',
                function ($scope, $timeout, ServiceApi, ServiceInput, ServiceOutput, ServiceModel, ServicePopup) {

                    /**
                     *
                     * @type {{add: $scope.list.add, remove: $scope.list.remove}}
                     */
                    $scope.list = {
                        /**
                         *
                         * @param name
                         */
                        add: function (name) {

                            if (!angular.isArray($scope.record[name])) {
                                $scope.record[name] = [];
                            }
                            $scope.record[name].push({id: '', text: ''});
                        },
                        /**
                         *
                         * @param name
                         * @param option
                         */
                        remove: function (name, option) {

                            if (angular.isArray($scope.record[name])) {
                                $scope.record[name].splice($scope.record[name].indexOf(option), 1);
                            }
                        },
                        /**
                         *
                         * @param name
                         * @param template
                         */
                        addInline: function (name, template) {

                            if (!angular.isArray($scope.record[name])) {
                                $scope.record[name] = [];
                            }
                            $scope.record[name].push({});

                            if ($scope.field.formInline) {
                                if (!angular.isArray($scope.field.formInline.rows)) {
                                    $scope.field.formInline.rows = [];
                                }
                                $scope.field.formInline.rows.push(angular.copy(template));
                            }
                        },
                        /**
                         *
                         * @param name
                         * @param option
                         */
                        removeInline: function (name, option) {

                            const indexOf = $scope.record[name].indexOf(option);

                            if (angular.isArray($scope.record[name])) {

                                $scope.record[name].splice(indexOf, 1);

                                if ($scope.field.formInline) {
                                    $scope.field.formInline.rows.splice(indexOf, 1);
                                }
                            }

                            return indexOf;
                        }
                    };

                    /**
                     *
                     * @param $event
                     * @param record
                     * @param key
                     * @param name
                     */
                    $scope.mask = function ($event, record, key, name) {

                        record[key] = App.mask(name, record[key]);
                    };

                    /**
                     *
                     * @param $event
                     * @param record
                     * @param key
                     */
                    $scope.upper = function ($event, record, key) {
                        var value = record[key];
                        record[key] = value ? value.toString().toUpperCase() : '';
                    };

                    /**
                     *
                     * @param _money
                     * @param record
                     * @param key
                     */
                    $scope.money = function (_money, record, key) {

                        var
                            cents = _money[key] ? App.numbers(_money[key]) : '0',
                            raw = (parseInt(cents) / 100).toFixed(2),
                            formatted = App.format(raw);

                        record[key] = raw;
                        _money[key] = formatted;

                        return formatted;
                    };

                    /**
                     *
                     * @param input
                     * @param scope
                     */
                    $scope.file = function (input, scope) {
                        ServiceInput.file(scope.record, ServiceModel.module, ServiceModel.entity, scope.field.key, input.files);
                    };

                    /**
                     *
                     * @param value
                     * @param type
                     * @returns {*}
                     */
                    $scope.output = function (value, type) {
                        return ServiceOutput.print(value, type);
                    };

                    //noinspection JSUnresolvedVariable
                    if ($scope.field) {

                        switch ($scope.field.component) {

                            case 'indexed':
                            case 'input[date-interval]':

                                if (!$scope.record) {
                                    $scope.record = {};
                                }
                                if (!$scope.record[$scope.field.key]) {
                                    $scope.record[$scope.field.key] = {};
                                }
                                break;

                            case 'list-inline':
                            case 'list-view':

                                $scope.popup = ServicePopup;
                                $scope.EXPOSE = $scope.$parent.EXPOSE;
                                break;

                            case 'input[money]':

                                $scope.$watch("record['" + $scope.field.key + "']",
                                    function (newValue) {
                                        $scope._money[$scope.field.key] = App.format(newValue);
                                    });
                                break;

                            case 'select[local]':

                                if (!$scope.field.settings) {
                                    $scope.field.settings = {};
                                }
                                if (!$scope.field.settings.id) {
                                    $scope.field.settings.id = 'id';
                                }
                                if (!$scope.field.settings.text) {
                                    $scope.field.settings.text = 'text';
                                }
                                break;

                            case 'select[remote]':

                                var remote = {
                                    /**
                                     * ng-model
                                     */
                                    text: "",
                                    /**
                                     * @var {boolean}
                                     */
                                    visible: false,
                                    /**
                                     * store to original selected text
                                     */
                                    original: "",
                                    /**
                                     * temp storage to search term
                                     */
                                    searching: "",
                                    /**
                                     * @var {int}
                                     */
                                    position: -1,
                                    /**
                                     * @var {object}
                                     */
                                    relationship: $scope.field.relationship,
                                    /**
                                     * @var {int}
                                     */
                                    take: 10,
                                    /**
                                     * @var {int}
                                     */
                                    skip: 0,
                                    /**
                                     * @var {int}
                                     */
                                    total: 0,
                                    /**
                                     * @var {Array}
                                     */
                                    collection: [],
                                    /**
                                     * @var {boolean}
                                     */
                                    canHide: true,
                                    /**
                                     *
                                     * @param value
                                     */
                                    init: function (value) {

                                        if (value) {

                                            var
                                                show = '',
                                                search = [];

                                            search.push([remote.relationship.value, value]);

                                            remote._search(search, function (response) {

                                                var collection = response.rows,
                                                    selected = {};

                                                if (angular.isArray(collection)) {

                                                    if (collection.length) {

                                                        selected = collection[0];

                                                        show = selected[remote.relationship.show];

                                                        remote.original = show;
                                                        remote.text = show;
                                                    }
                                                }
                                            });
                                        }
                                    },
                                    /**
                                     *
                                     * @param searchable
                                     * @param callback
                                     * @private
                                     */
                                    _search: function (searchable, callback) {

                                        var
                                            relationship = remote.relationship;

                                        if (relationship.module && relationship.entity && relationship.operation) {

                                            var
                                                filters = [],
                                                sorters = null,
                                                aggregators = null,
                                                limiters = [remote.skip, remote.take],
                                                fields = null;

                                            if (typeof searchable === 'string') {

                                                remote.searching = searchable;

                                                if (relationship && relationship.show && searchable) {
                                                    filters.push([relationship.show, searchable, '*=']);
                                                }
                                            } else if (searchable) {

                                                filters = searchable;
                                            }


                                            ServiceApi.request(relationship.module, relationship.entity, relationship.operation,
                                                [filters, sorters, aggregators, limiters, fields],
                                                function (ss, ok, status, data) {

                                                    if (ok) {

                                                        remote.collection = data.rows;
                                                        remote.total = data.total;
                                                        remote.last = Math.ceil(data.total / remote.take);

                                                        if (angular.isFunction(callback)) {
                                                            callback.call(this, data);
                                                        }
                                                    }
                                                }
                                            );

                                        }

                                    },
                                    /**
                                     *
                                     */
                                    keyup: function (_event) {

                                        switch (_event.keyCode) {

                                            /* ESC */
                                            case 27:
                                                remote.hide();
                                                break;

                                            /* ARROW UP */
                                            case 38:
                                                remote.visible = true;
                                                remote.arrow(-1);
                                                break;

                                            /* ARROW DOWN */
                                            case 40:
                                                remote.visible = true;
                                                remote.arrow(1);
                                                break;

                                            /* ARROW RIGHT */
                                            case 39:
                                                if (remote.visible && remote.position > -1) {
                                                    remote.next();
                                                }
                                                break;

                                            /* ARROW LEFT */
                                            case 37:
                                                if (remote.visible && remote.position > -1) {
                                                    remote.previous();
                                                }
                                                break;

                                            /* ENTER */
                                            case 13:
                                                if (angular.isArray(remote.collection)) {
                                                    remote.collection.forEach(function (row) {
                                                        if (row.selected) {
                                                            remote.select(row);
                                                        }
                                                    });
                                                }
                                                break;

                                            default:
                                                remote.skip = 0;
                                                remote.searching = remote.text;
                                                remote._search(remote.searching);
                                                break;
                                        }
                                    },
                                    /**
                                     *
                                     */
                                    previous: function () {

                                        var search = false,
                                            skip = remote.skip - remote.take;

                                        if (skip < 0) {
                                            skip = 0;
                                        } else {
                                            search = true;
                                        }

                                        remote.skip = skip;

                                        if (search) {
                                            remote._search(remote.searching, function () {
                                                remote.position = -1;
                                                remote.arrow(1);
                                            });
                                        }

                                        remote.canHide = false;

                                        $timeout(function () {
                                            remote.canHide = true;
                                        }, 250);
                                    },
                                    /**
                                     *
                                     */
                                    next: function () {

                                        var search = false,
                                            skip = ((remote.skip / remote.take) + 1) * remote.take,
                                            last = (remote.last - 1) * remote.take;

                                        console.log(remote.skip, remote.take, remote.last);

                                        if (skip > last) {
                                            skip = last;
                                        } else {
                                            search = true;
                                        }

                                        remote.skip = skip;

                                        if (search) {
                                            remote._search(remote.searching, function () {
                                                remote.position = -1;
                                                remote.arrow(1);
                                            });
                                        }

                                        remote.canHide = false;

                                        $timeout(function () {
                                            remote.canHide = true;
                                        }, 250);
                                    },
                                    /**
                                     *
                                     * @param factor
                                     */
                                    arrow: function (factor) {

                                        var _arrow = function (arrow) {

                                            var index = (remote.position + arrow),
                                                min = 0,
                                                max = remote.collection.length - 1;

                                            if (index < min) {
                                                index = min;
                                            } else if (index > max) {
                                                index = max;
                                            }

                                            remote.position = index;

                                            //console.log(index, min, max);

                                            remote.collection.forEach(function (c) {
                                                c.selected = false;
                                            });

                                            if (remote.collection[index]) {
                                                remote.collection[index].selected = true;
                                            }
                                        };

                                        if (angular.isArray(remote.collection)) {

                                            if (remote.collection.length) {
                                                _arrow.call(this, factor);
                                            } else {
                                                remote._search(remote.searching, function () {
                                                    _arrow.call(this, factor);
                                                });
                                            }
                                        }
                                    },
                                    /**
                                     *
                                     * @param selected
                                     */
                                    select: function (selected) {

                                        remote.visible = false;

                                        var show = remote.relationship.show,
                                            value = remote.relationship.value;

                                        if (!$scope.record) {
                                            $scope.record = {};
                                        }
                                        $scope.record[$scope.field.key] = selected[value];
                                        remote.selected = selected;

                                        $scope.listener('change', $scope.items, $scope.field, $scope.record);

                                        remote.searching = "";
                                        remote.original = selected[show];
                                        remote.text = selected[show];
                                        remote.position = -1;

                                        remote.collection = [];
                                    },
                                    /**
                                     *
                                     */
                                    clear: function () {

                                        remote.selected = {};
                                        remote.text = '';
                                        $scope.record[$scope.field.key] = '';
                                    },
                                    /**
                                     *
                                     */
                                    focus: function (field) {

                                        remote.position = -1;
                                        if (!field.readonly && !field.disabled) {

                                            remote.visible = true;

                                            if (remote.searching) {

                                                remote.text = remote.searching;
                                            } else {

                                                if (remote.collection && remote.collection.length <= 1) {
                                                    remote._search("");
                                                }

                                                remote.original = remote.text;
                                            }
                                        }
                                    },
                                    /**
                                     *
                                     */
                                    blur: function () {

                                        $timeout(function () {
                                            if (remote.canHide) {

                                                remote.hide();
                                            } else {

                                                remote.canHide = true;
                                            }
                                        }, 200);

                                        if (remote.searching) {
                                            remote.text = remote.original;
                                        }
                                    },
                                    /**
                                     *
                                     */
                                    hide: function () {

                                        remote.position = -1;
                                        remote.visible = false;
                                    },
                                    /**
                                     *
                                     */
                                    add: function (text) {
                                        alert(text);
                                        // call api,
                                        // save the item,
                                        // get the id and put in the model
                                    },
                                    /**
                                     *
                                     * @param row
                                     * @returns {*}
                                     */
                                    item: function (row) {

                                        function preg_quote(str) {
                                            return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
                                        }

                                        var show = row[remote.relationship.show],
                                            search = remote.searching;

                                        if (search) {
                                            show = show.replace(new RegExp("(" + preg_quote(search) + ")", 'gi'), "<i>$1</i>");
                                        }
                                        return show;
                                    },
                                    /**
                                     *
                                     * @param target
                                     */
                                    prevent: function (target) {

                                        var e = angular.element(target)[0];

                                        if (remote.text) {

                                            if (e.createTextRange) {
                                                var range = e.createTextRange();
                                                range.move('character', remote.text.length);
                                                range.select();
                                            } else {
                                                if (e.setSelectionRange) {
                                                    e.setSelectionRange(remote.text.length, remote.text.length);
                                                }
                                            }
                                        }
                                    }
                                };

                                $scope.field.remote = remote;
                                break;
                        }
                    } else {
                        console.error($scope.field);
                    }

                }
            ],
            link: function (scope, element, attrs) {

                var key = scope.field.key;

                scope.id = key + '_' + App.uniqid();
                scope.error = 'Field is required';
                if (App.language) {
                    scope.error = App.lang('validation.error');
                }
                scope.hint = 'hint--top-right hint--rounded hint--bounce';
                scope._money = {};

                scope.getTemplateUrl = function () {
                    return App._url + '/static/app/template/' + scope.field.component + '.ng';
                };
            },
            //ng-class="field.wrapper" ng-hide="field.hidden"
            template: '<div ng-include="getTemplateUrl()"></div>'
        };
    });
