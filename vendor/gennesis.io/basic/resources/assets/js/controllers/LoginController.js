/**
 * LoginController
 */
App.controller('LoginController', ['$location', '$scope', '$timeout', 'ServiceApi', 'ServiceGrowl', 'ServiceDialog',
    function ($location, $scope, $timeout, ServiceApi, ServiceGrowl, ServiceDialog) {

        if (App.token) {
            ServiceApi.go('home');
        }

        $scope.logging = false;


        $scope.login = function (credential) {

            $scope.logging = true;

            ServiceApi.auth.login(credential, function (token) {
                if (token) {
                    ServiceApi.go('home');
                } else {
                    ServiceGrowl.growl('Usuário ou senha incorretos', 'inverse');
                }
                $scope.logging = false;
            });
        };


        $scope.logout = function () {

            ServiceApi.auth.logout(App.token);
        };


        $scope.password = function () {
            ServiceDialog.alert(
                'Qual a minha senha?',
                '<div style="text-align: justify; ">' +
                    'A sua senha inicial é formada pelos números da sua <b>Data de Nascimento</b>.' +
                    '<br>' +
                    'Por exemplo: se você nasceu dia <b>01/01/1980</b> sua senha será <b>01011980</b>.' +
                    '<br>' +
                    '<br>' +
                    'Caso queira atualizar a senha basta ir no menu Minha Conta e alterar para uma senha de sua confiança ' +
                    'ou utilizar o serviço ESQUECI MINHA SENHA caso tenha mudado a senha e não lembre' +
                '</div>',
                'Fechar');
        };

    }]);