App
// =========================================================================
// Base controller for common functions
// =========================================================================

    .controller('AppController', function ($timeout, $scope, $cookies, ServiceApi, ServiceGrowl) {

        $scope.permissions = [];

        ServiceApi.auth.init($scope);

        App.token = $cookies.get(App.ref);

        $scope.logged = App.token ? true : false;

        $scope.isLogged = function () {
            return App.token ? true : false;
        };

        if (App.token) {

            ServiceApi.auth.grant(App.token);

            if (App.exit) {

                var appEvent = window.attachEvent || window.addEventListener;
                var appAttach = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compitable

                // For >=IE7, Chrome, Firefox
                appEvent(appAttach, function (e) {
                    if (App.token) {
                        // a space
                        var confirmationMessage = App.exit;
                        (e || window.event).returnValue = confirmationMessage;
                        return confirmationMessage;
                    }
                });
            }
        }

        $scope.go = function (path) {
            ServiceApi.go(path);
        };

        $scope.fullScreen = function () {

            function launchIntoFullscreen(element) {

                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            function exitFullscreen() {

                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            } else {
                launchIntoFullscreen(document.documentElement);
            }
        };

        $scope.clearCache = function () {

            //Get confirmation, if confirmed clear the localStorage
            swal({
                title: "Tem certeza?",
                text: "Todas as suas preferências de tela serão apagadas",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Tudo bem, pode apagar!",
                closeOnConfirm: false
            }, function () {
                localStorage.clear();
                swal("Pronto!", "Todas as suas preferências foram apagadas", "success");
            });
        };


        $scope.logout = function () {

            ServiceApi.auth.logout(App.token);
        };
    })

    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('materialadminCtrl', function ($window) {

        // Detact Mobile Browser
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        };

        // By default template has a boxed layout
        //this.layoutType = '1';
        this.layoutType = localStorage.getItem('ma-layout-status') !== null ? localStorage.getItem('ma-layout-status') : '1';

        this.setLayoutType = function (type) {
            this.layoutType = type;
            localStorage.setItem('ma-layout-status', type);
        };

        //Close sidebar on click
        this.sidebarStat = function (event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        };

        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;

        this.lvSearch = function () {
            this.listviewSearchStat = true;
        };

        //Skin Switch
        this.currentSkin = 'teal';

        this.openWindow = function (url) {

            var
                h = 500,
                w = 990,
                height = $(window).height(),
                width = $(window).width(),
                t = (height - h) / 2,
                l = (width - w) / 2;
            $window.open(url, "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=" + t + ",left=" + l + ",width=" + w + ",height=" + h);
        }
    })


    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function ($timeout, messageService) {

        // Top Search
        this.openSearch = function () {
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        };

        this.closeSearch = function () {
            angular.element('#header').removeClass('search-toggled');
        };
    })

    //=================================================
    // CALENDAR
    //=================================================

    .controller('calendarCtrl', function ($modal) {

        //Create and add Action button with dropdown in Calendar header. 
        this.month = 'month';

        this.actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
            '<li class="dropdown" dropdown>' +
            '<a href="" dropdown-toggle><i class="zmdi zmdi-more-vert"></i></a>' +
            '<ul class="dropdown-menu dropdown-menu-right">' +
            '<li class="active">' +
            '<a data-calendar-view="month" href="">Month View</a>' +
            '</li>' +
            '<li>' +
            '<a data-calendar-view="basicWeek" href="">Week View</a>' +
            '</li>' +
            '<li>' +
            '<a data-calendar-view="agendaWeek" href="">Agenda Week View</a>' +
            '</li>' +
            '<li>' +
            '<a data-calendar-view="basicDay" href="">Day View</a>' +
            '</li>' +
            '<li>' +
            '<a data-calendar-view="agendaDay" href="">Agenda Day View</a>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</li>';


        //Open new event modal on selecting a day
        this.onSelect = function (argStart, argEnd) {
            var modalInstance = $modal.open({
                templateUrl: 'addEvent.html',
                controller: 'addeventCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    calendarData: function () {
                        var x = [argStart, argEnd];
                        return x;
                    }
                }
            });
        }
    })

    //Add event Controller (Modal Instance)
    .controller('addeventCtrl', function ($scope, $modalInstance, calendarData) {

        //Calendar Event Data
        $scope.calendarData = {
            eventStartDate: calendarData[0],
            eventEndDate: calendarData[1]
        };

        //Tags
        $scope.tags = [
            'bgm-teal',
            'bgm-red',
            'bgm-pink',
            'bgm-blue',
            'bgm-lime',
            'bgm-green',
            'bgm-cyan',
            'bgm-orange',
            'bgm-purple',
            'bgm-gray',
            'bgm-black'
        ];

        //Select Tag
        $scope.currentTag = '';

        $scope.onTagClick = function (tag, $index) {
            $scope.activeState = $index;
            $scope.activeTagColor = tag;
        };

        //Add new event
        $scope.addEvent = function () {
            if ($scope.calendarData.eventName) {

                //Render Event
                $('#calendar').fullCalendar('renderEvent', {
                    title: $scope.calendarData.eventName,
                    start: $scope.calendarData.eventStartDate,
                    end: $scope.calendarData.eventEndDate,
                    allDay: true,
                    className: $scope.activeTagColor

                }, true); //Stick the event

                $scope.activeState = -1;
                $scope.calendarData.eventName = '';
                $modalInstance.close();
            }
        };

        //Dismiss 
        $scope.eventDismiss = function () {
            $modalInstance.dismiss();
        }
    });
