<?php
/*
 -------------------------------------------------------------------
 | @project: php@sistemaconcursos.com.br
 | @package: Apocalipse\basic\presentation\data
 | @file: Report.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 05/06/16 23:58
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Presentation\Data;


use Apocalipse\Core\Dao\Aggregator;
use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Join;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Dao\Sorter;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\Definition\Field;
use Apocalipse\Core\Domain\ModelManager;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Dynamo;
use Apocalipse\Core\Model\Type\Notification;

/**
 * Class Report
 * @package Apocalipse\Basic\Presentation\Data
 */
abstract class Report extends Dynamo
{
    /**
     * @var Record
     */
    protected $fields;

    /**
     * @var ModelManager
     */
    protected $model;

    /**
     * @var mixed
     */
    protected $resources;

    /**
     * @var Filter
     */
    protected $filters;

    /**
     * @var Sorter
     */
    protected $sorters;

    /**
     * @var Aggregator
     */
    protected $aggregators;

    /**
     * @var array
     */
    protected $limiters;

    /**
     * @var Join
     */
    protected $joins;

    /**
     * @param Record $record
     * @return mixed
     */
    protected abstract function beforeQuery(Record $record);

    /**
     * @param Collection $collection
     * @param Record $fields
     * @param Modifier $modifier
     * @return array
     */
    protected function query(Collection $collection, Record $fields, Modifier $modifier)
    {
        return $this->getDao()->read($collection, $fields, $modifier, $this->debug);
    }

    /**
     * @param Record $record
     * @return \Apocalipse\Core\Domain\Data\RecordSet
     */
    public function run($record = null)
    {
        $this->beforeQuery($record);

        $collection = new Collection($this->model->getCollection()->getName());

        $fields = $this->fields;
        if (!$fields) {
            $fields = [];
            /** @var Field $item */
            foreach ($this->model->getItems() as $item) {
                $fields[$item->getName()] = '';
            }
        }

        $modifier = $this->modifier;
        if (!$modifier) {
            $modifier = new Modifier($this->filters, $this->sorters, $this->aggregators, $this->limiters, $this->joins);
        }

        return $this->afterQuery($this->query($collection, $fields, $modifier));
    }

    /**
     * @param array $data
     * @return array
     */
    protected function afterQuery(array $data)
    {
        $notifications = Wrapper::getNotifications();

        if (is_array($notifications) && count($notifications)) {

            $errors = [];
            foreach ($notifications as $notification) {
                /** @var Notification $notification */
                $errors[] = $notification->getDescription();
            }

            if ($this->debug) {
                dd($errors);
            }
        }
        return $data;
    }

    /**
     * @param $array
     * @param $key
     * @param string $records
     * @return array
     */
    protected function indexByKey($array, $key, $records = 'records')
    {
        $indexed = [];

        foreach ($array as $position => $item) {

            if (isset($item[$key])) {

                $indexer = $item[$key];
                unset($item[$key]);

                $index = md5($indexer);

                if (!isset($indexed[$index])) {
                    $indexed[$index] = [$key => $indexer, $records => [$item]];
                } else {
                    $indexed[$index][$records][] = $item;
                }

            } else if (isset($item[$records])) {

                $item[$records] = $this->indexByKey($item[$records], $key, $records);

                $indexed[] = $item;
            }
        }

        return $indexed;
    }

    /**
     * @return Record
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param Record $fields
     * @return Report
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return ModelManager
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param ModelManager $model
     * @return Report
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param mixed $resources
     * @return Report
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param mixed $filters
     * @return Report
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSorters()
    {
        return $this->sorters;
    }

    /**
     * @param mixed $sorters
     * @return Report
     */
    public function setSorters($sorters)
    {
        $this->sorters = $sorters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAggregators()
    {
        return $this->aggregators;
    }

    /**
     * @param mixed $aggregators
     * @return Report
     */
    public function setAggregators($aggregators)
    {
        $this->aggregators = $aggregators;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLimiters()
    {
        return $this->limiters;
    }

    /**
     * @param mixed $limiters
     * @return Report
     */
    public function setLimiters($limiters)
    {
        $this->limiters = $limiters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJoins()
    {
        return $this->joins;
    }

    /**
     * @param mixed $joins
     * @return Report
     */
    public function setJoins($joins)
    {
        $this->joins = $joins;
        return $this;
    }

}