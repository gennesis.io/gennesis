<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Dao
 | @file: Simple.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 10:15
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Dao;


use Apocalipse\Core\Dao\Driver;
use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Helper\Text;

/**
 * Class Simple
 * @package Apocalipse\Basic\Dao
 */
class SimpleJson extends Driver
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $extension = 'json';

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var bool
     */
    private $transactionOn = true;

    /**
     * SimpleJson constructor.
     * @param string $dsn
     * @param string $user
     * @param string $password
     * @param array $parameters
     */
    public function __construct($dsn, $user, $password, $parameters = [])
    {
        parent::__construct($dsn, $user, $password, $parameters);

        $this->path = $dsn;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return string
     */
    public function create(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $filename = $this->getFileName($collection->getName());

        $all = $this->get($filename);
        $id = $collection->getId();
        $auto_increment = 1;

        if (count($all)) {
            $last = $all[count($all) - 1];
            if (isset($last->$id)) {
                $auto_increment = $last->$id + 1;
            }
        }

        $record->inject($id, (string) $auto_increment);
        $all[] = (object) $record->all();

        $created = $this->set($filename, $all);
        if ($created) {
            $created = $auto_increment;
        }

        return (string) $created;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return array
     */
    public function read(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $filename = $this->getFileName($collection->getName());

        $all = $this->get($filename);

        $filters = $modifier->getFilters();

        $all = array_filter($all, function ($value) use ($filters) {
            $match = true;
            if (is_array($filters) && count($filters)) {

                $try = 0;
                $done = 0;

                foreach ($filters as $filter) {

                    if ($filter instanceof Filter) {

                        $try++;

                        /** @var Filter $filter */
                        $identifier = $filter->identifier;
                        if (Text::indexOf($identifier, '.')) {
                            $identifier = explode('.', $identifier)[1];
                        }
                        if (isset($value->$identifier)) {
                            switch ($filter->operator) {
                                case '!=':
                                    if ($value->$identifier !== $filter->value) {
                                        $done++;
                                    }
                                    break;
                                case '*=':
                                    if (strpos($value->$identifier, $filter->value) !== false) {
                                        $done++;
                                    }
                                    break;
                                case 'in':
                                    if (in_array($value->$identifier, $filter->value, true)) {
                                        $done++;
                                    }
                                    break;
                                default:
                                    if ($value->$identifier === $filter->value) {
                                        $done++;
                                    }
                                    break;
                            }
                        } else {
                            $done++;
                        }
                    } else {
                        $done++;
                        Wrapper::info('Operator "' . ((string) $filter) . '" is not supported yet');
                    }
                }

                $match = $try === $done;
            }

            return $match;
        });

        $limiters = $modifier->getLimiters();
        if (is_array($limiters) && count($limiters) === 2) {
            $all = array_slice($all, $limiters[0], $limiters[1]);
        }

        $read = [];

        foreach ($all as $item) {
            $read[] = $item;
        }

        return $read;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public function update(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $filename = $this->getFileName($collection->getName());

        $all = $this->get($filename);

        $id = $collection->getId();

        $update = false;

        foreach ($all as $key => $row) {
            $primary = $record->get($id, true);
            if (isset($row->$id)) {
                if ($row->$id === $primary) {
                    $update = true;
                    $all[$key] = $record->all();
                    break;
                }
            } else {
                //Wrapper::info($row);
            }
        }

        if ($update) {
            $this->set($filename, $all);
        }

        return $update;
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public function delete(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        $filename = $this->getFileName($collection->getName());

        $all = $this->get($filename);
        $id = $collection->getId();

        $delete = false;

        foreach ($all as $index => $item) {
            if ($item->$id === $record->get($id, true)) {
                unset($all[$index]);
                if ($this->set($filename, $all)) {
                    $delete = true;
                }
                break;
            }
        }

        return $delete;
    }

    /**
     * @return bool
     */
    public function transactionBegin()
    {
        return $this->transactionOn = true;
    }

    /**
     * @return bool
     */
    public function transactionCommit()
    {
        $try = 0;
        $done = 0;

        foreach ($this->data as $filename => $data) {

            if ($this->in($filename, $data)) {
                $done++;
            }
        }

        return $try === $done;
    }

    /**
     * @return bool
     */
    public function transactionRollBack()
    {
        $this->data = [];
    }

    /**
     * @param $filename
     * @return array
     */
    private function get($filename)
    {
        if ($this->transactionOn) {
            if (isset($this->data[$filename])) {
                $get = $this->data[$filename];
            } else {
                $get = $this->out($filename);
                $this->data[$filename] = $get;
            }
        } else {
            $get = $this->out($filename);
        }

        return $get;
    }

    /**
     * @param $filename
     * @param $data
     * @return bool|int
     */
    private function set($filename, $data)
    {
        $set = false;
        if ($this->transactionOn) {
            if ($this->data[$filename] = $data) {
                $set = true;
            }
        } else {
            $set = $this->in($filename, $data);
        }

        return $set;
    }

    /**
     * @param $filename
     * @return array
     */
    private function out($filename)
    {
        $out = [];

        if (File::exists($filename)) {

            $data = (array)Json::decode(File::read($filename));
            if ($data) {
                foreach ($data as $row) {
                    $out[] = $row;
                }
            }
        } else {
            $this->error("File '" . $filename . "' not found");
        }

        return $out;
    }

    /**
     * @param $filename
     * @param $data
     * @return bool|int
     */
    private function in($filename, $data)
    {
        $in = false;

        $options = 0;

        $parameters = (object) $this->getParameters();
        if (isset($parameters->pretty) && $parameters->pretty) {
            $options = JSON_PRETTY_PRINT;
        }

        if (File::isWritable($filename)) {
            $in = File::write($filename, Json::encode($data, $options));
        } else {
            $this->error("File '" . $filename . "' can not be written");
        }

        return $in;
    }

    /**
     * @param $collection
     * @return string
     */
    private function getFileName($collection)
    {
        $filename = path(true, $this->path, $collection . '.' . $this->extension);
        if ($this->getParameters()) {
            $parameters = (object) $this->getParameters();
            if (isset($parameters->external) && $parameters->external) {
                $filename = path($this->path, $collection . '.' . $this->extension);
            }
        }
        return $filename;
    }
}