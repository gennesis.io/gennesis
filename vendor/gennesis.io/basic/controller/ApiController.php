<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: ApiController.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 14:20
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Data\RecordSet;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Dynamo;

/**
 * Class ApiController
 * @package Apocalipse\Basic\Controller
 */
class ApiController extends DataController
{
    /**
     * @var Dynamo
     */
    private $model;

    /**
     * @var bool
     */
    private $skeleton = false;

    /**
     * ApiController constructor.
     * @param null $module
     * @param string $entity
     */
    public function __construct($module, $entity)
    {
        parent::__construct(null);

        $use = pathToNamespace(implode('/', ['',environment('scope.namespace'), 'Model', dashesToCamelCase($module), dashesToCamelCase($entity)]));

        if (class_exists($use)) {

            $this->model = new $use();

            $this->skeleton = true;
        } else {

            $this->model = new Dynamo($module, $entity, null, null, null);
        }
    }

    /**
     * @param $route
     * @param Data $data
     * @return mixed
     */
    public function recover($route, Data $data)
    {
        if (!$this->skeleton) {
            $this->model->skeleton();
        }

        $parameters = $data->getPayload()->all();

        foreach ($parameters as $key => $parameter) {
            if (is_object($parameter)) {
                $record = new Record((array) $parameter);
                $parameters[$key] = $record;
            }
        }

        $rendered = $this->model->call($route, $parameters);

        if ($rendered instanceof RecordSet) {

            $total = $rendered->size();

            if (count($parameters) > 3) {

                $id = $this->model->getId();

                $read = $this->model->read($parameters[0], $parameters[1], $parameters[2], null, [$id => '']);

                $total = $read->size();
            }

            $rendered = (object) ['rows' => $rendered->getRecords(), 'total' => $total];
        }

        return $rendered;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model->getScaffold();
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->model->getPathName();
    }
}