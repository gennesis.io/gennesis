<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: FileReader.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:01
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Domain\Controller;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class FileReaderController
 * @package Apocalipse\Basic\Controller
 */
class FileReaderController extends Controller
{
    /**
     * @param $route
     * @param Data $data
     * @return mixed|string
     */
    public function render($route, Data $data)
    {
        $filename = path($this->context, implode(DIRECTORY_SEPARATOR, $route));
        $extension = File::extension($filename);

        $type = 'text/html';
        $content = '';

        if (File::exists($filename)) {

            $content = File::read($filename);

            switch ($extension) {
                case 'css':
                    $type = "text/css";
                    break;
                case 'js':
                    $type = "application/javascript";
                    break;
                default:
                    $type = File::mimeType($filename);
                    break;
            }
        } else {
            Wrapper::err("File '" . $filename . "' not found");
        }

        $info = [];

        $info['content-disposition'] = File::name($filename);

        if ($data) {

            $get = $data->getGet();
            if ($get && $get->name) {

                $info['content-disposition'] = urlencode($get->name) . '.' . $extension;
            }
        }
        $info['content-length'] = filesize($filename);

        return new Container($content, $type, $info);
    }

}