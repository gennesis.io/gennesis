<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: DataController.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:44
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Domain\Controller;

/**
 * Class DataController
 * @package Apocalipse\Basic\Controller
 */
abstract class DataController extends Controller
{
    /**
     * @param $route
     * @param Data $data
     * @return mixed
     */
    protected abstract function recover($route, Data $data);

    /**
     * @param $route
     * @param Data $data
     * @return Container
     */
    public final function render($route, Data $data)
    {
        $content = $this->recover($route, $data);

        return new Container($content, Container::TYPE_JSON);
    }
}