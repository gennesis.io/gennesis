<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Controller
 | @file: Template.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:42
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Controller;


use Apocalipse\Basic\Presentation\Data\Report;
use Apocalipse\Core\Domain\Content\Scope;
use Apocalipse\Core\Domain\Data\Record;

/**
 * Class ReportController
 * @package Apocalipse\Basic\Controller
 */
class ReportController extends ViewController
{
    /**
     * @var string
     */
    private $template = 'template';

    /**
     * ViewController constructor.
     * @param Record $context
     * @param string $views
     * @param string $suffix
     * @param string $design
     */
    public function __construct(Record $context, $views, $suffix, $design = null)
    {
        parent::__construct($context, $views, $suffix, $design);
    }

    /**
     * @param Scope $scope
     * @return Scope
     */
    public function scope(Scope $scope)
    {
        $content = $this->getContext();
        $report = null;

        if ($content instanceof Record) {

            $use = $this->defineUse($content);

            if ($use) {

                $payload = $scope->data->getPayload()->all();

                /** @var Report $model */
                $model = new $use();
                $report = $model->run(new Record((array) $payload[0]));

                $scope->inject('resources', $model->getResources());
            }
        }
        $scope->inject('report', $report);

        return $scope;
    }

    /**
     * @param $route
     * @param $data
     * @return mixed|null
     */
    protected function template($route, $data)
    {
        /** @var Record $context */
        $context = $this->getContext();

        return "{$context->get('module')}/{$context->get('entity')}/{$this->template}";
    }

    /**
     * @param $record
     * @return string
     */
    private function defineUse($record)
    {
        $use = '';

        /** @var Record $record */
        $scaffold = (object)$record->all();

        if (isset($scaffold->scope) && isset($scaffold->scope->namespace)) {

            $namespace = $scaffold->scope->namespace;
            $layer = dashesToCamelCase($scaffold->layer);
            $module = dashesToCamelCase($scaffold->module);
            $entity = dashesToCamelCase($scaffold->entity);

            if ($namespace && $layer && $module && $entity) {

                $use = "\\{$namespace}\\{$layer}\\{$module}\\{$entity}";
            }
        }

        return $use;
    }
}