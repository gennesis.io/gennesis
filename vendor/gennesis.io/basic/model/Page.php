<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Definition
 | @file: Page.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 15:59
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Basic\Model;


use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\ModelManager;
use Apocalipse\Core\Domain\Definition\Field;
use Apocalipse\Core\Domain\Data\Record;

/**
 * Class Page
 * @package Apocalipse\Basic\Definition
 */
class Page extends ModelManager
{
    /**
     * Page constructor.
     */
    public function __construct()
    {
        parent::__construct(new Collection('pages', 'id'), 'json');

        $this->add(new Field($this, 'path', 'string'));
        $this->add(new Field($this, 'template', 'string'));
        $this->add(new Field($this, 'controller', 'string'));
        $this->add(new Field($this, 'brook', 'boolean'));
        $this->add(new Field($this, 'cache', 'boolean'));
        $this->add(new Field($this, 'cache_time', 'int'));
    }
}