<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: error.html.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 22:33
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Notification;

if (__APP_TEST__) {

    switch ($type) {

        case 'overload':
            ?>
            :(
            <?php
            break;

        case 'json/api':

            header('Content-type: application/json');

            http_response_code(200);

            echo Wrapper::toJson();

            break;

        default:

            $notifications = Wrapper::getNotifications();

            if (count($notifications)) {

                ?>
                <style>
                    body {
                        padding: 20px 10%;
                    }

                    .card {
                        margin: 0 0 30px 0;
                        border-radius: 2px;
                        box-shadow: 0 2px 5px rgba(0, 0, 0, .16);
                        border: 1px solid #D0D0D0;
                        padding: 4px;
                    }

                    table {
                        table-layout: fixed;
                    }

                    table, th, td {
                        border: 1px solid #D0D0D0;
                        border-collapse: collapse;
                    }

                    th {
                        background: #F1EFEF;
                    }

                    th, td {
                        text-align: left;
                        padding: 4px;
                        word-wrap: break-word;
                        white-space: normal;
                    }
                </style>
                <h2>Collected notifications on Wrapper</h2>
                <?php
                /** @var Notification $error */
                foreach ($notifications as $notification) {
                    ?>
                    <div class="card">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <th width="50%">File</th>
                                <th width="5%">Line</th>
                                <th width="45%">Message</th>
                            </tr>
                            <tr>
                                <td><?php out($notification->getFile()); ?></td>
                                <td><?php out($notification->getLine()); ?></td>
                                <td><?php out($notification->getMessage()); ?></td>
                            </tr>
                            <tr>
                                <th>Class</th>
                                <th colspan="2">Function</th>
                            </tr>
                            <?php
                            $stack = $notification->getStack();
                            ?>
                            <tr>
                                <td><?php out(isset($stack->class) ? $stack->class : 'NULL'); ?></td>
                                <td colspan="2"><?php out(isset($stack->function) ? $stack->function : 'NULL'); ?></td>
                            </tr>
                        </table>
                    </div>
                    <?php
                }
            }
            break;
    }

}