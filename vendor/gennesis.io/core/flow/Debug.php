<?php
/*
 -------------------------------------------------------------------
 | @project: php@ambiental.site
 | @package: Apocalipse\Core\Flow
 | @file: Debug.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 28/08/16 12:40
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Flow;


/**
 * Class Debug
 * @package Apocalipse\Core\Flow
 */
class Debug
{
    /**
     * @return string
     */
    public static function trace()
    {
        return "TODO: catch stack trace!";
    }
}