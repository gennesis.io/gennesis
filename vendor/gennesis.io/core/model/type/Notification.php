<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Type
 | @file: Message.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 18:29
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;


use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Log\NotificationTrace;

/**
 * Class Message
 * @package Apocalipse\Core\Type
 */
class Notification extends Warning
{
    use NotificationTrace;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $stack;

    /**
     * Message constructor.
     * @param string $message
     * @param string $status
     * @param null $file
     * @param null $line
     */
    public function __construct($message, $status, $file = null, $line = null)
    {
        if (!$file || !$line) {

            //if ($status === Wrapper::STATUS_ERROR) {

            $this->stack = $this->stack(['Wrapper', 'Result', 'Notification', 'Driver']);

            $file = $this->stack->file;
            $line = $this->stack->line;
            //}
        }

        parent::__construct($message, $file, $line);

        $this->status = iif($status, Wrapper::STATUS_ERROR);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStack()
    {
        return $this->stack;
    }

    /**
     * @return array
     */
    public function expose()
    {
        return get_object_vars($this);
    }
}