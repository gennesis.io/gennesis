<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Definition\Type
 | @file: Scaffold.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 09/04/16 03:53
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;


use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class Scaffold
 * @package Apocalipse\Core\Definition\Type
 */
class Scaffold
{
    /**
     * @var object
     */
    public $definition;

    /**
     * @var array
     */
    public $operations;

    /**
     * @var array
     */
    public $language;

    /**
     * Scaffold constructor.
     * @param $scaffold
     */
    public function __construct($scaffold)
    {
        $extension = '.json';

        $definition = null;
        $file = path($scaffold, 'definition' . $extension);
        if (File::exists($file)) {

            $definition = Json::decode(File::read($file));
        } else {
            Wrapper::append("File '" . $file . "' not found at load Scaffold");
        }

        $language = Json::decode(File::read(path($scaffold, 'language', environment('language') . $extension)));

        $actions = $this->operations(path($scaffold, 'action'));
        $forms = $this->operations(path($scaffold, 'form'));

        $operations = [];

        $merge = array_merge($actions, $forms);
        foreach ($merge as $key => $merged) {

            if (sif($merged, 'id')) {
                $operations[$merged->id] = $merged;
            } else {
                Wrapper::info("Operation '" . $key . "' not loaded because the piece does not have ID");
            }
        }

        $this->definition = $definition;
        $this->operations = $operations;
        $this->language = $language;
    }

    /**
     * @param $dir
     * @return array
     */
    private function operations($dir)
    {
        $objects = [];
        foreach (new \DirectoryIterator($dir) as $file) {
            if ($file->isDot() || in_array($file->getFilename(), ['.gitkeep'])) {
                continue;
            }
            $objects[$file->getPathname()] = Json::decode(File::read($file->getPathname()));
        }
        return $objects;
    }

}