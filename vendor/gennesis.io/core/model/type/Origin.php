<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Type\Definition
 | @file: Origin.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 10/04/16 08:45
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;


/**
 * Class Origin
 * @package Apocalipse\Core\Model\Type
 */
class Origin
{
    /**
     * @return array
     */
    public function expose()
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode(get_object_vars($this));
    }
}