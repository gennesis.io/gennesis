<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Log
 | @file: DebugTrace.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 19:56
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type\Log;


use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Text;

/**
 * Class NotificationTrace
 * @package Apocalipse\Core\Model\Type\Log
 */
trait NotificationTrace
{
    /**
     * @param $parents
     * @return object
     */
    public function stack($parents)
    {
        $last = (object)[
            'class' => '',
            'function' => '',
            'file' => '',
            'line' => '',
            'object' => '',
            'type' => '',
            'args' => ''
        ];
        $debug_backtrace = debug_backtrace();

        if (is_array($debug_backtrace)) {

            $next = false;
            foreach ($debug_backtrace as $debug) {

                $name = $this->parseName($debug);

                $parent = in_array($name, $parents);

                if (!$parent && $next) {

                    foreach ($last as $key => $value) {

                        $last->$key = isset($debug[$key]) ? $debug[$key] : $value;
                    }
                    break;
                }
                if (!$parent) {
                    $next = true;
                }

            }
        }

        return $last;
    }

    /**
     * @param $debug
     * @return string
     */
    private function parseName($debug)
    {
        $name = '';

        if (isset($debug['file'])) {

            $filename = File::name($debug['file']);
            $name = Text::replaceLast($filename, '.' . File::extension($filename), '');
        } else if (isset($debug['class'])) {
            $name = File::name(namespaceToPath($debug['class']));
        }

        return $name;
    }
}