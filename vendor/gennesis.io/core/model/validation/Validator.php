<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Model\Validation
 | @file: Validator.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 09/04/16 03:21
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Validation;


use Apocalipse\Core\Dao\DaoManager;
use Apocalipse\Core\Domain\Definition\Field;

/**
 * Class Validator
 * @package Apocalipse\Core\Model\Validation
 */
abstract class Validator extends DaoManager
{
    /**
     * @var Field
     */
    public $field;

    /**
     * @var array
     */
    public $parameters;

    /**
     * Validator constructor.
     * @param Field $field
     * @param null $parameters
     */
    public function __construct(Field $field, $parameters = null)
    {
        parent::__construct($field->getAuthority()->getDriver());
    }
}