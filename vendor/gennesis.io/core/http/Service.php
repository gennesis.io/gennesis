<?php
/**
 * @package Http
 * --------------------------------------------
 * @project ...: apocalipse
 * @user ......: william
 * @date ......: 13/03/16
 * @hour ......: 08:51
 * --------------------------------------------
 * @file ......: Route.php
 * @copyright .: .gennesis.io | .dracones.io
 * @license ...: MIT
 * --------------------------------------------
 */

namespace Apocalipse\Core\Http;


use Apocalipse\Core\App;
use Apocalipse\Core\Domain\Security\Auth;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class Service
 * @package Apocalipse\Core\Http
 */
abstract class Service extends App
{
    /***
     * @param \Apocalipse\Core\Http\Request $request
     * @return \Apocalipse\Core\Http\Response
     */
    public abstract function render(Request $request);

    /**
     * @param $headers
     * @return bool
     */
    protected final function isAllowed($headers)
    {
        return Auth::isHeaderAllowed($headers);
    }
}