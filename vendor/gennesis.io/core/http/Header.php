<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Http
 | @file: Header.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 02:37
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Http;

use Apocalipse\Core\Model\Type\Object;

class Header extends Object
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $value;
    /**
     * @var bool
     */
    private $replace;

    /**
     * Header constructor.
     * @param string $key
     * @param string $value
     * @param bool $replace
     */
    public function __construct($key, $value, $replace = true)
    {
        parent::__construct();

        $this->key = $key;
        $this->value = $value;
        $this->replace = $replace;
    }

    /**
     * @return string
     */
    public function getAssign()
    {
       return $this->key . ':' . $this->value;
    }

    /**
     * @return bool
     */
    public function getReplace()
    {
        return $this->replace;
    }
}