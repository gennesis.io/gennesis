<?php

namespace Apocalipse\Core\Http;


/**
 * Class Cookie
 * @package Apocalipse\Core\Http
 */
abstract class Cookie
{
    /**
     * setcookie( string $name
     * [, string $value
     * [, int $expire = 0
     * [, string $path
     * [, string $domain
     * [, bool $secure = false
     * [, bool $httponly = false
     * ]]]]]])
     * @param $name
     * @param $value
     * @param $days
     * @param string $path
     */
    public static function set($name, $value, $days = 1, $path = "/")
    {
        $days = time() + ($days * 24 * 60 * 60);
        if ($days > 9999) {
            $days = 9999;
        }
        setcookie($name, $value, $days, $path);
    }

    /**
     * @param $name
     * @param string $default
     * @return string
     */
    public static function get($name, $default = '')
    {
        return sif($_COOKIE, $name, $default);
    }

}