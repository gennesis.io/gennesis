<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Helper
 | @file: Converter.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 14/04/16 07:29
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Helper;

use Apocalipse\Core\Flow\Wrapper;


/**
 * Class Converter
 * @package Apocalipse\Core\Helper
 */
class Converter
{
    /**
     * @param $date
     * @param bool $week
     * @param bool $first
     * @return string
     */
    public static function dateLikeText($date, $week = true, $first = false)
    {
        $formats = [
            'pt-br' => '{{day}} de {{month}} de {{year}}'
        ];

        $week_day = "";
        if ($week) {
            $week_day = self::dateWeekLikeText(Date::ymd_TO_dmy($date)) . ", ";
        }

        $values = explode("/", $date);
        $text = "";

        if (count($values) > 1) {

            $day = (int)$values[0];
            if ($first && $day == 1) {
                $day = "1°";
            }
            $month = $values[1];
            $year = $values[2];

            $replaces = [
                'day' => $day, 'month' => self::monthLikeText($month), 'year' => $year
            ];

            $text = $week_day . self::replace($formats['pt-br'], $replaces);
        } else {

            Wrapper::info("Invalid date (" . $date . ")");
        }

        return $text;
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public static function dateWeekLikeText($date, $format = 'br')
    {

        $text = "";
        $separator = "/";
        $d = 0;
        $m = 1;
        $y = 2;
        if ($format != 'br') {
            $separator = "-";
            $d = 2;
            $m = 1;
            $y = 0;
        }

        $var = explode($separator, $date);

        $day = "";
        if (isset($var[$d])) {
            $day = $var[$d];
        }
        $month = "";
        if (isset($var[$m])) {
            $month = $var[$m];
        }
        $year = "";
        if (isset($var[$y])) {
            $year = $var[$y];
        }

        if ($month and $day and $year) {
            $week = date("w", mktime(0, 0, 0, $month, $day, $year));
            $text = self::weekLikeText($week);
        }

        return $text;
    }

    /**
     * @param $month
     * @return string
     */
    public static function monthLikeText($month)
    {
        $months = [
            "pt-BR" => [
                "janeiro", "fevereiro", "março", "abril", "maio", "junho",
                "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"
            ]
        ];

        $text = sif((int)$month, $months["pt-BR"]);

        return $text;
    }

    /**
     * @param $day
     * @return string
     */
    public static function weekLikeText($day)
    {
        $days = [
            "pt-BR" => [
                "domingo", "segunda", "terça", "quarta", "maio", "quinta", "sexta", "sábado"
            ]
        ];

        $text = sif((int)$day, $days["pt-BR"]);

        return $text;
    }

    /**
     * @param int $valor
     * @param bool $maiusculas
     * @param string $tipo
     * @return string
     */
    public static function currencyToText($valor = 0, $maiusculas = false, $tipo = "")
    {

        if ($tipo == "number") {
            $singular = array("", "", "", "", "", "", "");
            $plural = array("", "", "", "", "", "", "");
        } else if ($tipo == "date") {
            $singular = array("dia", "dia", "dia", "dia", "dia", "dia", "dia");
            $plural = array("dias", "dias", "dias", "dias", "dias", "dias", "dias");
        } else {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
        }

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");

        if ($maiusculas) {

            foreach ($singular as $i => $v) {
                $singular[$i] = ucwords($v);
            }

            foreach ($plural as $i => $v) {
                $plural[$i] = ucwords($v);
            }

            foreach ($c as $i => $v) {
                $c[$i] = ucwords($v);
            }

            foreach ($d as $i => $v) {
                $d[$i] = ucwords($v);
            }

            foreach ($d10 as $i => $v) {
                $d10[$i] = ucwords($v);
            }

            foreach ($u as $i => $v) {
                $u[$i] = ucwords($v);
            }
        }

        $z = 0;
        $rt = "";

        $valor = number_format($valor * 1.0, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for ($i = 0; $i < count($inteiro); $i++)
            for ($ii = Text::length($inteiro[$i]); $ii < 3; $ii++)
                $inteiro[$i] = "0" . $inteiro[$i];

        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                    $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")
                $z++; elseif ($z > 0)
                $z--;
            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            if ($r)
                $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                        ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if (!$maiusculas) {
            return ($rt ? $rt : "zero");
        } else {

            //if ($rt)
            //$rt = ereg_replace(" E ", " e ", ucwords($rt));
            //$rt = ucwords($rt);
            return (($rt) ? ($rt) : "Zero");
        }
    }

    /**
     *
     * @param string $string
     * @param array $replaces
     *
     * @return string
     */
    public static function replace($string, array $replaces = array())
    {
        $replaces[] = array("key" => 'sys_now', "value" => date('d/m/Y H:i:s'), "type" => "datetime");
        $replaces[] = array("key" => 'sys_date', "value" => date('d/m/Y'), "type" => "date");

        $replaces[] = array("key" => 'sys_uniqid', "value" => uniqid(), "type" => "string");

        foreach ($replaces as $index => $replace) {

            $key = sif($replace, 'key', $index);
            $value = sif($replace, 'value', $replace);
            $type = sif($replace, 'type');
            $extra = "";

            switch ($type) {
                case 'date':
                    //$value = Text::substring($value, 0, 10);
                    //$extra = self::dateLikeText($value, false, false);
                    break;
                case 'money':
                    //$value = Number::format($value);
                    //$extra = self::currencyToText($value, true);
                    break;
                case 'int':
                    //$extra = self::currencyToText($value, true, "number");
                    break;
            }

            $wide = trim($extra);

            $s = Text::replace($string, '[{' . $key . '}]', $wide);
            $st = Text::replace($s, '({' . $key . '})', $value . ' (' . $wide . ')');
            $string = Text::replace($st, '{' . $key . '}', $value);
        }

        return $string;
    }
}