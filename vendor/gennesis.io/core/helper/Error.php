<?php
/**
 *
 * User: william
 * Date: 24/02/16
 * Time: 20:51
 */

namespace Apocalipse\Core\Helper;


use Apocalipse\Core\Model\Type\Warning;

abstract class Error
{
    /**
     * @var array
     */
    private static $exceptions = [];

    /**
     * @param $message
     * @param $file
     * @param $line
     * @param $type
     */
    public static function add($message, $file = "", $line = 0, $type = "")
    {
        self::$exceptions[] = new Warning($message, $file, $line, $type);
    }

    /**
     * @param bool $clear
     * @return array
     */
    public static function get($clear = true)
    {
        $exceptions = self::$exceptions;
        if ($clear) {
            self::$exceptions = [];
        }
        return $exceptions;
    }
}