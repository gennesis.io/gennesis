<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 24/02/16
 * Time: 23:08
 */

namespace Apocalipse\Core\Helper;


/**
 * Class Number
 * @package Apocalipse\Core\Helper
 */
abstract class Number
{

    /**
     * @param $value
     * @param bool $comma
     * @param string $current
     * @param int $precision
     *
     * @return string
     */
    public static function format($value, $comma = true, $current = "", $precision = 2)
    {
        $value = (double)(($value) ? $value : 0);

        $precision = ($precision) ? ((int)$precision) : 2;
        if ($comma) {
            $value = number_format($value, $precision, ',', '.');
        } else {
            $value = number_format($value, $precision, '.', '');
        }

        return $value;
    }
}