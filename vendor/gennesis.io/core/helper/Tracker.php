<?php

namespace Apocalipse\Core\Helper;


use Apocalipse\Core\App;
use Apocalipse\Core\Domain\Security\Auth;

/**
 * Class Tracker
 * @package Apocalipse\Core\Helper
 */
abstract class Tracker
{
    /**
     * @param $end
     */
    public static function shutdown($end)
    {
        $time = Number::format((microtime() - __APP_START__) * 10000, 5);

        self::log(App::getClientAddress(), $time);
    }

    /**
     * @param $address
     * @param $time
     */
    public static function log($address, $time)
    {
        $root = path(self::getRoot($address));

        if (defined('__APP_TIME__') && __APP_TIME__) {
            $name = Text::substring(File::escape(Text::replace(__APP_URL_SERVICE__, '/', '-')), 0, 100);
            File::write(path($root,  $name . '.' . 'log'), $time);
        }

        if (App::isLog()) {

            File::write($root . '.' . 'log', (((int) File::read($root . '.' . 'log')) + 1));
        }
    }

    /**
     * @return bool
     */
    public static function isAllowed()
    {
        $allowed = false;

        $blacklist = json_decode(__APP_TRACKER_BLACKLIST__);

        $address = App::getClientAddress();

        if (!in_array($address, $blacklist)) {

            $allowed = true;

            if (Auth::isGuest()) {

                $root = self::getRoot($address);
                $filename = $root . '.' . 'log';

                if (File::exists($filename)) {

                    $total = File::read($filename);
                    if ($total > __APP_TRACKER_MAX__) {
                        $allowed = false;
                    }
                }
            }
        }

        return $allowed;
    }

    /**
     * @param $address
     * @return string
     */
    private static function getRoot($address)
    {
        /**
         * @dir_dependency temp/log
         */
        return path(true, 'temp', 'log', date('Y'), date('m'), date('d'), $address);
    }
}