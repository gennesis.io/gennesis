<?php
/**
 *
 * User: william
 * Date: 24/02/16
 * Time: 20:09
 */

namespace Apocalipse\Core\Helper;


abstract class Text
{
    /**
     * @var int
     */
    const MAX_SIZE = 32768;

    /**
     * @param $content
     * @param $search
     * @param $replace
     * @return mixed
     */
    public static function replaceLast($content, $search, $replace)
    {
        $pos = strrpos($content, $search);

        if ($pos !== false) {
            $content = substr_replace($content, $replace, $pos, strlen($search));
        }

        return $content;
    }

    /**
     * @param $subject
     * @param $search
     * @param $replace
     * @param null $count
     * @return mixed
     */
    public static function replace($subject, $search, $replace, $count = null)
    {
        return str_replace($search, $replace, $subject, $count);
    }

    /**
     * @param $subject
     * @param $search
     * @param $replace
     * @param int $limit
     * @param null $count
     * @return mixed
     */
    public static function replaceRegex($subject, $search, $replace, $limit = -1, &$count = null)
    {
        return preg_replace($search, $replace, $subject, $limit, $count);
    }

    /**
     * @param $string
     * @param $start
     * @param null $length
     * @return string
     */
    public static function substring($string, $start, $length = null)
    {
        $length = is_null($length) ? self::length($string) : $length;

        return substr($string, $start, $length);
    }

    /**
     * @param $string
     * @return int
     */
    public static function length($string)
    {
        return strlen($string);
    }

    /**
     * @param $text
     * @param $length
     * @return string
     */
    public static function left($text, $length)
    {
        return self::substring($text, 0, $length);
    }

    /**
     * @param $text
     * @param $length
     * @return string
     */
    public static function right($text, $length)
    {
        return self::substring($text, strlen($text) - $length, $length);
    }

    /**
     * @param $text
     * @param $length
     * @param string $complement
     * @param bool $cut
     * @return string
     */
    public function slice($text, $length, $complement = '&hellip;', $cut = false)
    {
        $newText = self::substringMultiByte($text, 0, $length);

        if (self::substringMultiByte($text, $length, 1) == ' ' || $cut === true):
            return $newText . $complement;
        endif;

        if (mb_strlen($text) > $length):

            $lastSpacePos = mb_strrpos($newText, " ");

            if ($lastSpacePos !== false):
                $length = $lastSpacePos;
            endif;

            return self::substringMultiByte($newText, 0, $length) . $complement;
        else:
            return $text;
        endif;
    }

    /**
     * @param $text
     * @return bool
     */
    public function isUtf8($text)
    {
        return mb_check_encoding($text, 'utf-8');
    }

    /**
     * @param $text
     * @param array $verifyFollowings
     * @return string
     */
    public function getCharset($text, $verifyFollowings = array('utf-8', 'iso-8859-1'))
    {

        //O "x" corrige um bug da funcao "mb_detect_string", que falha caso
        //a Ultima letra da string seja acentuada.
        return mb_detect_encoding($text . 'x', $verifyFollowings);
    }

    /**
     * @param $string
     * @param $data
     * @return mixed
     */
    public static function insert($string, $data)
    {
        foreach ($data as $key => $value) {
            $regex = '%(:' . $key . ')%';
            $string = self::replace($string, $regex, $value, true);
        }
        return $string;
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function extract($string)
    {
        $extracted = array();
        preg_match_all('%:([a-zA-Z-_]+)%', $string, $extracted);
        return $extracted[1];
    }

    /**
     * @param $text
     * @param $put
     * @param $at
     * @return string
     */
    public static function putAt($text, $put, $at)
    {
        return self::left($text, $at) . $put . self::substringMultiByte($text, $at);
    }

    /**
     * @param $text
     * @param $mask
     * @return string
     */
    public static function applyMask($text, $mask)
    {
        $length = self::length($text);
        $buff = '';

        $special = array('/', '.', '-', '_', ' ');

        for ($i = 0, $j = 0; $i < $length; $i++, $j++) {
            if (!isset($text[$i]) || !isset($mask[$j]))
                break;

            if (in_array($mask[$j], $special)) {
                $buff .= $mask[$j];
                $j++;
            }
            $buff .= $text[$i];
        }

        return $buff;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return string
     */
    public static function highlight($needle, $haystack)
    {
        $ind = stripos($haystack, $needle);
        $len = self::length($needle);
        if ($ind !== false) {
            return self::substring($haystack, 0, $ind) . '<font style="background-color:yellow">' . self::substring($haystack, $ind, $len) . '</font>' . self::highlight($needle, self::substring($haystack, $ind + $len));
        } else {
            return $haystack;
        }
    }

    /**
     * @param $string
     * @return string
     */
    public static function password($string)
    {
        $p = sha1($string, true);
        $password = sha1($p);
        return "*" . self::upper($password);
    }

    /**
     * @param $var
     * @return string
     */
    public static function prevent($var)
    {
        $v = self::trim($var);
        $var = addslashes($v);

        return $var;
    }

    /**
     *
     * @param string $string
     * @return string
     */
    public static function trim($string)
    {
        return trim($string);
    }

    /**
     * @param $text
     * @param $size
     * @return string
     */
    public static function max($text, $size)
    {
        $end = ceil($size / 7.5);
        if (self::length($text) >= $end) {
            $text = self::substring($text, 0, $end) . "...";
        }

        return $text;
    }

    /**
     * @param $string
     * @return mixed|string
     */
    public static function upper($string)
    {

        $string = strtoupper($string);

        $strings = array();

        $strings[] = array('old' => "ç", 'new' => "Ç", 'regex' => false);
        $strings[] = array('old' => "á", 'new' => "Á", 'regex' => false);
        $strings[] = array('old' => "â", 'new' => "Â", 'regex' => false);
        $strings[] = array('old' => "ã", 'new' => "Ã", 'regex' => false);
        $strings[] = array('old' => "à", 'new' => "À", 'regex' => false);
        $strings[] = array('old' => "é", 'new' => "É", 'regex' => false);
        $strings[] = array('old' => "ê", 'new' => "Ê", 'regex' => false);
        $strings[] = array('old' => "í", 'new' => "Í", 'regex' => false);
        $strings[] = array('old' => "i", 'new' => "Î", 'regex' => false);
        $strings[] = array('old' => "ó", 'new' => "Ó", 'regex' => false);
        $strings[] = array('old' => "ô", 'new' => "Ô", 'regex' => false);
        $strings[] = array('old' => "ú", 'new' => "Ú", 'regex' => false);
        $strings[] = array('old' => "u", 'new' => "Û", 'regex' => false);
        $strings[] = array('old' => "õ", 'new' => "Õ", 'regex' => false);

        foreach ($strings as $s) {
            $old = $s['old'];
            $new = $s['new'];
            $regex = $s['regex'];
            $string = self::replace($string, $old, $new, $regex);
        }

        return $string;
    }

    /**
     *
     * @param string $string
     * @return string
     */
    public static function lower($string)
    {

        $string = strtolower($string);

        $strings = array();

        $strings[] = array('old' => "Ç", 'new' => "ç", 'regex' => false);
        $strings[] = array('old' => "Á", 'new' => "á", 'regex' => false);
        $strings[] = array('old' => "À", 'new' => "à", 'regex' => false);
        $strings[] = array('old' => "Â", 'new' => "â", 'regex' => false);
        $strings[] = array('old' => "Ã", 'new' => "ã", 'regex' => false);
        $strings[] = array('old' => "É", 'new' => "é", 'regex' => false);
        $strings[] = array('old' => "Ê", 'new' => "ê", 'regex' => false);
        $strings[] = array('old' => "Í", 'new' => "í", 'regex' => false);
        $strings[] = array('old' => "Î", 'new' => "i", 'regex' => false);
        $strings[] = array('old' => "Ó", 'new' => "ó", 'regex' => false);
        $strings[] = array('old' => "Ô", 'new' => "ô", 'regex' => false);
        $strings[] = array('old' => "Õ", 'new' => "õ", 'regex' => false);
        $strings[] = array('old' => "Ú", 'new' => "ú", 'regex' => false);
        $strings[] = array('old' => "Û", 'new' => "u", 'regex' => false);

        foreach ($strings as $s) {
            $old = $s['old'];
            $new = $s['new'];
            $regex = $s['regex'];
            $string = self::replace($string, $old, $new, $regex);
        }

        return $string;
    }

    /**
     * @param $value
     * @param $size
     * @param string $pad
     * @param int $side STR_PAD_LEFT, STR_PAD_RIGHT or STR_PAD_BOTH
     * @return string
     */
    public static function fix($value, $size, $pad = '0', $side = null)
    {
        if (self::length($value) > $size) {
            $value = self::substring($value, 0, $size);
        }
        $side = iif($side, STR_PAD_LEFT);

        $value = str_pad($value, $size, $pad, $side);

        return $value;
    }

    /**
     *
     * @param string $string
     * @param int $start
     * @param int $length
     * @param string $encoding
     *
     * @return string
     */
    public static function substringMultiByte($string, $start, $length = null, $encoding = null)
    {
        return mb_substr($string, $start, $length, $encoding);
    }

    /**
     * @param $text
     * @return mixed
     */
    public static function removeBreakLine($text)
    {

        $string = self::replace($text, '/\s/', ' ', true);
        $value = self::replace($string, array("<br>", "\n"), "");

        return $value;
    }

    /**
     * Trims text to a space then adds ellipses if desired
     *
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $strip_html if html tags are to be stripped
     *
     * @return string
     */
    public static function cutText($input, $length, $ellipses = true, $strip_html = true)
    {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (self::length($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = self::position(self::substring($input, 0, $length), ' ');
        $trimmed_text = self::substring($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= ' (...)';
        }

        return $trimmed_text;
    }

    /**
     *
     * @param string $string
     * @param string $search
     *
     * @return int
     */
    public static function position($string, $search)
    {
        return strpos($string, $search);
    }

    /**
     * @param $val
     * @param $mask
     * @return string
     */
    public static function mask($val, $mask)
    {

        $masked = $val;
        $v = self::clear($val);
        $val = self::replace($v, " ", "");

        $masks = array(
            'cep' => '#####-###',
            'cpf' => '###.###.###-##',
            'cnpj' => '##.###.###/#####-##',
            'tel' => self::substring($val, 0, 4) === '0800' ? '####-###-#####' : (self::length($val) <= 10 ? '(##) ####-####' : '(##) #-####-####'),
            'telefone' => self::substring($val, 0, 4) === '0800' ? '####-###-#####' : (self::length($val) <= 10 ? '(##) ####-####' : '(##) #-####-####')
        );

        if (isset($masks[$mask])) {

            $masked = '';
            $mask = $masks[$mask];

            $k = 0;
            for ($i = 0; $i < self::length($mask); $i++) {
                if ($mask[$i] == '#') {
                    if (isset($val[$k]))
                        $masked .= $val[$k++];
                } else {
                    if (isset($mask[$i]))
                        $masked .= $mask[$i];
                }
            }

        }

        return $masked;
    }

    /**
     * Método para abreviar strings baseado no espaço entre as palavras
     *
     * @param $string
     * @param bool $middle
     * @param int $limit
     * @return string
     */
    public static function abbreviate($string, $middle = false, $limit = 0)
    {
        $string = explode(" ", $string);

        $first = array_shift($string);
        $last = array_pop($string);
        $middle_str = " ";

        if ($middle) {
            if (count($string)) {
                foreach ($string as $s) {
                    $middle_str .= self::substring(self::trim($s), 0, 1) . '. ';
                }
            }
        }

        $return = $first . $middle_str . $last;
        if ($limit > 0) {
            $string = self::fix($return, $limit);
        }

        return $return;
    }

    /**
     * @param $string
     * @return string
     */
    public static function hyphening($string) {
        $dict = array(
            "I'm"      => "I am",
            "thier"    => "their",
        );
        return strtolower(
            preg_replace(
                array( '#[\\s-]+#', '#[^A-Za-z0-9\. -]+#' ),
                array( '-', '' ),
                self::clear(
                    str_replace(
                        array_keys($dict),
                        array_values($dict),
                        urldecode($string)
                    )
                )
            )
        );
    }

    /**
     * @param $text
     * @return mixed
     */
    public static function clear($text) {

        return clear($text);
    }

    /**
     * @param $string
     * @param $search
     * @param int $offset
     * @return bool|int
     */
    public static function indexOf($string, $search, $offset = 0)
    {
        return strpos($string, $search, $offset);
    }

}