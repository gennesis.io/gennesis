<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Helper
 | @file: Date.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 14/04/16 07:38
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Helper;


class Date
{

    /**
     * @param $ymd
     * @param bool $hour
     * @param bool $all
     * @return array|string
     */
    public static function ymd_TO_dmy($ymd, $hour = false, $all = true)
    {
        $dmy = "";
        if (strlen($ymd) >= 10) {

            $all = $all ? $all : ($ymd !== '0000-00-00' and $ymd !== '0000-00-00 00:00:00');
            if ($all) {

                $dmy = explode("-", substr(str_replace("/", "-", $ymd), 0, 10));
                $dmy = $dmy[2] . "/" . $dmy[1] . "/" . $dmy[0];
                if ($hour) {
                    if (strlen($ymd) === 19) {
                        $dmy = $dmy . "" . substr($ymd, 10, 9);
                    }
                }
            }
        }
        return $dmy;
    }

    /**
     * @param $dmy
     * @param bool $hour
     * @param bool $all
     * @return string
     */
    public static function dmy_TO_ymd($dmy, $hour = false, $all = true)
    {
        $ymd = '';
        $invalid = ['0000-00-00', '0000-00-00 00:00:00', '00/00/0000', '00/00/0000 00:00:00'];

        if ($all || (strlen($dmy) >= 10 && !in_array($dmy, $invalid))) {

            $arr_datetime = explode(' ', $dmy);
            if (count($arr_datetime) < 2) {
                $arr_datetime[] = '';
            }

            list($dmy, $time) = $arr_datetime;

            $arr_date = explode('/', $dmy);
            $arr_date = array_reverse($arr_date);

            $dmy = implode('-', $arr_date);

            $ymd = [];
            $ymd[] = $dmy;

            if ($hour && strlen($time)) {

                $ymd[] = $time;
            }
        }

        return implode(' ', $ymd);
    }

    /**
     * @param $format
     * @return string
     */
    public static function current($format = 'Y-m-d')
    {
        return date($format);
    }

    /**
     * @param $date
     * @param $days
     * @return string
     */
    public static function add($date, $days)
    {

        $date = explode('-', $date);

        $year = (int)isset($date[0]) && $date[0] ? $date[0] : 0;
        //[2016]-05-22
        $month = (int)isset($date[1]) && $date[1] ? $date[1] : 0;
        //2016-[05]-22
        $day = (int)isset($date[2]) && $date[2] ? $date[2] : 0;
        //2016-05-[22]

        return strftime("%Y-%m-%d", mktime(0, 0, 0, $month, $day + $days, $year));
    }

    /**
     * @param $date
     * @param $days
     * @return string
     */
    public static function sub($date, $days)
    {

        $date = explode('-', $date);

        $year = (int)isset($date[0]) && $date[0] ? $date[0] : 0;
        //[2016]-05-22
        $month = (int)isset($date[1]) && $date[1] ? $date[1] : 0;
        //2016-[05]-22
        $day = (int)isset($date[2]) && $date[2] ? $date[2] : 0;
        //2016-05-[22]

        return strftime("%Y-%m-%d", mktime(0, 0, 0, $month, $day - $days, $year));
    }

    /**
     * @param $date_begin
     * @param $date_end
     * @return float
     */
    public static function diff($date_begin, $date_end)
    {
        $data_1 = strtotime($date_begin);
        $data_2 = strtotime($date_end);

        $diff = floor(($data_1 - $data_2) / 86400);

        return $diff;
    }

    /**
     * @param $date
     * @return bool
     */
    public static function isDate($date)
    {
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    /**
     * @param $date
     * @param $holidays
     * @param $days
     * @return mixed
     */
    public static function next($date, $holidays = [], $days = null)
    {
        $valid = false;
        $dated = 0;

        if (self::isDate($date)) {

            do {

                $peaces = explode('-', $date);
                $day = $peaces[2] . '-' . $peaces[1];
                $is_weekend = in_array(date('w', strtotime($date)), ['0', '6']);
                $is_holiday = in_array($day, $holidays);
                $is_dated = $days === null ? true : ($dated >= (int)$days);

                if (!$is_weekend && !$is_holiday && $is_dated) {

                    $valid = true;

                } else {
                    /*
                    echo "$date, $dated, $days";
                    echo "<br>";
                    echo " W: " . ($is_weekend ? 'yes' : 'no');
                    echo " H: " . ($is_holiday ? 'yes' : 'no');
                    echo " D: " . ($is_dated ? 'yes' : 'no');
                    */
                    $date = self::add($date, 1);
                    if (!$is_weekend && !$is_holiday) {
                        $dated++;
                    }
                }
                //echo "<hr>";
            } while(!$valid);
        }

        return $date;
    }

}