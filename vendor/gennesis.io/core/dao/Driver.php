<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Driver.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 09:35
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Notification;

/**
 * Class Driver
 * @package Apocalipse\Core\Dao
 */
abstract class Driver
{
    /**
     * @var string
     */
    protected $dsn;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * Driver constructor.
     * @param string $dsn
     * @param string $user
     * @param string $password
     * @param array $parameters
     */
    public function __construct($dsn, $user, $password, $parameters = [])
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->password = $password;
        $this->parameters = $parameters;
    }

    /**
     * @param $message
     * @param $status
     * @param $file
     * @param $line
     */
    public function error($message, $status = null, $file = null, $line = null)
    {
        $status = iif($status, Wrapper::STATUS_ERROR);

        $this->errors[] = new Notification($message, $status, $file, $line);
    }

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return string
     */
    public abstract function create(Collection $collection, Record $record, Modifier $modifier, $debug);

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return array
     */
    public abstract function read(Collection $collection, Record $record, Modifier $modifier, $debug);

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public abstract function update(Collection $collection, Record $record, Modifier $modifier, $debug);

    /**
     * @param Collection $collection
     * @param Record $record
     * @param Modifier $modifier
     * @param bool $debug
     *
     * @return boolean
     */
    public abstract function delete(Collection $collection, Record $record, Modifier $modifier, $debug);

    /**
     * @return bool
     */
    public abstract function transactionBegin();

    /**
     * @return bool
     */
    public abstract function transactionCommit();

    /**
     * @return bool
     */
    public abstract function transactionRollback();

    /**
     * @return string
     */
    public function getDsn()
    {
        return $this->dsn;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}