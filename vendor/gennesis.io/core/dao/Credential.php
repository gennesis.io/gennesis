<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Credential.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 00:24
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;

use \stdClass;

/**
 * Class Credential
 * @package Apocalipse\Core\Dao
 */
class Credential
{
    /**
     * @var string
     */
    public $use;

    /**
     * @var string
     */
    public $dsn;

    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $password;

    /**
     * @var array
     */
    public $parameters;

    /**
     * Credential constructor.
     * @param $use
     * @param $dsn
     * @param $user
     * @param $password
     * @param $parameters
     */
    public function __construct($use, $dsn, $user, $password, $parameters)
    {
        $this->use = $use;
        $this->dsn = $dsn;
        $this->user = $user;
        $this->password = $password;
        $this->parameters = $parameters;
    }

    /**
     * @param stdClass $std
     * @return stdClass
     */
    public static function parse(stdClass $std)
    {
        $properties = ['use', 'dsn', 'user', 'password', 'parameters'];

        foreach ($properties as $property) {
            if (!isset($std->$property)) {
                $std->$property = '';
            }
        }

        return $std;
    }
}