<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Join.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 22/04/16 14:31
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;


use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Join
 * @package Apocalipse\Core\Dao
 */
class Join extends Origin
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var array
     */
    private $on;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    const TYPE_INNER = 'INNER';

    /**
     * @var string
     */
    const TYPE_LEFT = 'LEFT';

    /**
     * @var string
     */
    const TYPE_RIGHT = 'RIGHT';

    /**
     * @var string
     */
    const TYPE_FULL = 'FULL';

    /**
     * Join constructor.
     * @param Collection $collection
     * @param array $on
     * @param string $type
     */
    public function __construct(Collection $collection, array $on, $type = 'INNER')
    {
        $this->collection = $collection;
        $this->on = $on;
        $this->type = $type;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param Collection $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function getOn()
    {
        return $this->on;
    }

    /**
     * @param array $on
     */
    public function setOn($on)
    {
        $this->on = $on;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}