<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Transaction.php
 -------------------------------------------------------------------
 | @user: william
 | @creation: 03/04/16 09:35
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

/**
 * -----------------------------------------------------------
 * Operation        | SQL    | HTTP             | DDS
 * -----------------------------------------------------------
 * Create           | INSERT | PUT/POST         | write
 * Read (Retrieve)  | SELECT | GET                | read / take
 * Update (Modify)  | UPDATE | POST/PUT/PATCH   | write
 * Delete (Destroy) | DELETE | DELETE           | dispose
 * -----------------------------------------------------------
 */

namespace Apocalipse\Core\Dao;

use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Result;

/**
 * Class Transaction
 * @package Apocalipse\Core\Dao
 */
abstract class Transaction
{
    /**
     *
     * @var Driver array
     */
    private static $drivers = [];

    /**
     * @param $id
     * @param $type
     * @param $operation
     * @param $collection
     * @param $record
     * @param $modifier
     * @param $debug
     *
     * @return \Apocalipse\Core\Model\Type\Result
     */
    public static function result($id, $type, $operation, $collection, $record, $modifier, $debug)
    {
        $result = new Result();

        $data = null;

        $driver = self::start($id);

        if ($driver) {

            if (method_exists($driver, $operation)) {

                $data = $driver->$operation($collection, $record, $modifier, $debug);

                $errors = $driver->getErrors();

                if (count($errors) === 0 && is_array($errors)) {

                    if (gettype($data) === $type) {

                        $result->approve($data);

                        $result->push
                        ("Driver '" . $id . "' processed '" . $operation . "' successfully with type '"
                            . gettype($data) . "' matching with '" . $type . "'", Wrapper::STATUS_INFO);
                    } else {

                        $result->push("Driver '" . $id . "' returned invalid at run '" . $operation . "'. Expected '" . $type . "', but was given '" . gettype($data) . "'");
                    }
                } else {

                    $result->append($errors);
                }

            } else {
                $result->push("Driver '" . $id . "' do not have support to '" . $operation . "'");
            }

        } else {

            $result->push("Driver '" . $id . "' not found");
        }

        $result->close();

        return $result;
    }

    /**
     * @param $id
     * @return \Apocalipse\Core\Dao\Driver
     */
    private static function start($id)
    {
        $driver = null;

        $id = iif($id ? $id : null, __APP_DEFAULT_DRIVER__);

        if (isset(self::$drivers[$id])) {

            $driver = self::$drivers[$id];

        } else {

            $credentials = (array)config('dao', 'drivers');

            if ($credentials && isset($credentials[$id])) {

                /** @var Credential $credential */
                $credential = Credential::parse($credentials[$id]);

                $use = '\\' . pathToNamespace($credential->use);

                /** @var Driver $driver */
                if ($driver = new $use($credential->dsn, $credential->user, $credential->password, $credential->parameters)) {

                    if (!count($driver->getErrors())) {

                        $driver->transactionBegin();

                        self::$drivers[$id] = $driver;
                    }
                }
            }
        }

        return $driver;
    }

    /**
     * @return int
     */
    public static function commit()
    {
        $commits = 0;

        /** @var Driver $driver */
        foreach (self::$drivers as $id => $driver) {

            $driver->transactionCommit();
            unset(self::$drivers[$id]);
            $commits++;
        }

        return $commits;
    }

    /**
     * @return int
     */
    public static function rollback()
    {
        $rollbacks = 0;

        /** @var Driver $driver */
        foreach (self::$drivers as $id => $driver) {

            $driver->transactionRollback();
            unset(self::$drivers[$id]);
            $rollbacks++;
        }

        return $rollbacks;
    }
}