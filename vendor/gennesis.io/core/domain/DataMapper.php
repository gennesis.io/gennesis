<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: DataMapper.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 08:02
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain;


use Apocalipse\Core\Dao\DaoManager;

/**
 * Class DataMapper
 * @package Apocalipse\Core\Domain
 */
class DaoMapper extends DaoManager
{

}