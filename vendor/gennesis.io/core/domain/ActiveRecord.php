<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: ActiveRecord.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 16:15
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain;


abstract class ActiveRecord extends ModelManager
{

}