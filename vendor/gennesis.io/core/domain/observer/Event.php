<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Observer
 | @file: Event.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 21/04/16 00:02
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Observer;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Event
 * @package Apocalipse\Core\Domain\Observer
 */
class Event extends Origin
{
    /**
     * @var string
     */
    private $on;

    /**
     * @var array
     */
    private $actions;

    /**
     * @var string
     */
    private $use;

    /**
     * @var array
     */
    private $method;

    /**
     * @var array
     */
    private $map;

    /**
     * Event constructor.
     * @param string $on
     * @param array $actions
     * @param string $use
     * @param string $method
     * @param array $map
     */
    public function __construct($on, array $actions, $use, $method, $map = null)
    {
        $this->on = $on;
        $this->actions = $actions;
        $this->use = $use;
        $this->method = $method;
        $this->map = iif($map, []);
    }

    /**
     * @return string
     */
    public function getOn()
    {
        return $this->on;
    }

    /**
     * @param string $on
     * @return Event
     */
    public function setOn($on)
    {
        $this->on = $on;
        return $this;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     * @return Event
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @return string
     */
    public function getUse()
    {
        return $this->use;
    }

    /**
     * @param string $use
     * @return Event
     */
    public function setUse($use)
    {
        $this->use = $use;
        return $this;
    }

    /**
     * @return array
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param array $method
     * @return Event
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return array
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param string $map
     * @return Event
     */
    public function setMap($map)
    {
        $this->map = $map;
        return $this;
    }

}