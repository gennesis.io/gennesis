<?php
/*
 -------------------------------------------------------------------
 | @project: php@sistemaconcursos.com.br
 | @package: Apocalipse\Core\Domain\Auth
 | @file: Nonce.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 21/05/16 14:17
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Security;
use Apocalipse\Core\Helper\File;

/**
 * Class Nonce
 * @package Apocalipse\Core\Domain\Security
 */
abstract class Nonce
{
    /**
     * @var int
     */
    const LIFE_TIME = 7200;

    /**
     * @param $operation
     * @return string
     */
    public static function create($operation)
    {
        $nonce = Auth::guid();

        $filename = self::getFilename($nonce);
        if (!File::write($filename, $operation)) {
            $nonce = '';
        }
        return $nonce;
    }

    /**
     * @param $nonce
     * @param null $now
     * @return bool
     */
    public static function check($nonce, $now = null)
    {
        $check = false;

        $filename = self::getFilename($nonce);

        if (File::exists($filename)) {

            $check = true;

            $now = iif($now, time());

            $diff = round($now - filemtime(self::getFilename($nonce)));
            if ($diff > self::LIFE_TIME) {
                $check = false;
                self::destroy($nonce);
            }

            if ($check) {
                File::touch($filename);
            }
        }

        return $check;
    }

    /**
     * @param $nonce
     * @return bool
     */
    public static function destroy($nonce)
    {
        $destroyed = true;

        $filename = self::getFilename($nonce);
        if (File::exists($filename)) {
            $destroyed = File::destroy($filename);
        }

        return $destroyed;
    }

    /**
     * @param $nonce
     * @return string
     */
    public static function getFilename($nonce)
    {
        return path(true, 'storage', 'nonce', $nonce);
    }
}