<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Auth
 | @file: Session.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 11:45
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Security;


use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;

/**
 * Class Session
 * @package Apocalipse\Core\Domain\Auth
 */
abstract class Session
{
    /**
     * @var int
     */
    const LIFE_TIME = 86400;

    /**
     * @param $token
     * @param $now
     * @return string
     */
    public static function check($token, $now = null)
    {
        $check = false;

        $filename = self::getFilename($token);

        if (File::exists($filename)) {

            $check = true;

            $now = iif($now, time());

            $diff = round($now - filemtime(self::getFilename($token)));

            if ($diff > self::LIFE_TIME) {
                $check = false;
                self::destroy($token);
            }

            if ($check) {
                File::touch($filename);
            }
        }

        return $check;
    }

    /**
     * @param $token
     * @return mixed
     */
    public static function read($token)
    {
        return Json::decode(File::read(self::getFilename($token)));
    }

    /**
     * @param $user
     * @param $token
     * @return mixed
     */
    public static function create($user, $token)
    {
        $filename = self::getFilename($token);

        return File::write($filename, Json::encode($user));
    }

    /**
     * @param $token
     * @return bool
     */
    public static function destroy($token)
    {
        return File::destroy(self::getFilename($token));
    }

    /**
     * @param $token
     * @return string
     */
    public static function getFilename($token)
    {
        return path(true, 'storage', 'session', $token);
    }
}