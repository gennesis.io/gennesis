<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Content
 | @file: Scope.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 23/04/16 12:08
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Content;


use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class Scope
 * @package Apocalipse\Core\Domain\Content
 */
class Scope extends Record
{
    /**
     * @var Record
     */
    public $page;

    /**
     * @var array
     */
    public $route;

    /**
     * @var Data
     */
    public $data;

    /**
     * Scope constructor.
     * @param Record $page
     * @param array $route
     * @param Data $data
     */
    public function __construct(Record $page, array $route, Data $data)
    {
        parent::__construct([]);

        $this->page = $page;
        $this->route = $route;
        $this->data = $data;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if ($this->indexOf($name)) {

            Wrapper::err('One Record can not be modified, you need "inject" new values in this case');
        } else {

            $this->inject($name, $value);
        }
    }

}