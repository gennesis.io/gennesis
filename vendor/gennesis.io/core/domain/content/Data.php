<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Content
 | @file: Data.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 23/04/16 12:11
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Content;


use Apocalipse\Core\Domain\Data\Record;

/**
 * Class Data
 * @package Apocalipse\Core\Domain\Content
 */
class Data
{
    /**
     * @var Record
     */
    private $payload;

    /**
     * @var Record
     */
    private $post;

    /**
     * @var Record
     */
    private $get;

    /**
     * @var Record
     */
    private $files;

    /**
     * Data constructor.
     * @param $post
     * @param $get
     * @param $files
     * @param $payload
     */
    public function __construct($post, $get, $files, $payload)
    {
        $this->post = iif($post, new Record([]));
        $this->get = iif($get, new Record([]));
        $this->files = iif($files, new Record([]));
        $this->payload = iif($payload, new Record([]));
    }

    /**
     * @return Record
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return Record
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @return Record
     */
    public function getGet()
    {
        return $this->get;
    }

    /**
     * @return Record
     */
    public function getFiles()
    {
        return $this->files;
    }

}