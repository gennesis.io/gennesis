<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Authority.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 09/04/16 03:31
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Authority
 * @package Apocalipse\Core\Domain
 */
class Authority extends Origin
{
    /**
     * @var string
     */
    protected $collection;

    /**
     * @var string
     */
    protected $driver;

    /**
     * @var bool
     */
    public $debug = false;

    /**
     * @var string
     */
    const VALIDATE = 'Validation problems';

    /**
     * @var string
     */
    const BEFORE = 'Before processing problems';

    /**
     * @var string
     */
    const AFTER = 'After processing problems';

    /**
     * @var string
     */
    const RELATIONSHIP = 'Integrity committed relationship';

    /**
     * @var string
     */
    const FILTERS = 'No filters rendered to operation';

    /**
     * @var string
     */
    const DISPATCH = 'Could not dispatch event';

    /**
     * Authority constructor.
     * @param Collection $collection
     * @param string $driver
     */
    public function __construct($collection, $driver)
    {
        $this->collection = $collection;
        $this->driver = $driver ? $driver : __APP_DEFAULT_DRIVER__;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param string $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $driver
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

}