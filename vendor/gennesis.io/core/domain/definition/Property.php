<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Definition.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 00:02
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Property
 * @package Apocalipse\Core\Domain
 */
class Property extends Origin
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var bool
     */
    protected $required;

    /**
     * @var bool
     */
    protected $unique;

    /**
     * @var bool
     */
    protected $create;

    /**
     * @var bool
     */
    protected $read;

    /**
     * @var bool
     */
    protected $update;

    /**
     * @var bool
     */
    protected $delete;

    /**
     * @var string
     */
    protected $length;

    /**
     * @var string
     */
    public static $MAX = 'MAX';

    /**
     * Definition constructor.
     * @param $name
     * @param $type
     * @param null $required
     * @param null $unique
     * @param null $create
     * @param null $read
     * @param null $update
     * @param null $delete
     * @param null $length
     */
    public function __construct($name, $type, $required = null, $unique = null, $create = null, $read = null, $update = null, $delete = null, $length = null)
    {
        $this->name = $name;
        $this->type = $type;

        $this->required = iif($required, false);
        $this->unique = iif($unique, false);

        $this->create = iif($create, true);
        $this->read = iif($read, true);
        $this->update = iif($update, true);
        $this->delete = iif($delete, true);

        $this->length = iif($length, Property::$MAX);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * @return boolean
     */
    public function isUnique()
    {
        return $this->unique;
    }

    /**
     * @param boolean $unique
     */
    public function setUnique($unique)
    {
        $this->unique = $unique;
    }

    /**
     * @return boolean
     */
    public function isCreatable()
    {
        return $this->create;
    }

    /**
     * @param boolean $create
     */
    public function setCreate($create)
    {
        $this->create = $create;
    }

    /**
     * @return boolean
     */
    public function isReadable()
    {
        return $this->read;
    }

    /**
     * @param boolean $read
     */
    public function setRead($read)
    {
        $this->read = $read;
    }

    /**
     * @return boolean
     */
    public function isUpdatable()
    {
        return $this->update;
    }

    /**
     * @param boolean $update
     */
    public function setUpdate($update)
    {
        $this->update = $update;
    }

    /**
     * @return boolean
     */
    public function isDeletable()
    {
        return $this->delete;
    }

    /**
     * @param boolean $delete
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;
    }

    /**
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param string $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

}