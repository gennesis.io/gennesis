<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Definition
 | @file: Relation.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 10/04/16 13:22
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Object;


/**
 * Class Relation
 * @package Apocalipse\Core\Domain\Definition
 */
class Relation extends Relationship
{
    /**
     * Relation constructor.
     * @param $stdClass
     */
    public function __construct($stdClass)
    {
        $this->parameters = new Object();

        $parent = ['behaviour' => null, 'source' => null, 'target' => null];

        foreach ($stdClass as $key => $value) {
            if (array_key_exists($key, $parent)) {
                $parent[$key] = $value;
            } else {
                $this->parameters->$key = $value;
            }
        }

        $behaviour = $parent['behaviour'];

        $source = $parent['source'];
        if (!$source) {
            $source = sif($this->parameters, 'show', null);
        }

        $target = $parent['target'];
        if (!$target) {
            $target = sif($this->parameters, 'value', null);
        }

        //Wrapper::debug([$source, $target]);

        parent::__construct($behaviour, $source, $target);
    }

}