<?php
/*
 ___________________________________________________________________
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Relation.php
 ___________________________________________________________________
 | @user: william 
 | @creation: 10/04/16 07:50
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 ___________________________________________________________________
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Relation
 * @package Apocalipse\Core\Domain
 */
class Relationship extends Origin
{
    /**
     * @var array
     */
    const BEHAVIOURS = 'relationship';

    /**
     * @var string
     */
    private $behaviour;

    /**
     * @var string
     */
    private $source;

    /**
     * @var string
     */
    private $target;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var Object
     */
    protected $parameters;

    /**
     * @var mixed
     */
    private $returned;

    /**
     * Relation constructor.
     * @param $behaviour
     * @param string $source
     * @param string $target
     */
    public function __construct($behaviour, $source, $target = null)
    {
        $this->behaviour = $behaviour;
        $this->source = $source;
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getBehaviour()
    {
        return $this->behaviour;
    }

    /**
     * @param string $behaviour
     */
    public function setBehaviour($behaviour)
    {
        $this->behaviour = $behaviour;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return Object
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param Object $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return mixed
     */
    public function getReturned()
    {
        return $this->returned;
    }

    /**
     * @param $returned
     */
    public function setReturned($returned)
    {
        $this->returned = $returned;
    }
}