<?php
/*
 ___________________________________________________________________
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Behaviour.php
 ___________________________________________________________________
 | @user: william 
 | @creation: 08/04/16 01:53
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 ___________________________________________________________________
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Behaviour
 * @package Apocalipse\Core\Domain
 */
class Behaviour extends Origin
{
    /**
     * @var string
     */
    const PK = 'pk';

    /**
     * @var string
     */
    const HAS_MANY = 'one_to_many';

    /**
     * @var string
     */
    const HAS_ONE = 'one_to_one';

    /**
     * @var string
     */
    const BELONGS_TO = 'many_to_one';
}