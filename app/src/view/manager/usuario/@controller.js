/**
 *
 */
App.register("UsuarioController", function($scope, ServiceListener) {

    $scope.events.add('change', 'password', function (data) {
        //console.log(data);
    });

    ServiceListener.register('after.recover-record', 'usuario.after.edit', function (record) {

        if (record) {
            record['password'] = '';
        }
    });
});