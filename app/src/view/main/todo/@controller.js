/**
 *
 */
App.register("TodoController", function($scope, $rootScope, ServiceListener) {

    ServiceListener.register('after.add', 'main.todo.add', function (record) {
        record['project'] = App.project.driver;
    });
});