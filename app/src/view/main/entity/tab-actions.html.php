<div class="card" style="margin: 0 0 10px 0;">

    <div class="listview lv-bordered lv-lg">

        <div class="lv-body">

            <div class="lv-item media"
                 ng-repeat="_action in view.model.record['actions']">

                <div class="pull-left">
                    <button class="btn btn-sm btn-raised btn-success"
                            ng-click="view.popup.open('actions', _action, [])">
                        <i class="zmdi zmdi-edit"></i>
                    </button>
                    <button class="btn btn-sm btn-raised btn-danger"
                            ng-click="view.popup.remove('actions', _action)">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>

                <div class="media-body">
                    <ul class="lv-attrs">
                        <li>
                            Id: {{ _action.id }} / {{ _action[__id__] }}
                        </li>
                        <li>
                            Parâmetros: {{ _action.parameters.join(', ') }}
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="col-sm-12">
    <button class="btn btn-sm btn-success"
            ng-click="view.popup.open('actions', {local: ''})"> Adicionar
    </button>
</div>

<div class="modal--popup" ng-class="{'modal--open':view.popup.visible}">

    <div class="modal" style="height: 100%; overflow-y: scroll; overflow-x: auto; background-color: rgba(0,0,0,0.4);">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ação</h4>
                    <span class="modal--closer" ng-click="view.popup.close()">&times;</span>
                </div>

                <div class="modal-body">

                    <div class="col-sm-12 no-padding">

                        <div class="col-sm-4">

                            <div class="form-group fg-line">
                                <label for="id">Id</label>
                                <input type="text" ng-model="view.popup.data['actions'].id"
                                       class="form-control"
                                       id="id"
                                       placeholder="" autocomplete="off">
                            </div>
                            <div class="form-group fg-line">
                                <label>Interação</label>
                                <select chosen class="form-control"
                                        ng-model="view.popup.data['actions'].interaction"
                                        ng-options="_option.id as _option.text for _option in
                                        [{id:'', text:'Padrão'},{id: 'link', text:'Link Externo'},{id: 'popup', text:'Popup'}]"></select>
                            </div>
                            <div class="form-group fg-line">
                                <label>Configuração Adicional</label>
                                <input type="text" ng-model="view.popup.data['actions'].settings"
                                       class="form-control" placeholder="" autocomplete="off">
                            </div>
                            <div class="form-group fg-line">
                                <label>Confirmar</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="view.popup.data['actions'].confirm">
                                        Solicitar confirmação do usuário
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-5">

                            <div class="form-group fg-line">
                                <label for="message-success">Mensagem de Sucesso</label>
                                <input type="text" ng-model="view.popup.data['actions'].message.success"
                                       id="message-success" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group fg-line">
                                <label for="message-warning">Mensagem de Alerta</label>
                                <input type="text" ng-model="view.popup.data['actions'].message.warning"
                                       id="message-warning" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group fg-line">
                                <label for="message-error">Mensagem de Erro</label>
                                <input type="text" ng-model="view.popup.data['actions'].message.error"
                                       id="message-error" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group fg-line">
                                <label for="message-confirm">Mensagem de Confirmação</label>
                                <input type="text" ng-model="view.popup.data['actions'].message.confirm"
                                       id="message-confirm" class="form-control" autocomplete="off">
                            </div>

                        </div>

                        <div class="col-sm-3">

                            <label>Parâmetros</label>
                            <div class="col-sm-12 no-padding" style="margin-bottom: 5px;">
                                <select multiple ng-multiple="true" class="form-control" style="height: 254px;"
                                        ng-model="view.popup.data['actions'].parameters"
                                        ng-options="_option.id as _option.text for _option in parameters"></select>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="view.popup.save('actions')">
                        Ok
                    </button>
                    <button type="button" class="btn btn-link" ng-click="view.popup.close()">Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>