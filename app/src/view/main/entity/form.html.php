<!-- container -->
<div class="container-fluid">

    <!-- card -->
    <div class="card z-depth-1">

        <div class="listview lv-bordered lv-lg">

            <div class="lv-header-alt clearfix">

                <h2 class="lvh-label hidden-xs"> {{ view.model.getProperties().name }} </h2>
                <small> {{ view.model.getProperties().description }} {{ view.model.record[__id__] }}</small>

                <ul class="lv-actions actions">
                    <li ng-repeat="_action in view.model.getActions(index, 'top')">
                        <a href="javascript:void(0);" ng-click="view.operation(_action.id)">
                            <i ng-class="_action.classIcon"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="card--form">

            <form name="form">

                <div name-controller="name + 'Controller'">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <span ng-repeat="_action in view.model.getActions(index, 'top')">
                                <button type="button" class="btn" ng-class="_action.className"
                                        ng-click="view.operation(_action.id, view.model.record, _action.after)"
                                        ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
                                    <i class="{{ _action.classIcon }}"></i>
                                    {{ _action.label }}
                                </button>
                            </span>
                        </div>
                        <hr>
                    </div>

                    <div class="col-sm-12" ng-init="tab = 'settings'">

                        <ul class="tab-nav">
                            <li ng-class="{'active': tab === 'settings'}" ng-click="tab = 'settings'">
                                <a href="javascript: void(0);">Entidade</a>
                            </li>
                            <li ng-class="{'active': tab === 'items'}" ng-click="tab = 'items'">
                                <a href="javascript: void(0);">Atributos</a>
                            </li>
                            <li ng-class="{'active': tab === 'forms'}" ng-click="tab = 'forms'">
                                <a href="javascript: void(0);">Formulários</a>
                            </li>
                            <li ng-class="{'active': tab === 'actions'}" ng-click="tab = 'actions'">
                                <a href="javascript: void(0);">Ações</a>
                            </li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane active">

                                <div ng-include="'routes/view/main/entity/tab-' + tab + '.html'"></div>

                            </div>

                        </div>

                    </div>

                    <div class="col-sm-12">
                        <hr>
                        <div class="form-group">
                            <span ng-repeat="_action in view.model.getActions(index, 'bottom')">
                                <button type="button" class="btn" ng-class="_action.className"
                                        ng-click="view.operation(_action.id, view.model.record, _action.after)"
                                        ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
                                    <i class="{{ _action.classIcon }}"></i>
                                    {{ _action.label }}
                                </button>
                            </span>
                        </div>
                    </div>

                </div>

            </form>

            <!--<pre><code>{{ form | json }}</code></pre>-->

        </div>

        <br style="clear: both;">
    </div>
</div>

<div class="fab-button--toolbar">

    <button type="button" class="btn btn-float" ng-repeat="_action in view.model.getActions(index, 'fab')"
            ng-class="_action.className"
            ng-click="view.operation(_action.id, view.model.record, _action.after)"
            ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
        <i class="{{ _action.classIcon }}"></i>
    </button>
</div>
