<div class="col-sm-row">

    <div class="col-sm-6">
        <div class="form-group fg-line">
            <label for="id">Id</label>
            <input type="text" ng-model="view.popup.data['forms'].id"
                   class="form-control"
                   id="id"
                   placeholder="" autocomplete="off">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group fg-line">
            <label for="layout">Leiaute</label>
            <select id="layout" chosen
                    ng-model="view.popup.data['forms'].layout"
                    ng-options="_option.id as _option.text for _option in layouts">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group fg-line">
            <label for="recover" title="Determina se o formulário precisar recuperar dados">
                Recuperar
            </label>
            <select id="recover" chosen
                    data-placeholder="Determina se o formulário realiza recuperação de dados"
                    ng-model="view.popup.data['forms'].templateOptions.recover"
                    ng-options="_option.id as _option.id for _option in view.model.record.actions">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group fg-line">
            <label for="wrapper" title="Determina se a forma com que o dado será recuperado">
                Forma de Recuperação
            </label>
            <select id="wrapper" chosen
                    data-placeholder="Determina se a forma com que o dado será recuperado"
                    ng-model="view.popup.data['forms'].templateOptions.wrapper"
                    ng-options="_option.id as _option.text for _option in recoveries">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="col-sm-12">

        <label>Grupos</label>

        <div class="col-sm-12 no-padding">

            <div ng-repeat="_group in view.popup.data['forms'].groups track by $index">
                <div class="col-sm-3 no-padding">
                    <div class="form-group fg-line">
                        <input type="text" ng-model="_group.name"
                               class="form-control" placeholder="" autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-1" style="padding: 0 1px;margin: -1px -2px;">
                    <button type="button"
                            class="btn btn-sm btn-danger waves-effect"
                            style="width: 28px;height: 36px;border-radius: 0  3px 3px 0;"
                            ng-click="view.popup.data['forms'].groups.splice(view.popup.data['forms'].groups.indexOf(_group), 1);">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
            </div>

        </div>

        <div class="col-sm-12 no-padding">
            <button type="button" class="btn btn-sm btn-success"
                    ng-click="view.popup.data['forms'].groups.push({name: ''})"
                    style="width: 28px; height: 28px; padding: 0;">
                <i class="zmdi zmdi-plus"></i>
            </button>
        </div>

    </div>

</div>