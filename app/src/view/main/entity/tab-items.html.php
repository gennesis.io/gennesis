<div class="card" style="margin: 0 0 10px 0;">

    <div class="listview lv-bordered lv-lg">

        <div class="lv-body">

            <div class="lv-item media"
                 ng-repeat="_item in view.model.record['items'] track by $index">

                <div class="pull-left">
                    <button class="btn btn-sm btn-raised btn-success"
                            ng-click="view.popup.open('items', _item, __item_entity_fields, events)">
                        <span class="zmdi zmdi-edit"></span>
                    </button>
                    <button class="btn btn-sm btn-raised btn-danger"
                            ng-click="removeItemEntity(view.model.record, _item)">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>

                <div class="pull-right">
                    <button class="btn btn-sm btn-raised btn-default"
                            ng-click="addItemEntity(view.model.record, $index)">
                        <i class="zmdi zmdi-plus"></i>
                    </button>
                </div>

                <div class="media-body">
                    <div class="lv-title" style="cursor: pointer"
                         ng-click="view.popup.open('items', _item, __item_entity_fields, events)">
                        {{ _item.name }} <small> / {{ _item.type }}</small>
                    </div>
                    <ul class="lv-attrs">

                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>


<div class="col-sm-12">
    <button class="btn btn-sm btn-success"
            ng-click="view.popup.open('items')"> Adicionar
    </button>
</div>


<div class="modal--popup" ng-class="{'modal--open':view.popup.visible}">

    <div class="modal" style="height: 100%; overflow-y: scroll; overflow-x: auto; background-color: rgba(0,0,0,0.4);">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Atributo</h4>
                    <span class="modal--closer" ng-click="view.popup.close()">&times;</span>
                </div>

                <div class="modal-body">

                    <div ng-repeat="_field in __item_entity_fields" class="padding-sm">
                        <form-field field="_field" items="__item_entity_fields" record="view.popup.data['items']" listener="listener"></form-field>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="view.popup.save('items')">
                        Ok
                    </button>
                    <button type="button" class="btn btn-link" ng-click="view.popup.close()">Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
