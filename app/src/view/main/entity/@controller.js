/**
 *
 */
App.register("EntityController", ['$scope', 'ServiceListener', 'ServiceApi',
    function ($scope, ServiceListener, ServiceApi) {

        //fields / field / record
        $scope.events.add('change', 'behavior', function (items, field, record) {

            //console.log(items, field);
            if (record) {

                var
                    hidden_options = true,
                    hidden_settings = true;

                switch (record.behavior) {

                    case 'pk':
                        record.type = App.iif(record.type, 'int');
                        record.length = App.iif(record.length, 10);
                        record.required = App.iif(record.required, false);
                        record.nullable = App.iif(record.nullable, false);
                        record.create = App.iif(record.create, false);
                        record.read = App.iif(record.read, true);
                        record.update = App.iif(record.update, false);
                        break;

                    case 'fk':
                        record.type = App.iif(record.type, 'int');
                        record.length = App.iif(record.length, 10);
                        record.create = App.iif(record.create, true);
                        record.read = App.iif(record.read, true);
                        record.update = App.iif(record.update, true);

                        if (angular.isUndefined(record.required)) {
                            record.required = false;
                        }
                        if (angular.isUndefined(record.nullable)) {
                            record.nullable = true;
                        }

                        hidden_settings = false;
                        break;

                    case 'relationship':
                        record.type = App.iif(record.type, 'array');
                        record.length = App.iif(record.length, undefined);
                        record.create = App.iif(record.create, false);
                        record.read = App.iif(record.read, false);
                        record.update = App.iif(record.update, false);

                        if (angular.isUndefined(record.required)) {
                            record.required = false;
                        }
                        if (angular.isUndefined(record.nullable)) {
                            record.nullable = false;
                        }

                        hidden_settings = false;
                        break;

                    case 'restricted':
                        hidden_options = false;
                        break;

                    default:
                        record.type = App.iif(record.type, undefined);
                        record.length = App.iif(record.length, undefined);
                        record.required = App.iif(record.required, false);
                        record.nullable = App.iif(record.nullable, false);
                        record.create = App.iif(record.create, false);
                        record.read = App.iif(record.read, false);
                        record.update = App.iif(record.update, false);

                        hidden_settings = true;
                        hidden_options = true;
                        break;
                }
                if (angular.isArray(items)) {
                    items.forEach(function (item) {
                        if (item.key === 'options') {
                            item.hidden = hidden_options;
                        }
                        if (item.key === 'relationship') {
                            item.hidden = hidden_settings;
                        }
                    });
                }
            }
        });
        //fields / field / record
        $scope.events.add('change', 'key', function (items, field, record) {

            console.log(field);
            if (field && angular.isArray(field.options)) {
                field.options.forEach(function (option) {
                    if (option.name === record.key) {
                        console.log(option);
                        record.label = option.label;
                        record.title = option.title;
                        record.placeholder = option.placeholder;
                        record.required = option.required;
                    }
                });
            }
        });

        /**
         *
         * @type {*[]}
         * @private
         */
        $scope.__item_entity_fields = [
            {
                "key": "name",
                "component": "input[text]",
                "wrapper": "col-sm-3",
                "label": "Nome *"
            },
            {
                "key": "behavior",
                "component": "select[local]",
                "wrapper": "col-sm-3",
                "label": "Comportamento",
                "placeholder": "Selecione",
                "options": [
                    {"id": undefined, "text": "none"},
                    {"id": "pk", "text": "pk"},
                    {"id": "fk", "text": "fk"},
                    {"id": "relationship", "text": "relationship"},
                    {"id": "restricted", "text": "restricted"}
                ]
            },
            {
                "key": "type",
                "component": "select[local]",
                "wrapper": "col-sm-3",
                "label": "Tipo",
                "placeholder": "Selecione *",
                "options": [
                    {"id": "int", "text": "int"},
                    {"id": "double", "text": "double"},
                    {"id": "string", "text": "string"},
                    {"id": "text", "text": "text"},
                    {"id": "boolean", "text": "boolean"},
                    {"id": "array", "text": "array"},
                    {"id": "date", "text": "date"},
                    {"id": "datetime", "text": "datetime"},
                    {"id": "file", "text": "image"},
                    {"id": "file", "text": "file"}
                ]
            },
            {
                "key": "length",
                "component": "input[number]",
                "wrapper": "col-sm-3",
                "label": "Tamanho"
            },
            {
                "key": "options",
                "component": "list",
                "hidden": true,
                "wrapper": "col-sm-8",
                "label": "Opções"
            },
            {
                "key": "relationship",
                "component": "indexed",
                "hidden": true,
                "wrapper": "col-sm-8",
                "label": "Configuração",
                "options": [
                    {
                        "id": "module", "text": "Módulo"
                    },
                    {
                        "id": "entity", "text": "Entidade"
                    },
                    {
                        "id": "operation", "text": "Ação"
                    },
                    {
                        "id": "value", "text": "Valor"
                    },
                    {
                        "id": "show", "text": "Descrição"
                    },
                    {
                        "id": "behaviour", "text": "Comportamento"
                    }
                ]
            },
            {
                "key": "label",
                "component": "input[text]",
                "wrapper": "col-sm-4",
                "label": "Rótulo (label)"
            },
            {
                "key": "placeholder",
                "component": "input[text]",
                "wrapper": "col-sm-4",
                "label": "Complemento (placeholder)"
            },
            {
                "key": "title",
                "component": "input[text]",
                "wrapper": "col-sm-4",
                "label": "Dica (title)"
            },
            {
                "key": "required",
                "component": "input[checkbox]",
                "wrapper": "col-sm-2",
                "label": "Obrigatório",
                "placeholder": ""
            },
            {
                "key": "nullable",
                "component": "input[checkbox]",
                "wrapper": "col-sm-2",
                "label": "Nulo",
                "placeholder": ""
            },
            {
                "key": "create",
                "component": "input[checkbox]",
                "wrapper": "col-sm-1",
                "label": "Criar"
            },
            {
                "key": "read",
                "component": "input[checkbox]",
                "wrapper": "col-sm-1",
                "label": "Ler"
            },
            {
                "key": "update",
                "component": "input[checkbox]",
                "wrapper": "col-sm-1",
                "label": "Atualizar"
            }
        ];
        /**
         *
         * @type {*[]}
         * @private
         */
        $scope.__item_form_fields = [
            [
                {
                    "key": "key",
                    "component": "select[local]",
                    "wrapper": "col-sm-3",
                    "label": "Atributo",
                    "settings": {
                        "id": "name",
                        "text": "name"
                    },
                    "options": []
                },
                {
                    "key": "component",
                    "component": "select[local]",
                    "wrapper": "col-sm-3",
                    "label": "Componente",
                    "placeholder": "Selecione",
                    "options": [
                        {"id": "input[text]", "text": "input[text]"},
                        {"id": "input[password]", "text": "input[password]"},
                        {"id": "input[upper]", "text": "input[upper]"},
                        {"id": "input[number]", "text": "input[number]"},
                        {"id": "input[date]", "text": "input[date]"},
                        {"id": "input[cpf]", "text": "input[cpf]"},
                        {"id": "input[cnpj]", "text": "input[cnpj]"},
                        {"id": "input[cep]", "text": "input[cep]"},
                        {"id": "input[money]", "text": "input[money]"},
                        {"id": "input[telephone]", "text": "input[telephone]"},
                        {"id": "input[radio]", "text": "input[radio]"},
                        {"id": "input[checkbox]", "text": "input[checkbox]"},
                        {"id": "input[color]", "text": "input[color]"},
                        {"id": "input[file]", "text": "input[file]"},
                        {"id": "select[local]", "text": "select[local]"},
                        {"id": "select[multiple]", "text": "select[multiple]"},
                        {"id": "select[remote]", "text": "select[remote]"},
                        {"id": "textarea", "text": "textarea"},
                        {"id": "html", "text": "html"},
                        {"id": "list-inline", "text": "list-inline"},
                        {"id": "list-view", "text": "list-view"}
                    ]
                },
                {
                    "key": "wrapper",
                    "component": "select[local]",
                    "wrapper": "col-sm-3",
                    "label": "Largura (wrapper)",
                    "options": [
                        {"id": "col-sm-1", "text": "1/12"},
                        {"id": "col-sm-2", "text": "2/12"},
                        {"id": "col-sm-3", "text": "3/12"},
                        {"id": "col-sm-4", "text": "4/12"},
                        {"id": "col-sm-5", "text": "5/12"},
                        {"id": "col-sm-6", "text": "6/12"},
                        {"id": "col-sm-7", "text": "7/12"},
                        {"id": "col-sm-8", "text": "8/12"},
                        {"id": "col-sm-9", "text": "9/12"},
                        {"id": "col-sm-10", "text": "10/12"},
                        {"id": "col-sm-11", "text": "11/12"},
                        {"id": "col-sm-12", "text": "12/12"}
                    ]
                },
                {
                    "key": "label",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Rótulo (label)"
                }
            ],
            [
                {
                    "key": "group",
                    "component": "select[local]",
                    "wrapper": "col-sm-3",
                    "label": "Grupo",
                    "settings": {
                        "id": "name",
                        "text": "name"
                    },
                    "options": []
                },
                {
                    "key": "title",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Dica Flutuante"
                },
                {
                    "key": "placeholder",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Rótulo Complementar"
                },
                {
                    "key": "order",
                    "component": "input[number]",
                    "wrapper": "col-sm-1",
                    "label": "Ordenação"
                },
                {
                    "key": "readonly",
                    "component": "input[checkbox]",
                    "wrapper": "col-sm-1",
                    "label": "Bloqueado"
                },
                {
                    "key": "required",
                    "component": "input[checkbox]",
                    "wrapper": "col-sm-1",
                    "label": "Obrigatório"
                }
            ]
        ];
        /**
         *
         * @type {*[]}
         * @private
         */
        $scope.__item_form_actions = [
            [
                {
                    "key": "id",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Id"
                },
                {
                    "key": "label",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Rótulo"
                },
                {
                    "key": "className",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Classe"
                },
                {
                    "key": "classIcon",
                    "component": "input[text]",
                    "wrapper": "col-sm-3",
                    "label": "Ícone"
                }
            ],
            [
                {
                    "key": "after",
                    "component": "input[text]",
                    "wrapper": "col-sm-4",
                    "label": "Gatilho"
                },
                {
                    "key": "position",
                    "component": "indexed",
                    "wrapper": "col-sm-8",
                    "label": "Posição",
                    "options": [
                        {"id": "top", "text": "Barra Superior", "class": "col-sm-3"}
                        , {"id": "middle", "text": "Centro do Formulário", "class": "col-sm-3"}
                        , {"id": "bottom", "text": "Barra Inferior", "class": "col-sm-3"}
                        , {"id": "fab", "text": "Botão Flutuante", "class": "col-sm-3"}
                    ]
                }
            ],
            [
                {
                    "key": "hint",
                    "component": "input[text]",
                    "wrapper": "col-sm-6",
                    "label": "Dica de tela"
                },
                {
                    "key": "hintPosition",
                    "component": "input[text]",
                    "wrapper": "col-sm-6",
                    "label": "Posição da Dica de tela"
                }
            ]
        ];


        /**
         *
         * @type {*[]}
         */
        $scope.layouts = [
            {id: 'form', text: 'form'}, {id: 'index', text: 'index'}
        ];
        /**
         *
         * @type {*[]}
         */
        $scope.recoveries = [
            {id: 'record', text: 'record'}, {id: 'recordSet', text: 'recordSet'}
        ];
        /**
         *
         * @type {*[]}
         */
        $scope.parameters = [
            {id: 'record', text: 'record'},
            {id: 'filters', text: 'filters'},
            {id: 'sorters', text: 'sorters'},
            {id: 'aggregators', text: 'aggregators'},
            {id: 'fields', text: 'fields'}
        ];


        /**
         *
         * @param record
         * @param $index
         */
        $scope.addItemEntity = function (record, $index) {

            var item = {};
            item[$scope.__id__] = App.uniqid();

            if (typeof $index !== 'undefined') {
                record.items.splice($index + 1, 0, item);
            } else {
                record.items.push(item);
            }
            $scope.view.popup.open('items', item);
        };
        /**
         *
         * @param record
         * @param item
         */
        $scope.removeItemEntity = function (record, item) {

            record.items.splice(record.items.indexOf(item), 1);
        };


        /**
         * @param form
         * @param field
         */
        $scope.toggleFormField = function (form, field) {

            field.toggled = !field.toggled;
            if (!field.toggled) {
                field.__fields = form.__items_fields;
            } else {
                field.__fields = [];
            }
        };
        /**
         *
         * @param form
         * @param record
         * @param $index
         */
        $scope.addFieldToForm = function (form, record, $index) {

            var item = {};
            if (typeof $index !== 'undefined') {
                form.fields.splice($index + 1, 0, item);
            } else {
                form.fields.push(item);
            }

            item.__fields = form.__items_fields;
        };
        /**
         *
         * @param items
         * @param item
         */
        $scope.removeFieldFromForm = function (items, item) {

            items.splice(items.indexOf(item), 1);
        };


        /**
         *
         * @param form
         * @param record
         * @param $index
         */
        $scope.addActionToForm = function (form, record, $index) {

            var item = {};
            if (typeof $index !== 'undefined') {
                form.actions.splice($index + 1, 0, item);
            } else {
                if (!form.actions) {
                    form.actions = [];
                }
                form.actions.push(item);
            }
        };
        /**
         *
         * @param items
         * @param item
         */
        $scope.removeActionFromForm = function (items, item) {

            items.splice(items.indexOf(item), 1);
        };


        /**
         *
         * @param items
         * @param $index
         */
        $scope.addParametersToAction = function (items, $index) {

            var item = '';
            if (typeof $index !== 'undefined') {
                items.splice($index + 1, 0, item);
            } else {
                items.push(item);
            }
        };
        /**
         *
         * @param items
         * @param item
         */
        $scope.removeParametersFromAction = function (items, item) {

            items.splice(items.indexOf(item), 1);
        };


        /**
         *
         * @param record
         */
        var load = function (record) {

            if (!record) {
                record = {};
            }

            if (!angular.isArray(record.items)) {
                record.items = [];
            }
            record.items.forEach(function (item) {

                if (!item[App.id]) {
                    item[App.id] = App.uniqid();
                }
            });


            if (!record.forms) {

                record.forms = [];

                record.forms.push({
                    "id": "index",
                    "layout": "index",
                    "type": "form",
                    "templateOptions": {
                        "recover": "read",
                        "wrapper": "recordSet"
                    },
                    "fields": [],
                    "actions": [
                        {
                            "id": "add",
                            "position": {
                                "fab": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "zmdi zmdi-plus",
                            "hintPosition": "hint--top-left hint--rounded hint--bounce",
                            "hint": "Iniciar um novo registro"
                        },
                        {
                            "id": "add",
                            "position": {
                                "top": 0
                            },
                            "className": "",
                            "classIcon": "zmdi zmdi-plus"
                        },
                        {
                            "id": "index",
                            "position": {
                                "top": 1
                            },
                            "className": "",
                            "classIcon": "zmdi zmdi-refresh-alt"
                        },
                        {
                            "id": "show",
                            "position": {
                                "middle": 0
                            },
                            "conditions": [],
                            "className": "btn btn-sm btn-default",
                            "classIcon": "zmdi zmdi-search",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Visualizar o registro"
                        },
                        {
                            "id": "edit",
                            "position": {
                                "middle": 1
                            },
                            "conditions": [],
                            "className": "btn btn-sm btn-default",
                            "classIcon": "zmdi zmdi-edit",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Abrir o registro para alterar"
                        },
                        {
                            "id": "delete",
                            "after": "index",
                            "position": {
                                "middle": 2
                            },
                            "conditions": [],
                            "className": "btn btn-sm btn-danger",
                            "classIcon": "zmdi zmdi-delete",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Remover o registro"
                        }
                    ]
                });

                record.forms.push({
                    "id": "add",
                    "layout": "form",
                    "type": "form",
                    "templateOptions": {
                        "recover": ""
                    },
                    "fields": [],
                    "actions": [
                        {
                            "id": "add",
                            "position": {
                                "fab": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "zmdi zmdi-plus",
                            "hintPosition": "hint--top-left hint--rounded hint--bounce",
                            "hint": "Iniciar um novo registro"
                        },
                        {
                            "id": "save",
                            "position": {
                                "top": 0,
                                "bottom": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "",
                            "label": "Salvar",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Salvar o registro"
                        },
                        {
                            "id": "save",
                            "position": {
                                "top": 1,
                                "bottom": 1
                            },
                            "className": "btn btn-default",
                            "classIcon": "",
                            "label": "Salvar e Fechar",
                            "after": "index",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Salvar o registro e voltar para o índice"
                        },
                        {
                            "id": "index",
                            "position": {
                                "top": 2,
                                "bottom": 2
                            },
                            "className": "btn btn-link",
                            "classIcon": "",
                            "label": "Fechar",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Voltar para o índice"
                        }
                    ]
                });

                record.forms.push({
                    "id": "edit",
                    "layout": "form",
                    "type": "form",
                    "templateOptions": {
                        "recover": "read",
                        "wrapper": "record"
                    },
                    "fields": [],
                    "actions": [
                        {
                            "id": "add",
                            "position": {
                                "fab": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "zmdi zmdi-plus",
                            "hintPosition": "hint--top-left hint--rounded hint--bounce",
                            "hint": "Iniciar um novo registro"
                        },
                        {
                            "id": "save",
                            "position": {
                                "top": 0,
                                "bottom": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "",
                            "label": "Salvar",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Salvar o registro"
                        },
                        {
                            "id": "save",
                            "position": {
                                "top": 1,
                                "bottom": 1
                            },
                            "className": "btn btn-default",
                            "classIcon": "",
                            "label": "Salvar e Fechar",
                            "after": "index",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Salvar o registro e voltar para o índice"
                        },
                        {
                            "id": "delete",
                            "label": "Apagar",
                            "after": "index",
                            "position": {
                                "top": 2,
                                "bottom": 2
                            },
                            "className": "btn btn-danger",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Remover o registro e voltar para o \u00edndice"
                        },
                        {
                            "id": "index",
                            "position": {
                                "top": 3,
                                "bottom": 3
                            },
                            "className": "btn btn-link",
                            "classIcon": "",
                            "label": "Fechar",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Voltar para o índice"
                        }
                    ]
                });

                record.forms.push({
                    "id": "show",
                    "layout": "form",
                    "type": "form",
                    "templateOptions": {
                        "recover": "read",
                        "wrapper": "record"
                    },
                    "fields": [],
                    "actions": [
                        {
                            "id": "add",
                            "position": {
                                "fab": 0
                            },
                            "className": "btn btn-theme",
                            "classIcon": "zmdi zmdi-plus",
                            "hintPosition": "hint--top-left hint--rounded hint--bounce",
                            "hint": "Iniciar um novo registro"
                        },
                        {
                            "id": "index",
                            "position": {
                                "top": 0,
                                "bottom": 0
                            },
                            "className": "btn btn-default",
                            "classIcon": "",
                            "label": "Fechar",
                            "hintPosition": "hint--top-right hint--rounded hint--bounce",
                            "hint": "Voltar para o índice"
                        }
                    ]
                });
            }
            record.forms.forEach(function (form) {

                if (!form[App.id]) {
                    form[App.id] = App.uniqid();
                }

                if (!form.groups) {
                    form.groups = [];
                }

                //noinspection JSAccessibilityCheck
                var items_fields = angular.copy($scope.__item_form_fields);

                items_fields[0][0].options = record.items;
                items_fields[1][0].options = form.groups;

                form.__items_fields = items_fields;

                //noinspection JSAccessibilityCheck
                form.__items_actions = angular.copy($scope.__item_form_actions);

                if (angular.isObject(form.actions)) {
                    var actions = [];
                    for (var i in form.actions) {
                        if (form.actions.hasOwnProperty(i)) {
                            actions.push(form.actions[i]);
                        }
                    }
                    form.actions = actions;
                }
                /*
                 */
                if (angular.isArray(form.fields)) {

                    form.fields.forEach(function (_field) {
                        _field.toggled = true;
                    });
                }
                if (angular.isArray(form.actions)) {

                    form.actions.forEach(function (_action) {
                        _action.toggled = true;
                    });
                }
            });


            if (!record.actions) {

                record.actions = [];

                record.actions.push({
                    "id": "create",
                    "type": "action",
                    "parameters": [
                        "record"
                    ],
                    "message": {
                        "success": "Registro salvo com sucesso",
                        "error": "Erro ao salvar",
                        "warning": "A validação emperrou as coisas"
                    }
                });

                record.actions.push({
                    "id": "delete",
                    "type": "action",
                    "parameters": [
                        "record",
                        "filters"
                    ],
                    "confirm": true,
                    "message": {
                        "success": "Registro apagado com sucesso",
                        "error": "Erro ao apagar",
                        "warning": "Este registro nao pode ser apagado",
                        "confirm": "Deseja apagar este registro?"
                    }
                });

                record.actions.push({
                    "id": "read",
                    "type": "action",
                    "parameters": ["filters", "sorters", "aggregators", "fields"]
                });

                record.actions.push({
                    "id": "save",
                    "type": "action",
                    "parameters": [
                        "record"
                    ],
                    "message": {
                        "success": "Registro salvo com sucesso",
                        "error": "Erro ao salvar",
                        "warning": "A validação emperrou as coisas"
                    }
                });

                record.actions.push({
                    "id": "update",
                    "type": "action",
                    "parameters": [
                        "record",
                        "filters"
                    ],
                    "message": {
                        "success": "Registro salvo com sucesso",
                        "error": "Erro ao salvar",
                        "warning": "A validação emperrou as coisas"
                    }
                });
            }
            record.actions.forEach(function (action) {

                if (!action[App.id]) {
                    action[App.id] = App.uniqid();
                }
            });


            if (!record.languages) {

                record.languages = [];

                record.languages.push({
                    "id": "pt-BR",
                    "items": {},
                    "default": {},
                    "forms": {}
                });
            }

        };

        ['add', 'edit'].forEach(function (index) {

            ServiceListener.register('after.add', 'main.entity.' + index, function (record) {
                load(record);
            });

            ServiceListener.register('after.recover-record', 'main.entity.' + index, function (record) {
                load(record);
            });
        });

    }]);
