
<div class="col-sm-12" ng-init="attributes = view.popup.data['forms'].fields.generateJSON()">
    <div class="form-group">
        <textarea style="min-height: 100px" ng-model="attributes" class="form-control"></textarea>
    </div>
</div>
<div class="col-sm-12">
    <div class="form-group">
        <button class="btn btn-success" ng-click="view.popup.data['forms'].fields = attributes.generateJS()">
            Importar
        </button>
    </div>
</div>

<hr>

<div class="col-sm-12" ng-init="actions = view.popup.data['forms'].actions.generateJSON()">
    <div class="form-group">
        <textarea style="min-height: 100px" ng-model="actions" class="form-control"></textarea>
    </div>
</div>
<div class="col-sm-12">
    <div class="form-group">
        <button class="btn btn-success" ng-click="view.popup.data['forms'].actions = actions.generateJS()">
            Importar
        </button>
    </div>
</div>