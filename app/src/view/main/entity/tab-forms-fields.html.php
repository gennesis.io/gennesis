<div ng-repeat="__field__ in view.popup.data['forms'].fields | orderBy:'order' track by $index"
     ng-class="{'form--toggled': __field__.toggled}"
     class="col-sm-12 no-padding form--row">

    <div class="form--toggler" ng-click="toggleFormField(view.popup.data['forms'], __field__)">
        <i class="zmdi zmdi-hc-fw"
           ng-class="{'zmdi-plus-square': __field__.toggled, 'zmdi-minus-square': !__field__.toggled}"></i>
        <label ng-show="__field__.toggled">
            {{ __field__.key }}
            <small>{{ __field__.component }} / {{ __field__.wrapper }} / {{ __field__.order }}</small>
        </label>
    </div>

    <div ng-repeat="_group in __field__.__fields" class="col-sm-11 no-padding">

        <div ng-repeat="_field in _group" class="padding-sm">
            <form-field form="form" field="_field" record="__field__" listener="listener"
                        class="form--wrapper"></form-field>
        </div>
    </div>

    <div class="no-padding form--order">
        <button type="button" class="btn btn-sm btn-default"
                ng-click="__field__.order = __field__.order - 1"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>
        </button>
        <button type="button" class="btn btn-sm btn-default"
                ng-click="__field__.order = __field__.order + 1"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>
        </button>
    </div>

    <div class="no-padding form--action">
        <button type="button" class="btn btn-sm btn-danger"
                ng-click="removeFieldFromForm(view.popup.data['forms'].fields, __field__)"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-delete"></i>
        </button>
        <button type="button" class="btn btn-sm btn-success"
                ng-click="addFieldToForm(view.popup.data['forms'], view.model.record, $index)"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-plus"></i>
        </button>
    </div>

</div>

<div class="col-sm-12 no-padding" ng-hide="view.popup.data['forms'].fields.length">
    <button type="button" class="btn btn-sm btn-success"
            ng-click="addFieldToForm(view.popup.data['forms'], view.model.record)">
        <i class="zmdi zmdi-plus"></i>
    </button>
</div>