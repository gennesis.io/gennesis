<div class="card" style="margin: 0 0 10px 0;">

    <div class="listview lv-bordered lv-lg">

        <div class="lv-body">

            <div class="lv-item media"
                 ng-repeat="_form in view.model.record['forms']">

                <div class="pull-left">
                    <button class="btn btn-sm btn-raised btn-success"
                            ng-click="view.popup.open('forms', _form, [])">
                        <i class="zmdi zmdi-edit"></i>
                    </button>
                    <button class="btn btn-sm btn-raised btn-danger"
                            ng-click="view.popup.remove('forms', _form)">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>

                <div class="media-body">
                    <ul class="lv-attrs">
                        <li>
                            Id: {{ _form.id }} / {{ _form[__id__] }}
                        </li>
                        <li>
                            Leiaute: {{ _form.layout }}
                        </li>
                        <li>Recuperar:
                            <i ng-class="{
                              'zmdi zmdi-check-square zmdi-hc-fw':(_form.templateOptions.recover)
                            , 'zmdi zmdi-square-o zmdi-hc-fw': (!_form.templateOptions.recover)
                            }"></i>
                        </li>
                        <li>Forma de Recuperação: {{ _form.templateOptions.wrapper }}</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="col-sm-12">
    <button class="btn btn-sm btn-success"
            ng-click="view.popup.open('forms', {fields: []})"> Adicionar
    </button>
</div>

<div class="modal--popup" ng-class="{'modal--open':view.popup.visible}">

    <div class="modal" style="height: 100%; overflow-y: scroll; overflow-x: auto; background-color: rgba(0,0,0,0.4);">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Formulário</h4>
                    <span class="modal--closer" ng-click="view.popup.close()">&times;</span>
                </div>

                <div class="modal-body" ng-init="form_tab = 'settings'">

                    <ul class="tab-nav">
                        <li ng-class="{'active': form_tab === 'settings'}" ng-click="form_tab = 'settings'">
                            <a href="javascript: void(0);">Configuração</a>
                        </li>
                        <li ng-class="{'active': form_tab === 'fields'}" ng-click="form_tab = 'fields'">
                            <a href="javascript: void(0);">Atributos</a>
                        </li>
                        <li ng-class="{'active': form_tab === 'actions'}" ng-click="form_tab = 'actions'">
                            <a href="javascript: void(0);">Ações</a>
                        </li>
                        <li ng-class="{'active': form_tab === 'transfer'}" ng-click="form_tab = 'transfer'">
                            <a href="javascript: void(0);">Transferência</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade active in">

                            <div ng-include="'routes/view/main/entity/tab-forms-' + form_tab + '.html'"></div>

                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="view.popup.save('forms')">
                        Ok
                    </button>
                    <button type="button" class="btn btn-link" ng-click="view.popup.close()">Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>