<div ng-repeat="__action__ in view.popup.data['forms'].actions track by $index"
     ng-class="{'form--toggled': __action__.toggled}"
     class="col-sm-12 no-padding form--row">

    <div class="form--toggler" ng-click="__action__.toggled = !__action__.toggled">
        <i class="zmdi zmdi-hc-fw"
           ng-class="{'zmdi-plus-square': __action__.toggled, 'zmdi-minus-square': !__action__.toggled}"></i>
        <label ng-show="__action__.toggled">
            {{ __action__.id }}
            <small>{{ (__action__.after ? __action__.after + ' / ' : __action__.after) }}</small>
        </label>
    </div>

    <div ng-repeat="_group in view.popup.data['forms'].__items_actions" class="col-sm-11 no-padding">
        <div ng-repeat="_action in _group" class="padding-sm">
            <form-field form="form" field="_action" record="__action__" listener="listener"
                        class="form--wrapper"></form-field>
        </div>
    </div>
<!---->
<!--    <div class="col-sm-11 no-padding form--wrapper padding-sm">-->
<!---->
<!--        <div class="col-sm-3">-->
<!--            <div class="form-group fg-line">-->
<!--                <label for="position-top">Superior</label>-->
<!--                <input type="number" ng-model="__action__.position.top"-->
<!--                       id="position-top" class="form-control" autocomplete="off">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            <div class="form-group fg-line">-->
<!--                <label for="position-middle">Meio</label>-->
<!--                <input type="number" ng-model="__action__.position.middle"-->
<!--                       id="position-middle" class="form-control" autocomplete="off">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            <div class="form-group fg-line">-->
<!--                <label for="position-bottom">Inferior</label>-->
<!--                <input type="number" ng-model="__action__.position.bottom"-->
<!--                       id="position-bottom" class="form-control" autocomplete="off">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            <div class="form-group fg-line">-->
<!--                <label for="position-fab">Flutuante</label>-->
<!--                <input type="number" ng-model="__action__.position.fab"-->
<!--                       id="position-fab" class="form-control" autocomplete="off">-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->

    <div class="no-padding form--action">
        <button type="button" class="btn btn-sm btn-danger"
                ng-click="removeActionFromForm(view.popup.data['forms'].actions, __action__)"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-delete"></i>
        </button>
        <button type="button" class="btn btn-sm btn-success"
                ng-click="addActionToForm(view.popup.data['forms'], view.model.record, $index)"
                style="width: 28px; height: 28px; padding: 0;">
            <i class="zmdi zmdi-plus"></i>
        </button>
    </div>

</div>

<div class="col-sm-12 no-padding" ng-hide="view.popup.data['forms'].actions.length">
    <button type="button" class="btn btn-sm btn-success"
            ng-click="addActionToForm(view.popup.data['forms'])">
        <i class="zmdi zmdi-plus"></i>
    </button>
</div>