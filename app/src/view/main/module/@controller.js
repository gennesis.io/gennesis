/**
 *
 */
App.register("ModuleController", function($scope, $rootScope, ServiceListener) {

    ServiceListener.register('after.add', 'main.module.add', function (record) {
        record['project'] = App.project.driver;
    });
});