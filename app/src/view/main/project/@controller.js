/**
 *
 */
App.register("ProjectController", function($scope, $rootScope, ServiceListener) {

    ['add', 'edit'].forEach(function (index) {

        ServiceListener.register('after.save', 'main.project.' + index, function (record, data, primary) {

            if (record.active) {
                App.project = record;
                $rootScope.project = record;
                localStorage.setItem('gennesis-project', JSON.stringify(record));
            }
        });
    });

});