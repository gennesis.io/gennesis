<?php

/* {{refactor.namespace}} */
namespace Apocalipse\Project\Model\Main;


use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Model\Dynamo;

/**
 * {{refactor.class}}
 * Class Todo
 * @package Apocalipse\Project\Model\Main
 */
class Todo extends Dynamo
{
    /**
     * {{refactor.comment}}
     * Todo constructor.
     */
    public function __construct()
    {
        /* {{refactor.construct}} */
        parent::__construct(module(self::class), entity(self::class));

        $this->skeleton();

        $projects = (new Project())->read(new Filter('active', true));
        if ($projects->size()) {
            /** @var Record $project */
            foreach ($projects as $project) {
                $this->modifier = new Modifier(new Filter('project', $project->get('driver')));
            }
        }
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function before($action, Record $record)
    {
        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function after($action, Record $record)
    {
        return true;
    }
}