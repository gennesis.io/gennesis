<?php

/* {{refactor.namespace}} */
namespace Apocalipse\Project\Model\Main;


use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Action;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\Directory;
use Apocalipse\Core\Model\Dynamo;

/**
 * {{refactor.class}}
 * Class Module
 * @package Apocalipse\Project\Model\Main
 */
class Module extends Dynamo
{
    /**
     * @var \stdClass
     */
    private $root;

    /**
     * @var Record
     */
    private $before;

    /**
     * Module constructor.
     */
    public function __construct()
    {
        /* {{refactor.construct}} */
        parent::__construct(module(self::class), entity(self::class));

        $this->skeleton();

        $projects = (new Project())->read(new Filter('active', true));
        if ($projects->size()) {
            /** @var Record $project */
            foreach ($projects as $project) {
                $this->modifier = new Modifier(new Filter('project', $project->get('driver')));
                $this->root = $project->get('path');
                $this->setDriver($project->get('driver'));
            }
        }
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function before($action, Record $record)
    {
        $this->before = $this->read(new Filter('_id', $record->get('_id')))->current();

        return parent::before($action, $record);
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function after($action, Record $record)
    {
        $after = true;

        if (Directory::exists($this->root)) {

            switch ($action) {

                case Action::SAVE:
                case Action::UPDATE:
                case Action::CREATE:

                    if (!$this->before->get('name')) {

                        $after = $this->createModule($record->get('name'));

                    } else if ($this->before->get('name') !== $record->get('name')){

                        $after = $this->renameModule($this->before->get('name'), $record->get('name'));
                    }
                    break;

                case Action::DELETE:

                    $after = $this->removeModule($record->get('name'));
                    break;
            }
        }

        return $after;
    }

    /**
     * @param $name
     * @return bool
     */
    public function createModule($name)
    {
        $model = path($this->root, 'model', $name);
        $scaffold = path($this->root, 'domain', 'scaffold', $name);

        $m = Directory::exists($model);
        if (!$m) {
            $m = Directory::make($model);
            if (!$m) {
                Wrapper::err('Can not create dir "' . $model . '"');
            }
        }

        $s = Directory::exists($scaffold);
        if (!$s) {
            $s = Directory::make($scaffold);
            if (!$s) {
                Wrapper::err('Can not create dir "' . $scaffold . '"');
            }
        }

        return $m && $s;
    }

    /**
     * @param $source
     * @param $target
     * @return bool
     */
    public function renameModule($source, $target)
    {
        $model_source = path($this->root, 'model', $source);
        $model_target = path($this->root, 'model', $target);
        $scaffold_source = path($this->root, 'domain', 'scaffold', $source);
        $scaffold_target = path($this->root, 'domain', 'scaffold', $target);

        $m = Directory::exists($model_source);
        if ($m && $source) {
            $m = Directory::rename($model_source, $model_target);
            if (!$m) {
                Wrapper::err('Can not rename dir "' . $model_source . '" to "' . $model_target . '"');
            }
        } else {
            $m = Directory::make($model_source);
            if (!$m) {
                Wrapper::err('Can not create dir "' . $model_source . '"');
            }
        }

        $s = Directory::exists($scaffold_source);
        if ($s && $source) {
            $s = Directory::rename($scaffold_source, $scaffold_target);
            if (!$s) {
                Wrapper::err('Can not rename dir "' . $scaffold_source . '" to "' . $scaffold_target . '"');
            }
        } else {
            $s = Directory::make($scaffold_source);
            if (!$s) {
                Wrapper::err('Can not create dir "' . $scaffold_source . '"');
            }
        }

        return $m && $s;
    }

    /**
     * @param $name
     * @return bool
     */
    public function removeModule($name)
    {
        $model = path($this->root, 'model', $name);
        $scaffold = path($this->root, 'domain', 'scaffold', $name);

        $m = Directory::exists($model);
        if ($m) {
            $m = Directory::remove($model);
            if (!$m) {
                Wrapper::err('Can not remove dir "' . $model . '"');
            }
        } else {
            $m = true;
        }

        $s = Directory::exists($scaffold);
        if ($s) {
            $s = Directory::remove($scaffold);
            if (!$s) {
                Wrapper::err('Can not remove dir "' . $scaffold . '"');
            }
        } else {
            $s = true;
        }

        return $m && $s;
    }

}