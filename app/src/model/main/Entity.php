<?php

/* {{refactor.namespace}} */
namespace Apocalipse\Project\Model\Main;


use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Action;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\Directory;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Helper\Text;
use Apocalipse\Core\Model\Dynamo;

/**
 * {{refactor.class}}
 * Class Entity
 * @package Apocalipse\Project\Model\Main
 */
class Entity extends Dynamo
{
    /**
     * @var \stdClass
     */
    private $root;

    /**
     * @var string
     */
    private $namespace;

    /**
     * @var Record
     */
    private $before;

    /**
     * Entity constructor.
     */
    public function __construct()
    {
        /* {{refactor.construct}} */
        parent::__construct(module(self::class),  entity(self::class));

        $this->skeleton();

        $projects = (new Project())->read(new Filter('active', true));
        if ($projects->size()) {
            /** @var Record $project */
            foreach ($projects as $project) {
                $this->setDriver($project->get('driver'));
            }
        }
    }

    /**
     * @param Record $record
     * @return \stdClass
     */
    private function getRoot(Record $record)
    {
        if (!$this->root) {

            $project = null;
            $modules = (new Module())->read(new Filter('name', $record->get('module')));
            foreach ($modules as $module) {
                $projects = (new Project())->read(new Filter('driver', $module->get('project')));
                if ($projects->size()) {
                    $project = $projects->current();
                }
            }

            if ($project) {
                $this->root = pathToFile($project->get('path'));
                $this->namespace = $project->get('namespace');
            } else {
                Wrapper::err('Project not found');
            }
        }

        return $this->root;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function before($action, Record $record)
    {
        $this->before = $this->read(new Filter('code', $record->get('code')))->current();

        $forms = $record->get('forms');

        if (is_array($forms)) {

            foreach ($forms as $i => $form) {

                foreach ($form->fields as $j => $field) {
                    if (isset($field->__fields)) {
                        unset($field->__fields);
                    }
                    if (isset($field->toggled)) {
                        unset($field->toggled);
                    }
                    $form->fields[$j] = $field;
                }

                if (isset($form->actions)) {

                  foreach ($form->actions as $j => $action) {
                      if (isset($action->__fields)) {
                          unset($action->__fields);
                      }
                      if (isset($action->toggled)) {
                          unset($action->toggled);
                      }
                      $form->actions[$j] = $action;
                  }
                }

                $forms[$i] = $form;
            }
            $record->set('forms', $forms);
        }

        return parent::before($action, $record);
    }

    /**
     * @param Record $record
     * @return bool
     */
    public function createEntity(Record $record)
    {
        return $this->scaffold($record);
    }

    /**
     * @param Record $record
     * @return bool
     */
    private function updateEntity(Record $record)
    {
        if (
            ($this->before->get('module') !== $record->get('module'))
            ||
            ($this->before->get('entity') !== $record->get('entity'))
        ) {

            $source = path($this->getRoot($record), 'model', $this->before->get('module'), $this->before->get('entity') . '.' . 'php');
            if (File::exists($source)) {
                $target = path($this->getRoot($record), 'model', $record->get('module'), $record->get('entity') . '.' . 'php');
                $model = File::move($source, $target);
                if (!$model) {
                    Wrapper::err('Can not move file "' . $source . '"');
                    return false;
                }
            }

            $source = path($this->getRoot($record), 'domain', 'scaffold', $this->before->get('module'), camelCaseToDashes($this->before->get('entity')));
            if (Directory::exists($source)) {
                $target = path($this->getRoot($record), 'domain', 'scaffold', $record->get('module'), camelCaseToDashes($record->get('entity')));
                $scaffold = Directory::rename($source, $target);
                if (!$scaffold) {
                    Wrapper::err('Can not move file "' . $source . '"');
                    return false;
                }
            }
        }

        return $this->scaffold($record);
    }

    /**
     * @param Record $record
     * @return bool
     */
    public function removeEntity(Record $record)
    {
        $model = path($this->getRoot($record), 'model', $record->get('module'), $record->get('entity') . '.' . 'php');
        $scaffold = path($this->getRoot($record), 'domain', 'scaffold', $record->get('module'), camelCaseToDashes($record->get('entity')));

        $try = 0;
        $done = 0;

        $try++;
        if (File::destroy($model)) {
            $done++;
        }

        $try++;
        $bool = Directory::exists($scaffold);
        if ($bool) {
            $bool = Directory::remove($scaffold);
            if (!$bool) {
                Wrapper::err('Can not remove dir "' . $scaffold . '"');
            } else {
                $done++;
            }
        } else {
            $done++;
        }

        return $try === $done;
    }

    /**
     * @param Record $record
     * @return bool
     */
    private function scaffold(Record $record)
    {
        $model = path($this->getRoot($record), 'model', $record->get('module'), $record->get('entity') . '.' . 'php');
        $scaffold = path($this->getRoot($record), 'domain', 'scaffold', $record->get('module'), camelCaseToDashes($record->get('entity')));

        if (File::exists($model)) {
            $rewrite = true;
            $class = File::read($model);
        } else {
            $rewrite = false;
            $class = File::read(path(true, 'storage', 'template', 'model.php.template'));
        }
        $class = $this->refactor($class, $record, $rewrite);

        $m = File::write($model, $class);

        $s = $this->dir($scaffold);

        if ($s) {

            $definition = new \stdClass();

            $definition->properties = new \stdClass();
            $definition->properties->module = $record->get('module');
            $definition->properties->entity = $record->get('entity');
            $definition->properties->id = '';
            $definition->properties->name = $record->get('name');
            $definition->properties->description = $record->get('description');

            $definition->driver = new \stdClass();
            $definition->driver->collection = $record->get('collection');
            $definition->driver->id = $record->get('driver');

            $definition->items = [];

            $items = (array) $record->get('items');
            foreach ($items as $item) {
                unset($item->__fields);
                $definition->items[] = (array) $item;
            }

            $options = [];

            foreach ($definition->items as $key => $field) {
                $field = (object) $field;
                $field->name = ($record->get('prefix') ? $record->get('prefix') . '_' : '') . $field->name;

                $options[$field->name] = (object)['options'=>'', 'settings'=>'', 'relationship' =>''];

                if (isset($field->options)) {
                    $options[$field->name]->options = $field->options;
                }
                if (isset($field->settings)) {
                    $options[$field->name]->settings = $field->settings;
                }
                if (isset($field->relationship)) {
                    $options[$field->name]->relationship = $field->relationship;
                }
                $definition->items[$key] = $field;
            }

            $s = File::write(path($scaffold, 'definition' . '.' . 'json'), Json::encode($definition));

            if ($s) {

                $try = 0;
                $done = 0;

                $folders = ['action', 'form', 'language'];
                foreach ($folders as $name) {
                    $try++;
                    $folder = path($scaffold, $name);
                    if ($this->dir($folder)) {
                        $done++;

                        $resources = (array) $record->get($name . 's');

                        foreach ($resources as $resource) {

                            if (isset($resource->__fields)) {
                                unset($resource->__fields);
                            }
                            if (isset($resource->__items_actions)) {
                                unset($resource->__items_actions);
                            }
                            if (isset($resource->__items_fields)) {
                                unset($resource->__items_fields);
                            }

                            $operation = Json::decode(Json::encode($resource));

                            $operation->type = $name;
                            $nameless = 0;

                            if (isset($operation->fields) && is_array($operation->fields)) {

                                foreach ($operation->fields as $key => $field) {

                                    if (isset($field->__fields)) {
                                        unset($field->__fields);
                                    }

                                    if (isset($field->key)) {
                                        $field_key = $field->key;
                                    } else {
                                        $nameless++;
                                        $field_key = 'nameless_' . $nameless;
                                    }

                                    $field->key = ($record->get('prefix') ? $record->get('prefix') . '_' : '') . $field_key;

                                    if (isset($options[$field->key])) {
                                        if ($options[$field->key]->options) {
                                            $field->options = $options[$field->key]->options;
                                        }
                                        if ($options[$field->key]->settings) {
                                            $field->settings = $options[$field->key]->settings;
                                        }
                                        if ($options[$field->key]->relationship) {
                                            $field->relationship = $options[$field->key]->relationship;
                                        }
                                    }
                                    $operation->fields[$key] = $field;
                                }
                            }

                            if (isset($operation->id)) {
                                File::write(path($folder, $operation->id . '.' . 'json'), Json::encode($operation));
                            } else {
                                Wrapper::info($resource);
                            }
                        }
                    }
                }

                $s = $try === $done;
            }
        }

        return $m && $s;
    }

    /**
     * @param $class
     * @param Record $record
     * @param bool $rewrite
     * @return string
     */
    public function refactor($class, Record $record, $rewrite = false)
    {
        if ($rewrite) {
            $refactoring = explode(PHP_EOL, $class);
            //TODO: recover lines from template
            $refactor = '';
            foreach ($refactoring as $number => $line) {

                switch ($refactor) {
                    case 'namespace':
                        $refactoring[$number] = 'namespace {entity.package};';
                        $refactor = '';
                        break;
                    case 'class':
                        $refactoring[$number] = ' * Class {entity.name}';
                        $refactor = 'class-1';
                        break;
                    case 'class-1':
                        $refactoring[$number] = ' * @package {entity.package}';
                        $refactor = 'class-2';
                        break;
                    case 'class-2':
                        $refactoring[$number] = ' */';
                        $refactor = 'class-3';
                        break;
                    case 'class-3':
                        $refactoring[$number] = 'class {entity.name} extends Dynamo';
                        $refactor = '';
                        break;
                    case 'construct':
                        $refactoring[$number] = "        parent::__construct(module(self::class), entity(self::class));";
                        $refactor = '';
                        break;
                }
                if (!$refactor) {
                    switch (trim($line)) {
                        case '/* {{refactor.namespace}} */':
                            $refactor = 'namespace';
                            break;
                        case '* {{refactor.class}}':
                            $refactor = 'class';
                            break;
                        case '/* {{refactor.construct}} */':
                            $refactor = 'construct';
                            break;
                    }
                }
            }

            $class = implode(PHP_EOL, $refactoring);
        }

        $replaces = [];
        if ($this->namespace) {
            $replaces['{entity.package}'] = pathToNamespace($this->namespace) . '\\' . 'Model' . '\\' . dashesToCamelCase($record->get('module'));
        }
        $replaces['{entity.name}'] = $record->get('entity');
        $replaces['{entity.collection}'] = $record->get('collection');
        $replaces['{entity.driver}'] = $record->get('driver') ? "'" . $record->get('driver') . "'" : "null";

        foreach ($replaces as $key => $value) {
            $class = Text::replace($class, $key, $value);
        }

        return $class;
    }

    /**
     * @param $path
     * @return bool
     */
    public function dir($path)
    {
        $dir = Directory::exists($path);
        if (!$dir) {
            $dir = Directory::make($path);
            if (!$dir) {
                Wrapper::err('Can not create dir "' . $path . '"');
            }
        }

        return $dir;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function after($action, Record $record)
    {
        $after = true;

        if (Directory::exists($this->getRoot($record))) {

            switch ($action) {

                case Action::SAVE:
                case Action::UPDATE:
                case Action::CREATE:

                    if (!$this->before->get('entity')) {

                        $after = $this->createEntity($record);

                    } else {

                        $after = $this->updateEntity($record);
                    }
                    break;

                case Action::DELETE:

                    $after = $this->removeEntity($record);
                    break;
            }
        }

        return $after;
    }
}
