<?php

/* {{refactor.namespace}} */
namespace Apocalipse\Project\Model\Main;


use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Model\Dynamo;

/**
 * {{refactor.class}}
 * Class Project
 * @package Apocalipse\Project\Model\Main
 */
class Project extends Dynamo
{
    /**
     * Project constructor.
     */
    public function __construct()
    {
        /* {{refactor.construct}} */
        parent::__construct(module(self::class), entity(self::class));

        $this->skeleton();
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function after($action, Record $record)
    {
        if ($record->get('active')) {
            $projects = $this->read(new Filter('code', $record->get('code'), '!='));
            foreach ($projects as $project) {
                $project->set('active', false);
                $this->update($project);
            }
        }

        return parent::after($action, $record);
    }
}