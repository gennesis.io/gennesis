<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Project\Controller
 | @file: ConcursoController.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 14:11
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Project\Controller;


use Apocalipse\Basic\Controller\ViewController;
use Apocalipse\Core\Domain\Content\Scope;

/**
 * Class ConcursoController
 * @package Apocalipse\Project\FaleConoscoController
 */
class FaleConoscoController extends ViewController
{
    /**
     * @param Scope $scope
     * @return mixed
     */
    public function scope(Scope $scope)
    {
        return $scope;
    }
}