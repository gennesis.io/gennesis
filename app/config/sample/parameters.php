<?php

if (!defined('__TOKEN_AUTH__')) {
    define('__TOKEN_AUTH__', 'X-Auth-Token');
}

if (!defined('__TOKEN_NONCE__')) {
    define('__TOKEN_NONCE__', 'X-Nonce-Token');
}

if (!defined('__TOKEN__')) {
    define('__TOKEN__', '57918d5e3855a');
}

if (!defined('__APP_VERSION__')) {
    define('__APP_VERSION__', '0.0.0.2');
}

if (!defined('__APP_TEST__')) {
    define('__APP_TEST__', true);
}

if (!defined('__APP_DEBUG__')) {
    define('__APP_DEBUG__', false);
}

if (!defined('__APP_DEFAULT_DRIVER__')) {
    define('__APP_DEFAULT_DRIVER__', 'default');
}

if (!defined('__APP_DOMAIN__')) {
    define('__APP_DOMAIN__', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
}

if (!defined('__APP_TRACKER_MAX__')) {
    define('__APP_TRACKER_MAX__', 1000);
}

if (!defined('__APP_TRACKER_BLACKLIST__')) {
    define('__APP_TRACKER_BLACKLIST__', json_encode(['']));
}
