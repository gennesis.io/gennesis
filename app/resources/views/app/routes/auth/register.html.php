<div class="container container--login">

    <div class="card z-depth-1">

        <div class="card-header" style="border-bottom: 1px solid #E0E0E0;background: #fafafa;">
            <div class="pull-left">
                <h2>Cadastrar</h2>
            </div>
            <div class="pull-right">
                <button class="btn btn-danger waves-effect" ng-click="go('auth/login')"> J&aacute; tenho cadastro
                </button>
            </div>
            <br style="clear: both;">
        </div>

        <div class="card-body card-padding">

            <form name="form" ng-init="credential = {}">

                <div class="form-group fg-line">
                    <label for="no-cache-1">E-mail</label>
                    <input type="email" required class="form-control" ng-model="credential.email" id="no-cache-1"
                           placeholder="Informe seu e-mail" autocomplete="off">
                </div>
                <div class="form-group fg-line">
                    <label for="no-cache-2">Login</label>
                    <input type="text" required class="form-control" ng-model="credential.login" id="no-cache-2"
                           placeholder="Informe um nome de usuário" autocomplete="off">
                </div>
                <div class="form-group fg-line">
                    <label for="no-cache-3">Senha</label>
                    <input type="text" required class="form-control" ng-model="credential.password" id="no-cache-3"
                           placeholder="Informe a senha que vai usar para acessar sua conta" autocomplete="off">
                </div>

                <div style="height: 70px;border-top: 1px solid #ddd;padding: 5px 20px 0 20px;margin: 0 -25px;">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="credential.agree">
                            <i class="input-helper"></i>
                            Aceito os <a href="">termos de uso</a> e <a href="">regulamento do site</a>
                        </label>
                    </div>

                    <div class="pull-left">
                        <button type="submit" class="btn btn-theme waves-effect" ng-click="register(credential)"
                                ng-disabled="!(credential.agree && !form.$invalid)"
                                style="padding: 6px 40px; font-size: 14px;">Cadastrar
                        </button>
                    </div>

                    <div class="pull-left">
                        <div class="preloader pl-sm" ng-show="logging">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                            </svg>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>