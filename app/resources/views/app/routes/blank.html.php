<div class="container">
    <div class="block-header">
        <h2>Blank</h2>

        <ul class="actions">
            <li>
                <a class="icon-pop" href="">
                    <i class="zmdi zmdi-trending-up"></i>
                </a>
            </li>
            <li>
                <a class="icon-pop" href="">
                    <i class="zmdi zmdi-check-all"></i>
                </a>
            </li>
            <li class="dropdown" uib-dropdown>
                <a class="icon-pop" href="" uib-dropdown-toggle>
                    <i class="zmdi zmdi-more-vert"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="">Refresh</a>
                    </li>
                    <li>
                        <a href="">Manage Widgets</a>
                    </li>
                    <li>
                        <a href="">Widgets Settings</a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>

    <div class="card z-depth-1">
        <div class="card-header">
            <h2>Basic Example <small>Wrap any text and an optional dismiss button in '.alert' and one of the four contextual classes (e.g., .alert-success) for basic alert messages.</small></h2>
        </div>

        <div class="card-body card-padding">
            <div class="alert alert-success" role="alert">Well done! You successfully read this important alert message.</div>
            <div class="alert alert-info" role="alert">Heads up! This alert needs your attention, but it's not super important.</div>
            <div class="alert alert-warning" role="alert">Warning! Better check yourself, you're not looking too good.</div>
            <div class="alert alert-danger" role="alert">Oh snap! Change a few things up and try submitting again.</div>
        </div>
    </div>