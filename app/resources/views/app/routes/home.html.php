<div class="container">

    <div class="col-sm-6">
        <!-- Todo Lists -->
        <div class="z-depth-1">

            <div id="todo-lists" data-ng-controller="todoCtrl as tctrl">
                <div class="tl-header">
                    <h2>Todo Lists</h2>
                    <small>Add, edit and manage your Todo Lists</small>
                </div>

                <div class="clearfix"></div>

                <div class="tl-body">
                    <div id="add-tl-item" data-ng-class="{ 'toggled': tctrl.addTodoStat }"
                         data-ng-click="tctrl.addTodoStat = true">

                        <button type="button" class="btn btn-float waves-effect waves-circle btn-default"
                                data-ng-click="tctrl.addTodo($event)">
                            <i class="zmdi zmdi-plus"></i>
                        </button>

                        <div class="add-tl-body">
                            <input placeholder="Title..." data-ng-model="tctrl.title"/>
                            <textarea placeholder="Todo..." data-ng-model="tctrl.todo"></textarea>

                            <div class="add-tl-actions">
                                <a class="zmdi zmdi-close" data-tl-action="dismiss"
                                   data-ng-click="tctrl.cancel($event)"></a>
                                <a class="zmdi zmdi-check" data-tl-action="save"
                                   data-ng-click="tctrl.save($event)"></a>
                            </div>
                        </div>
                    </div>


                    <div class="checkbox media" data-ng-repeat="_todo in tctrl.rows">
                        <div class="pull-right">
                            <ul class="actions actions-alt">
                                <li class="dropdown" uib-dropdown>
                                    <a href="" uib-dropdown-toggle>
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="" data-ng-click="tctrl.delete(_todo)">Delete</a></li>
                                        <li><a href="" data-ng-click="tctrl.archive(_todo)">Archive</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="media-body">
                            <label>
                                <input type="checkbox" ng-checked="_todo.done" data-ng-click="tctrl.toggleDone(_todo)">
                                <i class="input-helper"></i>
                                <span>{{ _todo.todo }}</span>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
