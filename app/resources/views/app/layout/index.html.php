<!-- container -->
<div class="container-fluid">

    <!-- card -->
    <div class="card z-depth-1">

        <!-- top bar -->
        <div class="listview lv-bordered lv-lg">

            <div class="lv-header-alt clearfix">

                <h2 class="lvh-label hidden-xs">
                    {{ view.model.getProperties().name }}
                    <!-- <small> {{ view.model.getProperties().description }}  </small>-->
                </h2>

                <div class="lvh-search" ng-show="view.page.search.visible">
                    <input type="text" placeholder="Digite para pesquisar..." class="lvhs-input"
                           ng-model="view.page.search.text" ng-change="view.recover(index, view.page.settings.current)">

                    <i class="lvh-search-close" ng-click="view.page.search.hide()">&times;</i>
                </div>

                <ul class="lv-actions actions">
                    <li ng-repeat="_action in view.model.getActions(index, 'top')">
                        <a href="javascript:void(0);" ng-click="view.operation(_action.id)">
                            <i ng-class="_action.classIcon"></i>
                        </a>
                    </li>
                    <li style="display: none;">
                        <a href="" ng-click="view.page.search.show()"><i class="zmdi zmdi-search"></i></a>
                    </li>
                </ul>

                <ul class="pagination">
                    <li ng-class="{disabled: view.page.first()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, 1)">&laquo;</a>
                    </li>
                    <li ng-class="{disabled: view.page.previous()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.settings.current -1 )">&lsaquo;</a>
                    </li>
                    <li>
                        <input type="number" class="input" ng-model="view.page.settings.current" min="1"
                               ng-change="view.recover(index, view.page.settings.current)"/>
                    </li>
                    <li>
                        <div class="input--pages"> / {{ view.page.pages }}</div>
                    </li>
                    <li ng-class="{disabled: view.page.next()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.settings.current + 1)">&rsaquo;</a>
                    </li>
                    <li ng-class="{disabled: view.page.last()}">
                        <a href="javascript:void(0);" ng-click="view.recover(index, view.page.pages)">&raquo;</a>
                    </li>
                </ul>

                <div class="pull-right">
                    <input type="text" class="input input--search"
                           ng-model="view.page.search.text" ng-change="view.recover(index, view.page.settings.current)">

                    <input type="number" style="width: 70px;" class="input" ng-model="view.page.settings.skip" min="1"
                           ng-change="view.recover(index, view.page.settings.current)">
                </div>

                <ul class="lv-actions actions">
                    <li>
                        <a href="javascript:void(0);" title="Erro de Pagina&ccedil;&atilde;o" class="a-danger" style="height: 26px;width: 26px;padding: 3px 0;"
                           ng-show="view.page.invalid" ng-click="view.recover(index, view.page.pages)">
                            <i class="zmdi zmdi-bug zmdi-hc-fw"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" title="Limpar Filtro" class="a-danger" style="height: 26px;width: 26px;padding: 3px 0;"
                           ng-show="view.page.search.text">
                            <i class="zmdi zmdi-filter-list zmdi-hc-fw"></i>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

        <!-- data list -->
        <div class="listview lv-bordered lv-lg face list">

            <div class="lv-body">

                <div class="card--form no-records" ng-hide="view.model.recordSet.data.length">
                    No records to show
                </div>

                <div class="card--form table-responsive" ng-show="view.model.recordSet.data.length">
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                        <tr>
                            <th width="150px">#</th>
                            <th ng-class="_field.wrapper" ng-repeat="_field in view.model.getOperation(index).fields">
                                {{ _field.label }}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="_record in view.model.recordSet.data">
                            <td>
                                <span ng-repeat="_action in view.model.getActions(index, 'middle')">
                                    <button title="" type="button" class="btn" ng-class="_action.className"
                                            ng-click="view.operation(_action.id, _record, _action.after)">
                                        <i class="{{_action.classIcon}}"></i>
                                    </button>
                                </span>
                            </td>
                            <td ng-class="_field.wrapper" ng-repeat="_field in view.model.getOperation(index).fields">
                                {{ view.out.print(_record[_field.key], _field.component) }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </div>

</div>

<div class="fab-button--toolbar">

    <button type="button" class="btn btn-float" ng-repeat="_action in view.model.getActions(index, 'fab')"
            ng-class="_action.className"
            ng-click="view.operation(_action.id, view.model.record, _action.after)"
            ng-disabled="view.model.getOperation(_action.id).type === 'action' ? form.$invalid : false">
        <i class="{{ _action.classIcon }}"></i>
    </button>
</div>
