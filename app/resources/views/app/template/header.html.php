<ul class="header-inner clearfix">

    <li id="menu-trigger" data-target="mainmenu" data-toggle-sidebar data-model-left="mactrl.sidebarToggle.left"
        data-ng-class="{ 'open': mactrl.sidebarToggle.left === true }">
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>

    <li class="hidden-xs">
        <a href="<?php url('index'); ?>" class="font-play m-l-10" data-ng-click="mactrl.sidebarStat($event)">
            <?php out(config('app.name')); ?>
        </a>
    </li>

    <li ng-controller="GennesisController" ng-click="showProject()"
        style="margin: 5px 30px;background: white;padding: 5px 20px;border-radius: 2px;">
        {{ project.name }}
    </li>

    <li class="pull-right">
        <ul class="top-menu">

            <li id="toggle-width" class="dropdown" ng-click="copen = !copen" ng-class="{'open': copen}">
                <a href="javascript:void(0);">
                    <i class="tm-icon zmdi zmdi-view-dashboard zmdi-hc-fw"></i>
                </a>
                <ul class="dropdown-menu dm-icon pull-right" style="width: 72px; min-width: 0;">
                    <li>
                        <a href="javascript:void(0);" ng-click="mactrl.setLayoutType('1')"
                           ng-style="{opacity: (mactrl.layoutType === '1') ? '0.3' : '1', padding: '0px 10px'}">
                            <img src="<?php asset('@/images/window-icon--sidebar.min.png'); ?>">
                        </a>
                    </li>
                    <li class="divider hidden-xs"></li>
                    <li>
                        <a href="javascript:void(0);" ng-click="mactrl.setLayoutType('0')"
                           ng-style="{opacity: (mactrl.layoutType === '0') ? '0.3' : '1', padding: '0px 10px'}">
                            <img src="<?php asset('@/images/window-icon--no-sidebar.min.png'); ?>">
                        </a>
                    </li>
                </ul>
            </li>

            <li id="top-search">
                <a href="" data-ng-click="hctrl.openSearch()"><i class="tm-icon zmdi zmdi-search"></i></a>
            </li>

            <li class="dropdown" ng-click="dopen = !dopen" ng-class="{'open': dopen}">
                <a href="javascript:void(0);" ng-show="isLogged()">
                    <i class="tm-icon zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu dm-icon pull-right">
                    <li>
                        <a href="javascript:void(0);" ng-click="fullScreen()">
                            <i class="zmdi zmdi-fullscreen"></i> Tela Cheia
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" ng-click="clearCache()">
                            <i class="zmdi zmdi-delete"></i> Limpar Preferências
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" ng-click="logout()">
                            <i class="zmdi zmdi-power zmdi-hc-fw"></i> Sair
                        </a>
                    </li>
                </ul>
            </li>


        </ul>
    </li>
</ul>

<!-- Top Search Content -->
<div id="top-search-wrap">
    <div class="tsw-inner">
        <i id="top-search-close" class="zmdi zmdi-arrow-left" data-ng-click="hctrl.closeSearch()"></i>
        <input type="text">
    </div>
</div>
