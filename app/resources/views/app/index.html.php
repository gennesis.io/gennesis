<!DOCTYPE html>
<!--[if IE 9 ]>
<html class="ie9" data-ng-app="App" data-ng-controller="materialadminCtrl as mactrl">
<![endif]-->
<![if IE 9 ]>
<html data-ng-app="App" data-ng-controller="materialadminCtrl as mactrl">
<![endif]>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> <?php out(config('app.name')); ?> </title>

    <link rel="shortcut icon" href="<?php asset('@/favicon/favicon.ico'); ?>" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php asset('@/favicon/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php asset('@/favicon/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php asset('@/favicon/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php asset('@/favicon/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php asset('@/favicon/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php asset('@/favicon/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php asset('@/favicon/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php asset('@/favicon/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php asset('@/favicon/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php asset('@/favicon/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php asset('@/favicon/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php asset('@/favicon/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php asset('@/favicon/favicon-16x16.png'); ?>">
    <meta name="msapplication-TileImage" content="<?php asset('@/favicon//ms-icon-144x144.png'); ?>">
    <link rel="manifest" href="<?php asset('@/favicon/manifest.json'); ?>">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#000000">

    <!-- Vendor CSS -->
    <link href="<?php asset('app/vendors/animate.css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'); ?>" rel="stylesheet">
    <link href="<?php asset('google-fonts/play/index.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/bootstrap-sweetalert/lib/sweet-alert.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/angular-loading-bar/src/loading-bar.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/fullcalendar/dist/fullcalendar.min.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/vendors/chosen/chosen.min.css'); ?>" rel="stylesheet">

    <!-- CSS -->
    <link href="<?php asset('app/css/app.min.1.css'); ?>" rel="stylesheet" id="app-level">
    <link href="<?php asset('app/css/app.min.2.css'); ?>" rel="stylesheet">
    <link href="<?php asset('app/css/custom.css'); ?>" rel="stylesheet">

    <link href="<?php asset('@/css/custom.css'); ?>" rel="stylesheet">

</head>

<body data-ng-class="{ 'sw-toggled': mactrl.layoutType === '1', 'logged': logged}" ng-controller="AppController">

<header id="header" data-current-skin="indigo" data-ng-include src="'template/header.html'" data-ng-controller="headerCtrl as hctrl"></header>

<section id="main">

    <aside id="sidebar" data-ng-include src="'template/sidebar-left.html'" data-ng-class="{ 'toggled': mactrl.sidebarToggle.left === true }"></aside>

    <section id="content" class="page-view" ng-view></section>

</section>

<!-- Core -->
<script src="<?php asset('app/vendors/jquery/dist/jquery.min.js'); ?>"></script>

<!-- Angular -->
<script src="<?php asset('app/vendors/angular/angular.js'); ?>"></script>
<script src="<?php asset('app/vendors/angular/angular-route.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/angular/angular-cookies.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/angular-animate/angular-animate.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/angular-resource/angular-resource.min.js'); ?>"></script>

<!-- Angular Modules -->
<script src="<?php asset('app/vendors/angular-loading-bar/src/loading-bar.js'); ?>"></script>

<!-- Common Vendors -->
<script src="<?php asset('app/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/bootstrap-sweetalert/lib/sweet-alert.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/Waves/dist/waves.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/bootstrap-growl/bootstrap-growl.min.js'); ?>"></script>


<!-- Placeholder for IE9 -->
<!--[if IE 9 ]>
<script src="<?php asset('app/vendors/jquery-placeholder/jquery.placeholder.min.js'); ?>"></script>
<![endif]-->

<!-- Using below vendors in order to avoid misloading on resolve -->
<script src="<?php asset('app/vendors/input-mask/input-mask.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/fileinput/fileinput.min.js'); ?>"></script>
<script src="<?php asset('app/vendors/chosen/chosen.jquery.js'); ?>"></script>
<script src="<?php asset('app/vendors/angular-chosen-localytics/chosen.js'); ?>"></script>


<!-- App level -->
<script src="<?php asset('app/js/app.js'); ?>"></script>
<script src="<?php asset('@/config/' . __APP_ENV__ . '/env.js'); ?>"></script>
<script src="<?php asset('app/js/config.js'); ?>"></script>
<script src="<?php asset('@/js/custom.js'); ?>"></script>

<script src="<?php asset('app/js/services.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceApi.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceModel.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceOperation.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceLayout.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceInput.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceOutput.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceDialog.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServicePopup.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceListener.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServiceView.js'); ?>"></script>
<script src="<?php asset('app/js/factory/ServicePagination.js'); ?>"></script>

<script src="<?php asset('app/js/controllers/AppController.js'); ?>"></script>
<script src="<?php asset('app/js/controllers/LoginController.js'); ?>"></script>
<script src="<?php asset('app/js/controllers/ViewController.js'); ?>"></script>

<!-- Template Modules -->
<script src="<?php asset('app/js/modules/calendar.js'); ?>"></script>
<script src="<?php asset('app/js/modules/components.js'); ?>"></script>
<script src="<?php asset('app/js/modules/form.js'); ?>"></script>
<script src="<?php asset('app/js/modules/template.js'); ?>"></script>
<script src="<?php asset('app/js/modules/ui.js'); ?>"></script>
<script src="<?php asset('app/js/modules/ui-bootstrap.js'); ?>"></script>
</body>
</html>

