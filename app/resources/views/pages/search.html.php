<?php

/** @var \Apocalipse\Core\Domain\Content\Renderer $this */
/** @var \Apocalipse\Core\Domain\Content\Data $data */

$this->extend('index', 'content');

$data = $this->scope->data;

?>

<div class="container main-container">

    <div class="col-sm-12">
        <div class="card">
            <h4>Pesquisar em <?php out(config('app.name')); ?></h4>
            <hr>
            <p>
                Pesquisando por <pre><?php out($data->getGet()->get('for')); ?></pre>
        </div>
    </div>
</div>