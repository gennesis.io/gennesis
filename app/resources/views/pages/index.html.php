<?php

/** @var \Apocalipse\Core\Domain\Content\Renderer $this */

$this->extend('index', 'content');

?>

<div class="container main-container">

    <div class="col-sm-12">
        <div class="card">
            <h4><?php out(config('app.name')); ?></h4>
            <hr>
            <p>
                Projeto de exemplo de uso do Apocalipse
            </p>
        </div>
    </div>
</div>