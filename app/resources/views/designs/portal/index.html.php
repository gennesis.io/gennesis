<?php

use Apocalipse\Core\Helper\Error;
use Apocalipse\Core\Helper\Console;

/** @var \Apocalipse\Core\Domain\Content\Renderer $this */

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php

    $this->append('head/index');

    ?>
</head>

<body>
<?php

$this->append('includes/header');

$this->append('includes/nav');

?>
<div class="container-fluid" style="padding: 0 6px;">
    <?php
    $this->tag('content');
    ?>
</div>

<?php

$this->append('includes/footer');

$this->append('resources/scripts');

?>
</body>
</html>
