<footer class="footer">
    <div class="container">
        <div class="copyright">
            <p class="text-muted text-right"> <?php out(strtolower(config('app.name'))); ?> &copy; copyright <?php out(date('Y')); ?></p>
        </div>
    </div>
</footer>