<?php

use Apocalipse\Core\Domain\Security\Auth;

?>
<nav class="navbar navbar-default navbar-static-top">

    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle Navigation</span>
                <i class="material-icons collapse-button">menu</i>
            </button>

        </div>

        <div class="collapse navbar-collapse navbar-collapse-top" id="navbar">

            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php url('index'); ?>" class="<?php out(url_starts('index') ? 'active' : ''); ?>">Home</a>
                </li>
                <li>
                    <a href="<?php url('fale-conosco'); ?>" class="<?php out(url_starts('fale-conosco') ? 'active' : ''); ?>">Fale Conosco</a>
                </li>
            </ul>

            <?php
                if (Auth::isGuest()) {
                    ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?php url('app/#/auth/login'); ?>">Entrar</a>
                            </li>
                            <li class="hidden-xs"><a> | </a></li>
                            <li>
                                <a href="<?php url('app/#/auth/register'); ?>">Cadastrar</a>
                            </li>

                        </ul>
                    <?php
                } else {
                    ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="javascript:void(0);" onclick="jQuery(this).parent().toggleClass('open');"
                               id="dropdown-toggle" class="dropdown-toggle"> <?php out(Auth::get('name')); ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown--options">
                                <li>
                                    <a href="<?php url('app/#/home'); ?>"> <i class="material-icons">home</i> Área Administrativa</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a  href="<?php url('app/#/auth/logout'); ?>"> <i class="material-icons">power_settings_new</i> Sair</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <?php
                }
            ?>

        </div>
    </div>
</nav>