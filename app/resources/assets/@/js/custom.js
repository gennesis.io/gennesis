App

    .controller('GennesisController', ['$rootScope', function ($rootScope) {

        $rootScope.project = App.project;
        
        $rootScope.showProject = function () {
            
        }
    }])

    .service('todoService', ['$resource', function ($resource) {
        return $resource(App.resource + "/resource/main/todo");
    }])

    .controller('todoCtrl', function (todoService) {

        var vm = this;

        vm.todo = '';
        vm.rows = [];

        vm.addTodoStat = false;

        todoService.get()
            .$promise.then(
                function (value) {
                    vm.rows = value.data.rows;
                }
            );

        vm.save = function ($event) {
            var todo = {
                project: App.project.driver,
                title: vm.title,
                todo: vm.todo
            };
            todoService.save(todo)
                .$promise.then(
                function (value) {
                    console.log(value.data);
                }
            );
            vm.todo = '';
            vm.rows.push(todo);
            vm.addTodoStat = false;
            $event.stopPropagation()
        };

        vm.cancel = function ($event) {
            vm.todo = '';
            vm.addTodoStat = false;
            $event.stopPropagation()
        };

        vm.delete = function (todo) {
            todoService.remove(todo);
            vm.rows.splice(vm.rows.indexOf(todo), 1);
        };

        vm.toggleDone = function (todo) {
            todo.done = !todo.done;
            todoService.save(todo);
        };
    });
