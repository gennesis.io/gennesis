
App._url = '..';
App.api = App._url  + '/api/head';
App.upload = App._url  + 'static/upload';
App.download = App._url  + 'static/download';
App.resource = '..';

App.header = 'X-Auth-Token';
App.cookies = {
    path: '/'
};
App.ref = '_csrf';
App.token = '';
App.id = '_id';
App.exit = '';
App.code = true;
App.debug = false;

App.project = localStorage.getItem('gennesis-project') ? JSON.parse(localStorage.getItem('gennesis-project')) : {};