<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: autoload.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 19/03/16 07:54
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Register autoload kernel function
 |
 */

use Kernel\Autoload\ClassLoader;

/** @noinspection PhpIncludeInspection */
require_once path(__APP_DIR_ROOT__, 'kernel', 'autoload', 'ClassLoader.php');

spl_autoload_register(['\Kernel\Autoload\ClassLoader', 'loadClass'], true, true);

ClassLoader::import('app');
