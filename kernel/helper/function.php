<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: function.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 04:23
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Add in environment built-ins functions
 |
 */

if (!function_exists('path')) {

    /**
     * @uses __APP_DIR__
     * @uses DIRECTORY_SEPARATOR
     * @uses func_get_args()
     *
     * @param $root
     *
     * @return string
     */
    function path($root)
    {
        $args = func_get_args();
        $peaces = [];
        if (is_bool($root)) {
            array_shift($args);
            if ($root) {
                $peaces[] = __APP_DIR__;
            }
        }
        $path = array_merge($peaces, $args);

        return implode(DIRECTORY_SEPARATOR, $path);
    }
}


if (!function_exists('config')) {

    /**
     * @param $property
     * @param null $scope
     * @return null
     */
    function config($property, $scope = null)
    {
        $scope = is_null($scope) ? 'app' : $scope;

        $__CONSTANT__ = '__' . strtoupper($scope) . '_CONFIG__';

        if (!defined($__CONSTANT__)) {

            $scope = is_null($scope) ? 'app' : $scope;

            define($__CONSTANT__, file_get_contents(path(true, 'config', __APP_ENV__, $scope . '.' . 'json')));
        }

        $settings = json_decode(constant($__CONSTANT__));
        $path = explode('.', $property);

        return search($settings, $path);
    }
}


if (!function_exists('environment')) {

    /**
     * @param $property
     * @return mixed
     */
    function environment($property)
    {
        return \Apocalipse\Core\App::environment($property);
    }
}

if (!function_exists('search')) {

    /**
     * @param $object
     * @param $path
     * @param int $matched
     * @param int $searched
     *
     * @return string
     */
    function search($object, $path, $matched = 0, $searched = 0)
    {

        $search = null;
        $value = null;

        foreach ($path as $property) {
            $searched++;
            if (isset($object->$property)) {
                $matched++;
                $value = $object->$property;

                $object = $object->$property;
            }
        }

        if ($matched === $searched) {
            $search = $value;
        }

        return $search;
    }
}


if (!function_exists('url')) {

    /**
     * @param $route
     * @param bool $query_string
     * @param bool $no_cache
     * @param bool $print
     * @return string
     */
    function url($route, $query_string = false, $no_cache = false, $print = true)
    {
        $url = __APP_URL__ . '/' . $route;
        if ($query_string && __APP_URL_QUERY__) {
            $url = $url . '?' . __APP_URL_QUERY__;
        }
        if ($no_cache) {
            $url = $url . (strpos('?', $url) === false ? '?' : '&') . 'no-cache=' . uniqid();
        }
        if ($print) {
            print $url;
        }

        return $url;
    }
}


if (!function_exists('url_starts')) {

    /**
     * @param $route
     * @return bool|int
     */
    function url_starts($route)
    {
        return strpos(__APP_URL_SERVICE__, $route) === 0;
    }
}


if (!function_exists('clear')) {

    /**
     * @param $text
     * @return string
     */
    function clear($text) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-',
            '/[’‘‹›‚]/u'    =>   ' ',
            '/[“”«»„]/u'    =>   ' ',
            '/ /'           =>   ' ',
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}


if (!function_exists('out')) {

    /**
     *
     * @param $value
     * @param bool $print
     * @param string $type
     * @return string
     */
    function out($value, $print = true, $type = null)
    {
        $type = iif($type, gettype($value));

        switch ($type) {
            case "date":
                $value = date("d/m/Y", strtotime($value));
                break;
            case "boolean":
            case "integer":
            case "double":
            case "float":
            case "string":
                $value = trim($value);
                break;
            case "array":
            case "object":
                $value = json_encode($value);
                break;
            case "resource":
            case "NULL":
            case "unknown type":
                $value = '';
                break;
        }
        if ($print) {
            print $value;
        }

        return $value;
    }
}


if (!function_exists('dashesToCamelCase')) {

    /**
     * @param $dashes
     * @param bool $first
     * @return mixed
     */
    function dashesToCamelCase($dashes, $first = true)
    {
        $camelCase = preg_replace_callback('/-(.)/', create_function('$matches', 'return strtoupper($matches[1]);'), $dashes);

        if ($first) {
            $camelCase[0] = strtoupper($camelCase[0]);
        } else {
            $camelCase[0] = strtolower($camelCase[0]);
        }

        return $camelCase;
    }
}


if (!function_exists('camelCaseToDashes')) {

    /**
     * @param $camelCase
     * @return mixed
     */
    function camelCaseToDashes($camelCase)
    {
        $dashes = preg_replace_callback('/([A-Z])/', create_function('$matches', 'return \'-\' . strtolower($matches[1]);'), $camelCase);

        return substr($dashes, 1);
    }
}


if (!function_exists('pathToNamespace')) {

    /**
     * @param $string
     * @return mixed
     */
    function pathToNamespace($string)
    {
        return str_replace('/', '\\', $string);
    }
}


if (!function_exists('namespaceToPath')) {

    /**
     * @param $string
     * @return mixed
     */
    function namespaceToPath($string)
    {
        return str_replace('\\', '/', $string);
    }
}


if (!function_exists('pathToFile')) {

    /**
     * @param $string
     * @return mixed
     */
    function pathToFile($string)
    {
        return str_replace('/', DIRECTORY_SEPARATOR, $string);
    }
}


if (!function_exists('includeFile')) {

    /**
     * Scope isolated include.
     * Prevents access to $this/self from included files.
     *
     * @param $filename
     * @param $once
     * @param $protocol
     */
    function includeFile($filename, $once = true, $protocol = 'file')
    {
        if (file_exists($filename)) {

            if ($once) {

                /** @noinspection PhpIncludeInspection */
                require_once $protocol . '://' . $filename;
            } else {

                /** @noinspection PhpIncludeInspection */
                require $protocol . '://' . $filename;
            }
        }
    }
}


if (!function_exists('getAllHeaders')) {

    /**
     * @uses $_SERVER
     *
     * @return string
     */
    function getAllHeaders()
    {
        $headers = '';
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


if (!function_exists('iif')) {

    /**
     * @param $value
     * @param $default
     * @return mixed
     */
    function iif($value, $default = false)
    {
        return is_null($value) ? $default : $value;
    }
}


if (!function_exists('sif')) {

    /**
     * @param $value
     * @param $property
     * @param $default
     *
     * @return mixed
     */
    function sif($value, $property = false, $default = null)
    {
        if (is_array($value)) {

            return isset($value[$property]) ? $value[$property] : $default;
        } else if (is_object($value)) {

            return isset($value->$property) ? $value->$property : $default;
        }
        return $default;
    }
}

if (!function_exists('dd')) {

    /**
     * @param $data
     */
    function dd($data)
    {
        $backtrace = debug_backtrace();
        if (is_array($backtrace) && isset($backtrace[1])) {
            $last = $backtrace[1];
            print '<pre>';
            var_dump((object)['data' => $data, 'file' => $last['file'], 'line' => $last['line']]);
            exit(0);
        }
    }
}

if (!function_exists('module')) {

    /**
     * @param $use
     * @return mixed
     */
    function module($use)
    {
        $class = explode('\\', $use);
        array_pop($class);
        return camelCaseToDashes(end($class));
    }
}

if (!function_exists('entity')) {

    /**
     * @param $use
     * @return mixed
     */
    function entity($use)
    {
        $class = explode('\\', $use);
        return camelCaseToDashes(end( $class ));
    }
}

if(!function_exists('mime_content_type')) {

    function mime_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } 
        else if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
}