<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @file: shutdown.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 19/03/16 07:52
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | Register shutdown action behavior
 |
 */

use Apocalipse\Core\App;

/**
 *
 */
register_shutdown_function(function () {

    App::shutdown();
});