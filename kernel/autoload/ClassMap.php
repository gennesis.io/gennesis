<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Kernel\Autoload
 | @file: Map.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 22/03/16 10:45
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Kernel\Autoload;


class ClassMap
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $package;

    /**
     * @var string
     */
    public $protocol;

    /**
     * @var \stdClass
     */
    public $environment;

    /**
     * Map constructor.
     * @param string $id
     * @param string $namespace
     * @param string $type
     * @param string $package
     * @param string $protocol
     * @param string $environment
     */
    public function __construct($id, $namespace, $type, $package, $protocol, $environment = null)
    {
        $this->id = $id;
        $this->namespace = $namespace;
        $this->type = $type;
        $this->package = $package;
        $this->protocol = $protocol;
        $this->environment = $environment;
    }

}