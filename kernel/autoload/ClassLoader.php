<?php

/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: ${NAMESPACE}
 | @file: ClassLoader.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 22/03/16 10:41
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Kernel\Autoload;

use \stdClass;

abstract class ClassLoader
{
    /**
     * @var \stdClass
     */
    private static $MAP = null;

    /**
     * @param string $pathClassMap
     */
    public static function init($pathClassMap = null)
    {
        if (!self::$MAP) {

            $pathClassMap = is_null($pathClassMap) ? path(__APP_DIR_ROOT__, 'kernel', 'autoload', 'ClassMap.php') : $pathClassMap;

            self::$MAP = new stdClass();

            includeFile($pathClassMap);
        }
    }

    /**
     * @param ClassMap $map
     * @return ClassMap
     */
    public static function add(ClassMap $map)
    {
        $id = $map->id;

        return self::$MAP->$id = $map;
    }

    /**
     * @param null $scope
     */
    public static function import($scope)
    {
        self::init();

        $autoloads = config('scopes', $scope);

        if (is_object($autoloads)) {

            foreach ($autoloads as $id => $autoload) {
                self::add(new ClassMap($id, pathToNamespace($id), $autoload->type, $autoload->package, $autoload->protocol, isset($autoload->environment) ? $autoload->environment : null));
            }
        }
    }

    /**
     * @param $use
     * @return bool
     */
    public static function loadClass($use)
    {
        if ($filename = self::findClass($use)) {

            includeFile($filename, true);

            return true;
        }

        return false;
    }

    /**
     * @param $use
     * @return bool
     */
    private static function findClass($use)
    {
        $file = false;

        /** @var \Kernel\Autoload\ClassMap $map */
        $map = self::findMap($use);

        if ($map) {
            $file = self::getFilenameClass($map, $use);
        }

        return $file;
    }

    /**
     * @param $map
     * @param $use
     * @return string
     */
    private static function getFilenameClass($map, $use)
    {
        $filename = '';

        $use = self::clear($use);

        $namespace = pathToFile(path(__APP_DIR_ROOT__, $map->type, $map->package));

        $peaces = explode('/', namespaceToPath(substr($use, strlen($map->id))));
        $parts = [];
        foreach ($peaces as $i => $peace) {
            if ($i > 0) {
                if ($i < count($peaces) - 1) {
                    $peace = camelCaseToDashes($peace);
                }
                $parts[] = $peace;
            }
        }
        $path = implode(DIRECTORY_SEPARATOR, $parts) . '.' . 'php';

        $file = path($namespace, $path);
        if (file_exists($file)) {
            $filename = $file;
        }

        return $filename;
    }

    /**
     * @param $use
     * @return bool
     */
    private static function loadBootstrap($use)
    {
        if ($file_names = self::findBootstrap($use)) {

            if (is_array($file_names)) {
                foreach ($file_names as $filename) {
                    includeFile($filename, true);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param $use
     * @return bool
     */
    private static function findBootstrap($use)
    {
        $file = false;

        /** @var \Kernel\Autoload\ClassMap $map */
        $map = self::findMap($use);

        if ($map) {
            $file = self::getFilenameBootstrap($map);
        }

        return $file;
    }

    /**
     * @param $map
     * @return string
     */
    private static function getFilenameBootstrap($map)
    {
        $file_names = null;

        $namespace = pathToFile(path(__APP_DIR_ROOT__, $map->type, $map->package));

        $files = ['bootstrap.php', 'init.php', 'helper.php'];
        if (is_dir($namespace)) {
            $file_names = [];
            foreach ($files as $file) {
                $file_names[] = path($namespace, $file);
            }
        }

        return $file_names;
    }

    /**
     * @param $class
     * @return ClassMap|null
     */
    private static function findMap($class)
    {
        $class = self::clear($class);

        $map = null;
        /** @var \Kernel\Autoload\ClassMap $autoload */
        foreach (self::$MAP as $key => $autoload) {
            $key = pathToNamespace($key);
            if (strpos($class, $key) === 0) {
                $map = $autoload;
            }
        }

        return $map;
    }

    /**
     * @param $namespace
     * @return string
     */
    private static function clear($namespace)
    {
        if ('\\' == $namespace[0]) {
            $namespace = substr($namespace, 1);
        }
        return $namespace;
    }

    /**
     * @param $use
     * @return bool
     */
    public static function bootstrap($use)
    {
        return self::loadBootstrap($use);
    }

    /**
     * @param $use
     * @return ClassMap|null
     */
    public static function root($use)
    {
        return self::findMap($use);
    }

}